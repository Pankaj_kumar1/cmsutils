package com.times.service;

import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.web.util.UriUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.DBObject;
import com.mongodb.util.JSON;
import com.times.constants.SeoConstants;

public class MongoRestAPIService {
	private  final Logger logger = LoggerFactory.getLogger(MongoRestAPIService.class);
	/*
	 * Please do not change this URL this is for test environment.
	 */
	public String MONGO_REST_API_URL;
	private static final String MONGO_REST_API_LIVE = "http://mra.indiatimes.com/mra/";
	private static final String MONGO_REST_API_LOCAL = "http://mratest.indiatimes.com/mra/";

	private  final String getURL = "get/#collection?query=#query";
	private  final String updateURL = "update/#collection?queryJson=#query&rowJson=#rowJson";
	private  final String insertURL = "insert/#collection?rowJson=#rowJson";
	private  final String deleteURL = "delete/#collection?query=#query";
	private  final String findandUpdateURL = "findAndUpdate/#collection?query=#query&rowJson=#rowJson";
	private  final String groupURL = "group/#collection?query=#query";
	private  final String countURL = "count/#collection?query=#query";
	
	private  final String insertURL_post = "insert/post/#collection";
	private  final String findandUpdateURL_post = "findAndUpdate/#collection?query=#query";
	private static final String updateURL_post = "update/#collection?queryJson=#query";
	
	public  DBObject get(String collection, DBObject query) {
		String extData = null;
		String strURL = null;
		List<DBObject> dbObj = null;
		strURL = getURL.replaceAll("#query", Matcher.quoteReplacement(query.toString())).replaceAll("#collection", collection);
		strURL = MONGO_REST_API_LIVE + strURL;
		extData = getExtData(strURL);
		if(extData != null && !"".equals(extData)){
			Object dbObjectRes = JSON.parse(extData);
			if (dbObjectRes instanceof List<?>) {
				List<DBObject> dbObjects = (List<DBObject>) dbObjectRes;
				if (((List<DBObject>) dbObjectRes).size() > 0) {
					return ((List<DBObject>) dbObjectRes).get(0);
				}
			} else if (dbObjectRes instanceof DBObject) {
				return (DBObject) dbObjectRes;
			}
		}
		return null;
	}
	
	public  List<DBObject> getList(String collection, DBObject query) {
		String extData = null;
		String strURL = null;
		List<DBObject> dbObj = null;
		strURL = getURL.replaceAll("#query", Matcher.quoteReplacement(query.toString())).replaceAll(
				"#collection", collection);
		strURL = MONGO_REST_API_LIVE + strURL;
		extData = getExtData(strURL);
		if(extData != null && !"".equals(extData)){
			Object dbObjectRes = JSON.parse(extData);
			if (dbObjectRes instanceof List<?>) {
					return ((List<DBObject>) dbObjectRes);
			} 
		}
		return null;
	}
	
	public  List<DBObject> getList(String collection, DBObject query, Map<String, Object> options) {
		String extData = null;
		String strURL = null;
		List<DBObject> dbObj = null;
		strURL = getURL.replaceAll("#query", Matcher.quoteReplacement(query.toString())).replaceAll(
				"#collection", collection);
		strURL = MONGO_REST_API_LIVE + strURL + optiosParameter(options);
		extData = getExtData(strURL);
		if(extData != null && !"".equals(extData)){
			Object dbObjectRes = JSON.parse(extData);
			if (dbObjectRes instanceof List<?>) {
					return ((List<DBObject>) dbObjectRes);
			} 
		}
		return null;
	}
	
	public int getCount(String collection, DBObject query){
		
		String extData = null;
		String strURL = null;
		strURL = countURL.replaceAll("#query", Matcher.quoteReplacement(query.toString())).replaceAll(
				"#collection", collection);
		strURL = MONGO_REST_API_LIVE + strURL;
		extData = getExtData(strURL);
		return Integer.parseInt(extData);
	}

	private  String optiosParameter(Map<String, Object> options){
		StringBuilder builder = new StringBuilder();
		for(Map.Entry<String, Object> entry : options.entrySet()){
			builder.append("&"+entry.getKey()+"="+entry.getValue());
		}
		return builder.toString();
	}

	public  boolean insert(String collection, DBObject rowJson) {
		String strURL = null;
		strURL = insertURL.replaceAll("#rowJson", Matcher.quoteReplacement(rowJson.toString()))
				.replaceAll("#collection", collection);
		strURL = MONGO_REST_API_LIVE + strURL;
		if (getExtData(strURL) != null) {
			return true;
		}
		
		return false;
	}

	public  boolean insertWithPost(String collection, DBObject rowJson) {
		String strURL = null;
		//strURL = insertURL.replaceAll("#rowJson", Matcher.quoteReplacement(rowJson.toString()))	.replaceAll("#collection", collection);
		strURL = insertURL_post.replaceAll("#collection", collection);
		strURL = MONGO_REST_API_LIVE + strURL;
		if (postData(strURL,  rowJson, null) != null) {
			return true;
		}
		
		return false;
	}
	
	public  boolean update(String collection, DBObject queryJson,	DBObject rowJson) {
		String strURL = null;
		strURL = updateURL.replaceAll("#rowJson", Matcher.quoteReplacement(rowJson.toString()))
				.replaceAll("#collection", collection).replaceAll("#query",
						Matcher.quoteReplacement(queryJson.toString()));
		strURL = MONGO_REST_API_LIVE + strURL;
		if (getExtData(strURL) != null) {
			return true;
		}
		return false;
	}
	

	/**
	 * This API will update collection with using upsert value.
	 * @param collection
	 * @param queryJson
	 * @param rowJson
	 * @param upsert
	 * @return
	 */
	public  boolean update(String collection, DBObject queryJson,
			DBObject rowJson, boolean upsert) {
		String strURL = null;
		strURL = updateURL.replaceAll("#rowJson", Matcher.quoteReplacement(rowJson.toString().replace("&", "%26")))
				.replaceAll("#collection", collection).replaceAll("#query",
						Matcher.quoteReplacement(queryJson.toString()));
		strURL = strURL +  ", {upsert : "  + upsert + "}";
		strURL = MONGO_REST_API_LIVE + strURL;
		
		String responseText = getExtData(strURL);
		
		if (responseText != null) {
			return true;
		}
		return false;
	}
	
	public  boolean updateWithPost(String collection, DBObject queryJson,	DBObject rowJson) {
		 String strURL = null;
         //strURL = insertURL.replaceAll("#rowJson", Matcher.quoteReplacement(rowJson.toString()))       .replaceAll("#collection", collection);
         strURL = updateURL_post.replaceAll("#collection", collection).replaceAll("#query", Matcher.quoteReplacement(queryJson.toString()));
         //strURL = strURL + ", {upsert :true}";
         strURL = MONGO_REST_API_LIVE + strURL;
         if (postData(strURL, rowJson, null) != null) {
                return true;
         }
        
         return false;
	}

	public  boolean delete(String collection, DBObject queryJson) {
		String strURL = null;
		strURL = deleteURL.replaceAll("#collection", collection).replaceAll(
				"#query", Matcher.quoteReplacement(queryJson.toString()));
		strURL = MONGO_REST_API_LIVE + strURL;
		if (getExtData(strURL) != null) {
			return true;
		}
		return false;
	}

	public  boolean findAndUpdate(String collection, DBObject queryJson,
			DBObject rowJson) {
		String strURL = null;
		strURL = findandUpdateURL.replaceAll("#rowJson", Matcher.quoteReplacement(rowJson.toString()))
				.replaceAll("#collection", collection).replaceAll("#query",
						Matcher.quoteReplacement(queryJson.toString()));
		strURL = MONGO_REST_API_LIVE + strURL;
		if (getExtData(strURL) != null) {
			return true;
		}
		return false;
	}
	
	public  boolean findAndUpdateWithPost(String collection, DBObject queryJson,	DBObject rowJson) {
		String strURL = null;
		strURL = findandUpdateURL_post.replaceAll("#collection", collection).replaceAll("#query", Matcher.quoteReplacement(queryJson.toString()));
		
		//.replaceAll("#rowJson", Matcher.quoteReplacement(rowJson.toString())).replaceAll("#query", Matcher.quoteReplacement(queryJson.toString())
		strURL = MONGO_REST_API_LIVE + strURL;
		if (postData(strURL, rowJson, null) != null) {
			return true;
		}
		return false;
	}

	private  String getExtData(String strURL) {
		HttpClient hClient = HttpClients.custom()
				.setRetryHandler(new DefaultHttpRequestRetryHandler(3, false))
				.build();
		RequestConfig config = RequestConfig.custom()
				.setMaxRedirects(2)
				.setConnectTimeout(15000)
				.setSocketTimeout(15000)
				.build();
		
		HttpRequestBase gMethod = null;
		HttpResponse hResponse = null;
		try {
			strURL = UriUtils.encodeQuery(strURL, StandardCharsets.UTF_8.toString());
			strURL = strURL.replace("%2526", "%26");
			gMethod = new HttpGet(strURL);
			gMethod.setConfig(config);
			hResponse = hClient.execute(gMethod);
			String extData = EntityUtils.toString(hResponse.getEntity(), StandardCharsets.UTF_8);
			return extData;
		} catch (Exception e) {
			logger.error("error ", e);
			e.printStackTrace();
		} finally {
			HttpClientUtils.closeQuietly(hResponse);
			HttpClientUtils.closeQuietly(hClient);
		}
		return null;
	}
	
	public  DBObject group(String collection, DBObject query, Map<String, Object> options) {
		String strURL = null;
		strURL = groupURL.replaceAll("#query", Matcher.quoteReplacement(query.toString())).replaceAll(
				"#collection", collection);
		strURL = MONGO_REST_API_LIVE + strURL + optiosParameter(options);
		String extData = getExtData(strURL);
		if(extData != null && !"".equals(extData)){
			DBObject dbObjectRes = (DBObject) JSON.parse(extData);
			return dbObjectRes;
		}
		return null;
	}

	private  String postData(String strURL, DBObject content, DBObject queryJson) {
		HttpClient hClient = HttpClients.custom()
				.setRetryHandler(new DefaultHttpRequestRetryHandler(3, false))
				.build();
		RequestConfig config = RequestConfig.custom()
				.setMaxRedirects(2)
				.setConnectTimeout(5000)
				.setSocketTimeout(5000)
				.build();

        HttpRequestBase pMethod = null;
        HttpResponse hResponse = null;
        try {
               strURL = UriUtils.encodeQuery(strURL, StandardCharsets.UTF_8.toString());
               /*this replce is to hanlde the life & style section to get msid*/
              // strURL = strURL.replace("%20&%20", "+%26+");
               
               if (content != null) {
            	   URI uri = new URIBuilder(strURL).addParameter("rowJson", content.toString()).build();
            	   strURL = uri.toString();
//            	   pMethod.addParameter("rowJson", content.toString());
               }
               pMethod = new HttpPost(strURL);
               
               pMethod.setConfig(config);
               /*StringRequest Entity requestEntity = new StringRequestEntity(content.toString(), "application/json", "UTF-8");
               pMethod.setRequestEntity(requestEntity);*/
                    
               hResponse = hClient.execute(pMethod);
               String extData = EntityUtils.toString(hResponse.getEntity(), StandardCharsets.UTF_8);
               return extData;
        } catch (Exception e) {
        } finally {
               HttpClientUtils.closeQuietly(hResponse);
               HttpClientUtils.closeQuietly(hClient);
        }
        return null;
 }
	

	public  boolean insert(String collection, DBObject rowJson, boolean isPost) {
		String strURL = null;
		//strURL = insertURL.replaceAll("#rowJson", Matcher.quoteReplacement(rowJson.toString()))	.replaceAll("#collection", collection);
		strURL = insertURL_post.replaceAll("#collection", collection);
		strURL = MONGO_REST_API_LIVE + strURL;
		if (postData(strURL, rowJson) != null) {
			return true;
		}
		
		return false;
	}
	
	
	
	public boolean updateWithPost(String collection, DBObject queryJson,	DBObject rowJson, boolean upsert) {
		String strURL = null;
        
        strURL = updateURL_post.replaceAll("#collection", collection).replaceAll("#query", Matcher.quoteReplacement(queryJson.toString()));
        
        strURL = strURL +  ", {upsert : "  + upsert + "}";
        
        strURL = MONGO_REST_API_LIVE + strURL;
        if (postData(strURL, rowJson) != null) {
               return true;
        }
       
        return false;
	}
	
	
	
	
	// POST data insert
		private static String postData(String strURL, DBObject content) {
			
			// Get http client
			HttpClient httpClient = HttpClients.custom().setRetryHandler(new DefaultHttpRequestRetryHandler(3, false)).build();
			
			// Create request config parameters
			RequestConfig config = RequestConfig.custom().setMaxRedirects(2).setConnectTimeout(5000).setSocketTimeout(5000).build();
			
			// Post object
			HttpPost httpPost = null;
			
			// Response object
			HttpResponse httpResponse = null;
			
			try {
				// Encode Url
				strURL = UriUtils.encodeQuery(strURL, "UTF-8");
				
				// Create post request
				httpPost = new HttpPost(strURL);
				
				// Set configuration
				httpPost.setConfig(config);
				
				// List for query parameters to set in post entity
				List<NameValuePair> params = new ArrayList<NameValuePair>();
				
				// Set content of dbObject as parameter 
				params.add(new BasicNameValuePair("rowJson", content.toString()));
				
				// Create entity with above parameters and allow utf-8 characters.
				HttpEntity entity = new UrlEncodedFormEntity(params, SeoConstants.UTF_8);
				
				// Set entity in post request
				httpPost.setEntity(entity);
				
				// Get response
				httpResponse = httpClient.execute(httpPost);
				
				//logger.info("Response for inserting with post in DB: "+ httpResponse!=null ? httpResponse.toString(): httpResponse);
				
				String extData = EntityUtils.toString(entity, "UTF-8");
				
				return extData;
				
			} catch (Exception e) {
				
//				log.error(strURL+" - Exception Occured in get the external data Exception : "+ e.getMessage() + " with paramsMap ", e);
//				
//				new CMSCallExceptions(strURL+ " - Exception Occured in get the external data Exception : "+ e.getMessage() + " with paramsMap ",
//										CMSExceptionConstants.CMS_Exception, e);
			} finally {
				
				HttpClientUtils.closeQuietly(httpClient);
				HttpClientUtils.closeQuietly(httpResponse);
			}
			return null;
		}
	
	

		/**
		 * Get from mra (mongo db) with encoding of queryJson to support special symbol like '&' in query.
		 * @param collection
		 * @param query
		 * @return
		 */
		public  List<DBObject> getListWithEncodeQuery(String collection, DBObject query) {
			String extData = null;
			String strURL = null;
			strURL = getURL.replaceAll("#query", Matcher.quoteReplacement(encodeQueryParam(query.toString()))).replaceAll(
					"#collection", collection);
			strURL = MONGO_REST_API_LIVE+ strURL;
			extData = getExtDataWithoutEncode(strURL);
			if(extData != null && !"".equals(extData)){
				Object dbObjectRes = JSON.parse(extData);
				if (dbObjectRes instanceof List<?>) {
					return ((List<DBObject>) dbObjectRes);
				} 
			}
			return null;
		}


		private static String encodeQueryParam(String query) {
			try{
				if (StringUtils.isNotEmpty(query)) {
					return UriUtils.encodeQueryParam(query.toString(), StandardCharsets.UTF_8.toString());
				}
			} catch (Exception e) {			

				e.printStackTrace();
			}
			return query;
		}
		
		
		/**
		 * Http Get call without url encode.
		 * @param strURL
		 * @return
		 */
		private  String getExtDataWithoutEncode(String strURL) {
			HttpClient hClient = HttpClients.custom()
					.setRetryHandler(new DefaultHttpRequestRetryHandler(3, false))
					.build();
			RequestConfig config = RequestConfig.custom()
					.setMaxRedirects(2)
					.setConnectTimeout(15000)
					.setSocketTimeout(15000)
					.build();

			HttpRequestBase gMethod = null;
			HttpResponse hResponse = null;
			try {

				gMethod = new HttpGet(strURL);
				gMethod.setConfig(config);
				hResponse = hClient.execute(gMethod);
				String extData = EntityUtils.toString(hResponse.getEntity(), StandardCharsets.UTF_8);
				return extData;
			} catch (Exception e) {
				logger.error("error ", e);
				e.printStackTrace();
			} finally {
				HttpClientUtils.closeQuietly(hResponse);
				HttpClientUtils.closeQuietly(hClient);
			}
			return null;
		}




		/**
		 * update in mra (mongo db) with encoding of queryJson to support special symbol like '&' in query.
		 * @param collection
		 * @param queryJson
		 * @param rowJson
		 * @param upsert
		 * @return
		 */
		public boolean updateWithPostWithEncodeQuery(String collection, DBObject queryJson,	DBObject rowJson, boolean upsert) {
			String strURL = null;

			strURL = updateURL_post.replaceAll("#collection", collection).replaceAll("#query", Matcher.quoteReplacement(encodeQueryParam(queryJson.toString() 
					+ "{upsert : "  + upsert + "}")));

			strURL = MONGO_REST_API_LIVE + strURL;
			if (postDataWithoutEncode(strURL, rowJson) != null) {
				return true;
			}

			return false;
		}




		/**
		 * post call without url encode.
		 * @param strURL
		 * @param content
		 * @return
		 */
		private static String postDataWithoutEncode(String strURL, DBObject content) {

			// Get http client
			HttpClient httpClient = HttpClients.custom().setRetryHandler(new DefaultHttpRequestRetryHandler(3, false)).build();			
			// Create request config parameters
			RequestConfig config = RequestConfig.custom().setMaxRedirects(2).setConnectTimeout(5000).setSocketTimeout(5000).build();			
			// Post object
			HttpPost httpPost = null;			
			// Response object
			HttpResponse httpResponse = null;			
			try {
				httpPost = new HttpPost(strURL);				
				// Set configuration
				httpPost.setConfig(config);				
				// List for query parameters to set in post entity
				List<NameValuePair> params = new ArrayList<NameValuePair>();				
				// Set content of dbObject as parameter 
				params.add(new BasicNameValuePair("rowJson", content.toString()));				
				// Create entity with above parameters and allow utf-8 characters.
				HttpEntity entity = new UrlEncodedFormEntity(params, SeoConstants.UTF_8);				
				// Set entity in post request
				httpPost.setEntity(entity);				
				// Get response
				httpResponse = httpClient.execute(httpPost);
				String extData = EntityUtils.toString(entity, "UTF-8");				
				return extData;

			} catch (Exception e) {

				System.out.println(e.getMessage());

			} finally {

				HttpClientUtils.closeQuietly(httpClient);
				HttpClientUtils.closeQuietly(httpResponse);
			}
			return null;
		}




	
	


	public  String getMONGO_REST_API_URL() {
		return MONGO_REST_API_URL;
	}

	public  void setMONGO_REST_API_URL(String mONGO_REST_API_URL) {
		MONGO_REST_API_URL = mONGO_REST_API_URL;
	}
}
