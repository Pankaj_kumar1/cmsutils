package com.times.service;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.mongodb.DBObject;
import com.mongodb.util.JSON;

public class CMSPortalService {

	public static DBObject getHostInstance(String header) {
		DBObject headerObj = null;
		try {
			String portalUrl = "http://cmsportalle.indiatimes.com/hostheader?filter={isdefault:true,header:'####'}";
			portalUrl = portalUrl.replace("####", header);
			List<DBObject> headerList = getPortalDataFromMongo(portalUrl, "portalliveread", "readLivePortal1");
			headerObj = headerList.get(0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return headerObj;
	}

	public static DBObject getHeaderInfo(String host, String instance) {
		DBObject headerObj = null;
		try {
			String portalUrl = "http://cmsportalle.indiatimes.com/hostheader?filter={isdefault:true,hostid:##,instanceid:@@@}";
			portalUrl = portalUrl.replace("##", host).replace("@@@", instance);
			List<DBObject> headerList = getPortalDataFromMongo(portalUrl, "portalliveread", "readLivePortal1");
			headerObj = headerList.get(0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return headerObj;
	}

	@SuppressWarnings("unchecked")

	public static List<DBObject> getPortalDataFromMongo(String mongoUrl, String userName, String Passwd)
			throws Exception {

		List<DBObject> objectList = new ArrayList<DBObject>();

		String extData = getHttpData(mongoUrl, userName, Passwd);

		if (extData == null)

			return null;

		Object parsedData = JSON.parse(extData);

		if (parsedData instanceof Map<?, ?>) {

			Map<?, ?> objectMap = (Map<?, ?>) parsedData;

			if (objectMap.containsKey("_embedded") && objectMap.get("_embedded") instanceof Map<?, ?>) {

				Map<?, ?> map = (Map<?, ?>) objectMap.get("_embedded");

				if (map.containsKey("rh:doc") && map.get("rh:doc") instanceof List<?>)

					objectList = (List<DBObject>) map.get("rh:doc");

			}

		}

		return objectList;

	}

	private static String getHttpData(String mongoUrl, String userName, String passWd)
			throws URISyntaxException, ClientProtocolException, IOException, InterruptedException {

		String extData = "";

		int noOfExection = 0;

		HttpResponse response = null;

		HttpClient httpClient = HttpClients.custom().setMaxConnPerRoute(3).build();

		RequestConfig config = RequestConfig.custom()

				.setMaxRedirects(2)

				.setSocketTimeout(30000)

				.setConnectTimeout(50000)

				.build();

		try {

			URL url = new URL(mongoUrl);

			URI uri = null;

			uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(),
					url.getQuery(), url.getRef());

			HttpRequestBase getRequest = new HttpGet(uri);

			getRequest.setConfig(config);

			getRequest.addHeader("Authorization", "Basic " + getBasicAuthenticationEncoding(userName, passWd));

			int status = 0;

			while (noOfExection < 3 && status != HttpStatus.SC_OK) {

				response = httpClient.execute(getRequest);

				noOfExection++;

				status = response.getStatusLine().getStatusCode();

				if (status != HttpStatus.SC_OK) {

					// logger.error("Status: {}, Url: {}", status ,url);

					TimeUnit.SECONDS.sleep(4);

				}

			}

			if (status != HttpStatus.SC_OK)

				return null;

			response.setHeader("Content-Type", "application/json; charset=UTF-8");

			extData = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);

		} catch (Exception ex) {

		} finally {

			HttpClientUtils.closeQuietly(response);

			HttpClientUtils.closeQuietly(httpClient);

		}

		return extData;

	}

	private static String getBasicAuthenticationEncoding(String userName, String passWd) {

		String userPassword = userName.trim() + ":" + passWd.trim();

		return new String(Base64.encodeBase64(userPassword.getBytes()));

	}

}
