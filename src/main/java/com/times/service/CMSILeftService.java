package com.times.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.mongodb.DBObject;
import com.times.dao.CMSILeftTagHandlerDAO;
import com.times.model.CMSILeftObject;

public class CMSILeftService implements ApplicationContextAware {

	ApplicationContext ctx;

	private int interval;
	private Date lastUpdated;
	private Map<String,String> iLeftMapSEOtoOL = new HashMap<String,String>();
	private Map<String,String> iLeftMapOLtoSEO = new HashMap<String,String>();
	private CMSILeftTagHandlerDAO iLeftDAO;
	
	private final static Logger logger = LoggerFactory.getLogger(CMSILeftService.class);
	@Override
	public void setApplicationContext(ApplicationContext arg0)
			throws BeansException {
		this.ctx = arg0;
	}
	
	public boolean init(){
		try {
			
			Calendar calendar = Calendar.getInstance();
			Date date = calendar.getTime();
			
			List<Map<String, String>> iLeftMaps=new ArrayList<Map<String,String>>();
			
			iLeftMaps = iLeftDAO.getCMSIleftInfo();
			iLeftMapSEOtoOL = iLeftMaps.get(0);
			iLeftMapOLtoSEO = iLeftMaps.get(1);
			
			if(iLeftMapSEOtoOL!=null && iLeftMapOLtoSEO!=null){
				lastUpdated = date;
			}
			
		} catch (Exception e) {
			logger.error("Generic Exception caught while getting CMSILeftInfo from Mongo", e);
			return false;
		}
		return true;
		
	}
	
	public String getOverrideLink(String seolocation){
		if(iLeftMapSEOtoOL!=null && !iLeftMapSEOtoOL.isEmpty()){
			String value = iLeftMapSEOtoOL.get(seolocation);
			return value;
		}
		return null;
	}
	
	public String getSeolocation(String overridelink){
		if(iLeftMapOLtoSEO!=null && !iLeftMapOLtoSEO.isEmpty()){
			String value = iLeftMapOLtoSEO.get(overridelink);
			return value;
		}
		return null;
	}
	
	public String getActualURL(String seolocation){
		String value = null;
		if(seolocation!=null){
			try {
				value = iLeftDAO.getActualURL(seolocation);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return value;
	}
	
	public String getShortURL(String overridelink){
		String value = null;
		if(overridelink!=null){
			try {
				value = iLeftDAO.getShortURL(overridelink);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return value;
	}
	
	public List<CMSILeftObject> getAllShortUrls(){
		List<CMSILeftObject> iLeftList=new ArrayList<CMSILeftObject>();
		try {
			iLeftList = iLeftDAO.getAllShortUrls();
			Collections.reverse(iLeftList);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return iLeftList;
	}
	public void addCMSIleftInfo(CMSILeftObject cmsILeftObj) throws Exception{
		
		String seolocation = cmsILeftObj.getSeolocation();
		String overridelink = cmsILeftObj.getOverridelink();
		
		CMSILeftObject cmsILeftDBValue = iLeftDAO.getCMSIleftInfo(seolocation);
		if(cmsILeftDBValue!=null){
			if(overridelink!=null && !cmsILeftDBValue.getOverridelink().equals(overridelink)){
				iLeftDAO.addCMSIleftInfo(cmsILeftObj);
			}
		}
		else{
			iLeftDAO.addCMSIleftInfo(cmsILeftObj);
		}
		
		if(seolocation!=null && overridelink!=null && cmsILeftDBValue.getRedirectionType() == 0){
			if(!iLeftMapSEOtoOL.containsKey(seolocation)){
				iLeftMapSEOtoOL.put(seolocation, overridelink);
			}
			else{
				if(overridelink!=null && !iLeftMapSEOtoOL.get(seolocation).equals(overridelink)){
					iLeftMapSEOtoOL.remove(seolocation);
					iLeftMapSEOtoOL.put(seolocation, overridelink);
				}
			}

			if(!iLeftMapOLtoSEO.containsKey(overridelink)){
				iLeftMapOLtoSEO.put(overridelink, seolocation);
			}
			else{
				if(seolocation!=null && !iLeftMapOLtoSEO.get(overridelink).equals(seolocation)){
					iLeftMapOLtoSEO.remove(overridelink);
					iLeftMapOLtoSEO.put(overridelink, seolocation);
				}
			}
		}
		
	}
	
	public CMSILeftObject getCMSIleftInfo(String seolocation) throws Exception{
		
		CMSILeftObject resultObj;
		
		if(seolocation!=null){
 			if(getOverrideLink(seolocation)!=null){
				resultObj = new CMSILeftObject();
				resultObj.setOverridelink(getOverrideLink(seolocation));
				resultObj.setSeolocation(seolocation);
			}
			else{
				resultObj = iLeftDAO.getCMSIleftInfo(seolocation);
				if(resultObj!=null){
					addCMSIleftInfo(resultObj);
				}
			}
			if(resultObj!=null){
				return resultObj;
			}
		}
		return null;
		
	}
	
	public CMSILeftObject getCMSIleftInfoFromOverLink(String overridelink) throws Exception{
		CMSILeftObject resultObj = null;		
		if(overridelink!=null){
			if(getSeolocation(overridelink)!=null){
				resultObj = new CMSILeftObject();
				resultObj.setOverridelink(overridelink);
				resultObj.setSeolocation(getSeolocation(overridelink));
			}				
		}
		return resultObj;
	}
	
	public boolean addCMSObject(CMSILeftObject obj){
		try {
			return iLeftDAO.addCMSIleftInfo(obj);
		} catch (Exception e) {
			return false;
		}
	}
	
	public boolean updateCMSObject(CMSILeftObject obj,String key){
		try {
			return iLeftDAO.updateCMSIleftInfo(obj,key);
		} catch (Exception e) {
			return false;
		}
	}
	
	public Object searchandupdateCMSObject(DBObject obj,String key, boolean delete){
		try {
			return iLeftDAO.searchandupdateCMSIleftInfo(obj, key , delete);
		} catch (Exception e) {
			return false;
		}
	}
	
	public boolean removeCMSObject(CMSILeftObject obj,String key){
		try {
			return iLeftDAO.removeCMSIleftInfo(obj,key);
		} catch (Exception e) {
			return false;
		}
	}
	
	public Map<String,Integer> getHostInstanceId(String host, int port,  boolean isLocal){
		try{
			if(isLocal)
				return iLeftDAO.getHostInfo(host, port);
			else
				return iLeftDAO.getHeaderInfo(host);
		} catch(Exception e){
			e.printStackTrace();
		}
		return null;	
	}
	
	public Map<String, Object> getDomain(int hostid, int instanceId) {
		try{
			
			return iLeftDAO.getDomain(hostid, instanceId);
			
		} catch( Exception ex){
			ex.printStackTrace();
		}
		return null;
	}
	
	public int getMappingCount(DBObject obj){
		return iLeftDAO.getMappingCount(obj);
	}
	
	public int getInterval() {
		return interval;
	}

	public void setInterval(int interval) {
		this.interval = interval;
	}

	public CMSILeftTagHandlerDAO getiLeftDAO() {
		return iLeftDAO;
	}

	public void setiLeftDAO(CMSILeftTagHandlerDAO iLeftDAO) {
		this.iLeftDAO = iLeftDAO;
	}

}
