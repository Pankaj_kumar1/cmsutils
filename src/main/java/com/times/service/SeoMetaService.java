package com.times.service;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mongodb.DBObject;
import com.times.dao.SeoMetaDao;
import com.times.model.SeoMeta;

@Service
public class SeoMetaService {

	@Autowired
	private SeoMetaDao seoMetaDao;

	public boolean insert(SeoMeta seoMeta) throws UnsupportedEncodingException {
		return seoMetaDao.insert(seoMeta);
	}

	public List<DBObject> getUriList(SeoMeta seoMeta) {
		return seoMetaDao.getUriList(seoMeta);
	}
	
	public boolean isMetaPresent(String uri, String channel){
		return seoMetaDao.isMetaPresent(uri, channel);
	}
}
