package com.times.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.times.dao.ValidUserDao;

/**
 * 
 * @author pankaj.kumar1
 *
 */
@Service
public class InMemoryUpdateService {

	private static final Logger logger = LoggerFactory.getLogger(InMemoryUpdateService.class);

	@Autowired
	private ValidUserDao validUserDao;

	public String updateSeoPanelUsers() {
		try {
			validUserDao.updateValidUserList();
			return "Users updated";
		} catch (Exception e) {
			logger.error("Error occured while updating in memory seoPanel users: ", e);
			return "failed to update with exception: " + e.getMessage();

		}
	}

}
