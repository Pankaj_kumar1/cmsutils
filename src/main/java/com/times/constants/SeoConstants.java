package com.times.constants;

import java.util.HashSet;
import java.util.Set;

/**
 * 
 * @author pankaj.kumar1 These are the project specific constants.
 *
 */

public class SeoConstants {

	
	public static String LDAP_HOST = "https://jssostg.indiatimes.com";
	
	public static final String LOGIN_URI = "/sso/ldap/identity/login?ru=";
	public static final String REDIRECT_URI = "/cmsutils/";
	public static final String ACCESS_CODE_URI = "/sso/ldap/identity/getToken";
	public static final String USER_DETAILS_URI = "/sso/ldap/identity/getUserDetails";

	public static final String PERMISSION_DENIED_URI = "/cmsutils/permissiondenied";

	public static final String RESOURCES_URI = "/cmsutils/scripts";
	public static final String METAFETCH = "/cmsutils/seo-meta/geturilist";
	public static final String METAINSERRT = "/cmsutils/seo-meta/insert";
	public static final String METAUPDATE = "/cmsutils/seo-meta/insert";
	public static final String METAFILEUPLOAD = "/cmsutils/upload/uploadfile";

	public static final String SHORTURL_ADD = "/cmsutils/add";
	public static final String SHORTURL_GETALL = "/cmsutils/getall";
	public static final String SHORTURL_GET = "/cmsutils/get";
	public static final String SHORTURL_UPDATE = "/cmsutils/update";
	public static final String SHORTURL_REFRESH = "/cmsutils/refresh";

	public static final String HTTP = "http://";
	public static final String HTTP_SECURE = "https://";
	public static final String UTF_8 = "UTF-8";
	public static final String TEMPCODE = "tempCode";
	public static final String CODE = "code";
	public static final String DATA = "data";
	public static final String ACCESSTOKEN = "accessToken";
	public static final String COOKIE_NAME = "SEOTOKEN";
	public static final String EMAIL = "email";

	public static Set<String> validAPISet = new HashSet<String>();

	static {

		validAPISet.add(PERMISSION_DENIED_URI);

		validAPISet.add(RESOURCES_URI);
		validAPISet.add(METAFETCH);
		validAPISet.add(METAINSERRT);
		validAPISet.add(METAUPDATE);
		validAPISet.add(METAFILEUPLOAD);

		validAPISet.add(SHORTURL_ADD);
		validAPISet.add(SHORTURL_GETALL);
		validAPISet.add(SHORTURL_GET);
		validAPISet.add(SHORTURL_UPDATE);
		validAPISet.add(SHORTURL_REFRESH);

	}

}
