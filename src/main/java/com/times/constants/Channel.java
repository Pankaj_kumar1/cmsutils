package com.times.constants;
 
public enum Channel {
 
       TOI("83"),
       ET("19"),
	   NBT("53"),
	   GN("307"),
	   FR("309"),
	   NBT_GN("326"),
	   MALAYALAM_GN("336"),
	   TELUGU_GN("335"),
	   TAMIL_GN("334"),
	   VK_GN("333"),
	   EISAMAY_GN("332"),
	   MT_GN("331"),
	   AUTO_NOW("347"),
	   GADGET_CENTRAL("352"),
	   MT("155"),
	   EISAMAY("252"),
	   VK("245"),
	   TAMIL_SAMAYAM("285"),
	   TELUGU_SAMAYAM("286"),
	   MALAYALAM_SAMAYAM("287"),
	   IAG("338"),
	   BI("261");
	   
       
      
       private final String hostid;
      
       Channel(String hostid){
              this.hostid = hostid;
       }
      
       public static Channel getByTypeName(String hostid){
              if(hostid == null || hostid.trim().isEmpty()){
                     return null;
              }
             
              switch(hostid){
              case "83" : return TOI;
              case "19" : return ET;
              case "307" : return GN;
              case "53" : return NBT;
              case "309" : return FR;
              case "326" : return NBT_GN;
              case "336" : return MALAYALAM_GN;
              case "335" : return TELUGU_GN;
              case "334" : return TAMIL_GN;
              case "333" : return VK_GN;
              case "332" : return EISAMAY_GN;
              case "331" : return MT_GN;
              case "347" : return AUTO_NOW;
              case "352" : return GADGET_CENTRAL;
              case "155" : return MT;
              case "252" : return EISAMAY;
              case "245" : return VK;
              case "285" : return TAMIL_SAMAYAM;
              case "286" : return TELUGU_SAMAYAM;
              case "287" : return MALAYALAM_SAMAYAM;
              case "338" : return IAG;
              case "261" : return BI;
                           
              }
              return null;
       }
       
       public String getHostid(){
    	   
    	   return hostid;
       }
 
       
}
 
