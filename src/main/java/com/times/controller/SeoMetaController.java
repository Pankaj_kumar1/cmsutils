package com.times.controller;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.times.constants.Channel;
import com.times.model.AppRequestContext;
import com.times.model.SeoMeta;
import com.times.service.SeoMetaService;
import com.times.users.ChannelUsers;

@Controller
@RequestMapping("/seo-meta")
public class SeoMetaController {
	
	private static final Logger logger = LoggerFactory.getLogger(SeoMetaController.class);
	
	
	@Autowired
	private SeoMetaService seoMetaService;
	
	@Autowired
	private ChannelUsers channelUsers;

	@Autowired
	private AppRequestContext appRequestContext;
	
	@RequestMapping(value = "/form", method = RequestMethod.GET)
	public String displayAns(Model model) {
		return "techgadget";
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public String edit(Model model) {

		return "edit";
	}

	@RequestMapping(value = "/insert", method = {RequestMethod.PUT,RequestMethod.POST,RequestMethod.GET})
	@ResponseBody
	public String insert(@RequestParam(name ="uri") String uri, @RequestParam(name ="channel") Channel channel,
			@RequestParam(name ="title") String title, @RequestParam(name ="shortdesc") String shortDesc,
			@RequestParam(name ="longdesc") String longDesc, @RequestParam(name ="atf-content") String atfContent,
            @RequestParam(name ="btf-content") String btfContent,@RequestParam(name ="canonicalUrl") String canonicalUrl,  @RequestParam(name ="mUrl", required=false) String mUrl,
            @RequestParam(name ="ampUrl", required=false) String ampUrl, @RequestParam(name ="metakeywords",required=false) String metakeywords,
            @RequestParam(name="action")String action,
            @RequestParam(name="titleElement", required=false)String titleElement,
            @RequestParam(name="urlElement",required=false)String urlElement) throws JSONException{
            
		
		// @RequestParam(name="mode")String mode , @RequestParam(name ="username") String userName
		
		try {
			
			String email = appRequestContext.getEmail();
			
			if(email == null || "".equals(email) ){
				JSONObject jsonObject = new JSONObject();
				jsonObject.append("msg", "ERROR: your session expired, login again");
				return jsonObject.toString();
			}else{
				
				if(!uri.startsWith("/")){
					JSONObject jsonObject = new JSONObject();
					jsonObject.append("msg", "URI should start with /");
					return jsonObject.toString();
				}
			
			if(seoMetaService.isMetaPresent(uri, channel.toString()) && "create".equals(action)){
				logger.info("reinsertion called for URI: "+uri+" and tried by- "+email);
				JSONObject jsonObject = new JSONObject();
				jsonObject.append("msg", "Cannot add ! Meta already entered for this URI, Kindly use Update Seo Description");
				return jsonObject.toString();
			}
			
			
			logger.debug("inser method called");
			logger.debug("With detail - ");
			logger.debug("uri -"+uri);
			logger.debug("channel - "+channel);
			logger.debug("title - "+title);
			logger.debug("shortDesc - "+shortDesc);
			logger.debug("longDesc - "+longDesc);
			logger.debug("atfContent - "+atfContent);
			logger.debug("btfContent - "+btfContent);
			logger.debug("userName - "+email);
			logger.debug("canonicalUrl - "+canonicalUrl);
			logger.debug("mUrl - "+mUrl);
			logger.debug("ampUrl - "+ampUrl);
			logger.debug("metakeywords - "+metakeywords);
			logger.debug("action - "+action);
//			logger.debug("mode - "+mode);
			
			// authenticate user and then proceed
			if(channelUsers.isValidUser(email, channel.name())){
				SeoMeta seoMeta = new SeoMeta();
				seoMeta.setHostid(Integer.parseInt(channel.getHostid()));
				seoMeta.setChannel(channel.toString());
				seoMeta.setUri(uri);
				seoMeta.setLongdesc(longDesc);
				seoMeta.setShortdesc(shortDesc);
				seoMeta.setTitle(title);
				seoMeta.setAtfContent(atfContent);
				seoMeta.setBtfContent(btfContent);
				seoMeta.setCanonicalUrl(canonicalUrl);
				seoMeta.setmUrl(mUrl);
				seoMeta.setAmpUrl(ampUrl);
				seoMeta.setMetakeywords(metakeywords);
				seoMeta.setAction(action);
				seoMeta.setUserName(email);
				if (StringUtils.isNotEmpty(titleElement) && StringUtils.isNotEmpty(urlElement)) {
					List<String> titleElementList = Arrays.asList(titleElement
							.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)"));
					List<String> urlElementList = Arrays.asList(urlElement
							.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)"));
					Map titleUrlMap =  getMap(titleElementList, urlElementList);
					if (titleUrlMap != null && titleUrlMap.size() > 0) {
						seoMeta.setTitle_url_map(titleUrlMap);
					}
				}

//				seoMeta.setMode(mode);
				boolean inserted = seoMetaService.insert(seoMeta);
				logger.info("is inserted - " + inserted);
				JSONObject jsonObject = new JSONObject();
				if(inserted){
					if("create".equals(seoMeta.getAction())){
					jsonObject.append("msg", "SUCCESS! Data inserted successfully");
					logger.info("Data inserted for seometa url: "+seoMeta.getUri()+ " and inserted by "+seoMeta.getUserName());
					}else if("update".equals(seoMeta.getAction())){
						jsonObject.append("msg", "SUCCESS! Data updated successfully");
						logger.info("Data updated for seometa url: "+seoMeta.getUri()+ " and updated by "+seoMeta.getUserName());
					}
				}else{
					jsonObject.append("msg", "SORRY! you are not authorised for this channel");
				}
				
				logger.debug(jsonObject.toString());
				return jsonObject.toString();
			}else{
				logger.info("SORRY! you are not authorised for this channel");
				JSONObject jsonObject = new JSONObject();
				jsonObject.append("msg", "SORRY! you are not authorised for this channel");
				return jsonObject.toString();
			}
			}
		}catch(Exception e){
			logger.error("ERROR while inserting the data", e);
			JSONObject jsonObject = new JSONObject();
			jsonObject.append("msg", "SORRY! got unexpected exception ("+e.getMessage()+")");
			return jsonObject.toString();
		}
		
	}
	
	@RequestMapping(value = "/geturilist", method = RequestMethod.GET)
	@ResponseBody
	public String getUriList(@RequestParam(value ="shorturl") String shorturl, @RequestParam(name ="channel") Channel channel) throws JSONException {
		
		//sAuthentication auth = SecurityContextHolder.getContext().getAuthentication();
		String email = appRequestContext.getEmail();
		if(email == null || "".equals(email) ){
			JSONObject jsonObject = new JSONObject();
			jsonObject.append("msg", "ERROR: your session expired, login again");
			return jsonObject.toString();
		}else{
		
		logger.debug("getUriList called");
		logger.debug("search for shorturl + " + shorturl + " and channel " + channel);
		
		SeoMeta seoMeta = new SeoMeta();
		seoMeta.setUri(shorturl);
		seoMeta.setHostid(Integer.parseInt(channel.getHostid()));
		
		ObjectMapper mapper = new ObjectMapper();
        String jsonInString = null;
        try {
			jsonInString = mapper.writeValueAsString(seoMetaService.getUriList(seoMeta));
			logger.info("db value for " + jsonInString);
		} catch (JsonProcessingException e) {
			logger.error("Error while get list for " + shorturl + " and channel " + channel);
		}
        return jsonInString;
	}
	}
	
	public static <K, V> Map<K, V> getMap(List<K> keys, List<V> values) {
		Iterator<K> keyIter = keys.iterator();
		Iterator<V> valIter = values.iterator();
		Map map = new LinkedHashMap();
		while(keyIter.hasNext() && valIter.hasNext()) {
			String key = (String) keyIter.next();
			String value = (String) valIter.next();
			if (StringUtils.isNotEmpty(key) && StringUtils.isNotEmpty(value)) {
				map.put(key, value);
			}
		}
		return map;
		/*  return keys.stream()
	              .collect(Collectors.toMap(_i -> keyIter.next(), _i -> valIter.next()));*/
		/* return IntStream.range(0, keys.size()).boxed()
	            .collect(Collectors.toMap(_i -> keyIter.next(), _i -> valIter.next()));*/
	}
}
