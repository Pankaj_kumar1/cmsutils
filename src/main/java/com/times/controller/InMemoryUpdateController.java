package com.times.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.times.service.InMemoryUpdateService;

@RestController
@RequestMapping("/inmemoryupdate")
public class InMemoryUpdateController {
	
	@Autowired
	private InMemoryUpdateService inMemoryUpdateService;
	
	@RequestMapping(value = "/validusers", method = RequestMethod.GET)
	public String updateSeoPanelUser() {
		return inMemoryUpdateService.updateSeoPanelUsers();
		
	}

}
