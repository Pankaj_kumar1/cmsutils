package com.times.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import com.times.model.AppRequestContext;
import com.times.model.SeoMeta;
import com.times.service.SeoMetaService;
import com.times.users.ChannelUsers;
import com.times.utility.XlsxReader;



@Controller
@RequestMapping("/upload")
public class FileUploadController {

	private static final Logger logger = LoggerFactory.getLogger(FileUploadController.class);
	public static final String[] COLUMN = {"URI","Channel","HostId", "title", "longdesc","shortdesc","Atf-content","Btf-content","Canonical-url",
	"Mode"};

	
	@Autowired
	private SeoMetaService seoMetaService;

	@Autowired
	private ChannelUsers channelUsers;
	
	@Autowired
	private AppRequestContext appRequestContext;

	/**
	 * This API upload meta from xlsx.
	 * @param req
	 * @param response
	 * @param model
	 * @param file
	 * @throws IOException
	 */
	@RequestMapping("/uploadfile")
	public void uploadFile(HttpServletRequest req,HttpServletResponse response,Model model, MultipartFile file) throws IOException {

		
		String username = appRequestContext.getEmail();

		InputStream in = file.getInputStream();
		List<Map<String, Object>> metaList = XlsxReader.getXLSXRecordsList(in, COLUMN);
		in.close();

		Map<String, String> resultMap = new HashMap<String, String>();

		for (Map<String, Object>meta : metaList) {

			SeoMeta seoMeta = new SeoMeta();
			if (meta.get("HostId") != null && !StringUtils.isEmpty(String.valueOf(meta.get("HostId")))) {
				seoMeta.setHostid(Math.round(Float.parseFloat(meta.get("HostId").toString())));
			}
			if (meta.get("Channel") != null && !StringUtils.isEmpty(String.valueOf(meta.get("Channel")))) {
				seoMeta.setChannel(meta.get("Channel").toString());
			}
			if (meta.get("URI") != null && !StringUtils.isEmpty(String.valueOf(meta.get("URI")))) {
				seoMeta.setUri(meta.get("URI").toString());
			}

			if (meta.get("longdesc") != null && !StringUtils.isEmpty(String.valueOf(meta.get("longdesc")))) {
				seoMeta.setShortdesc(meta.get("longdesc").toString());
			}

			if (meta.get("shortdesc") != null && !StringUtils.isEmpty(String.valueOf(meta.get("shortdesc")))) {
				seoMeta.setTitle(meta.get("shortdesc").toString());
			}			
			if (meta.get("title") != null && !StringUtils.isEmpty(String.valueOf(meta.get("title")))) {
				seoMeta.setTitle(meta.get("title").toString());
			}			
			if (meta.get("Atf-content") != null && !StringUtils.isEmpty(String.valueOf(meta.get("Atf-content")))) {
				seoMeta.setAtfContent(meta.get("Atf-content").toString());
			}
			if (meta.get("Btf-content") != null && !StringUtils.isEmpty(String.valueOf(meta.get("Btf-content")))) {
				seoMeta.setBtfContent(meta.get("Btf-content").toString());
			}
			if (meta.get("Canonical-url") != null && !StringUtils.isEmpty(String.valueOf(meta.get("Canonical-url")))) {
				seoMeta.setCanonicalUrl(meta.get("Canonical-url").toString());
			}
			seoMeta.setAction("upload");

			seoMeta.setUserName(username);
			if (meta.get("Mode") != null && !StringUtils.isEmpty(String.valueOf(meta.get("Mode")))) {
				seoMeta.setMode(meta.get("Mode").toString());
			}
			try {
				if(channelUsers.isValidUser(username, seoMeta.getChannel())){
					boolean inserted = seoMetaService.insert(seoMeta);
					logger.info("is inserted - " + inserted);
					JSONObject jsonObject = new JSONObject();
					if(inserted){
						if("upload".equals(seoMeta.getAction())){
							jsonObject.append("msg", "SUCCESS! Data upload successfully");
							logger.info("Data upload for seometa url: "+seoMeta.getUri()+ " and updated by "+seoMeta.getUserName());
							resultMap.put(seoMeta.getUri(), "Data upload successfully");
						}
					}else{
						jsonObject.append("msg", "SORRY! you are not authorised for this channel");
					}

				}else{
					logger.info("SORRY! you are not authorised for this channel");
					JSONObject jsonObject = new JSONObject();
					jsonObject.append("msg", "SORRY! you are not authorised for this channel");

				}
			} catch (Exception e) {
				// TODO: handle exception
			}

		}

		if (resultMap != null && resultMap.size() > 0) {
			InputStream in1 = file.getInputStream();
			XSSFWorkbook wb  = XlsxReader.updateXLSXRecordsList(in1, COLUMN,response, resultMap);


			ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
			wb.write(outByteStream);

			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Disposition", "attachment; filename=result.xlsx");

			byte[] outArray = outByteStream.toByteArray();
			OutputStream outStream = response.getOutputStream();
			outStream.write(outArray);
			outStream.flush();
		}



		model.addAttribute("message", "File: " + file.getOriginalFilename() 
				+ " has been uploaded successfully!");	}
}
