package com.times.controller;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.LongUnaryOperator;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;
import com.times.model.AppRequestContext;
import com.times.model.CMSILeftObject;
import com.times.model.ConfigObject;
import com.times.service.CMSILeftService;
import com.times.service.CMSPortalService;
import com.times.service.MongoRestAPIService;
import com.times.users.ChannelUsers;
import com.times.users.MappingUsers;
import com.times.utility.MimeType;

@Controller
public class CMSShortUrlController extends HttpServlet implements ApplicationContextAware {

	private static final long serialVersionUID = 1L;

	private static final String MONGO_REST_API_LOCAL = "http://mratest.indiatimes.com/mra/";
	private static final String MONGO_REST_API_LIVE = "http://mra.indiatimes.com/mra/";

	private static final Map<String, String> domainChannelMap = new HashMap<>();
	static {

		domainChannelMap.put("timesofindia.indiatimes.com", "TOI");
		domainChannelMap.put("m.timesofindia.com", "TOI");
		
		domainChannelMap.put("economictimes.indiatimes.com", "ET");
		domainChannelMap.put("m.economictimes.com", "ET");
		
		//ET lang
		domainChannelMap.put("vsp1mlanghindiet.economictimes.com", "ETLM");
		domainChannelMap.put("etlanghindi.indiatimes.com", "ETLW");
		
		// Mirror sites enabled
		
		domainChannelMap.put("mumbaimirror.indiatimes.com", "MM");
		domainChannelMap.put("bangaloremirror.indiatimes.com", "BM");
		domainChannelMap.put("punemirror.indiatimes.com", "PM");
		domainChannelMap.put("ahmedabadmirror.indiatimes.com", "AM");
		
		
		// Times job
		domainChannelMap.put("content.timesjobs.com", "TJ");
		domainChannelMap.put("content.techgig.com", "TG");
		
		
		// Language sites enabled
		
		domainChannelMap.put("navbharattimes.indiatimes.com", "NBT");
		domainChannelMap.put("m.navbharattimes.indiatimes.com", "NBT");
		
		domainChannelMap.put("eisamay.indiatimes.com", "EIS");
		domainChannelMap.put("m.eisamay.com", "EIS");
		
		domainChannelMap.put("tamil.samayam.com", "TAS");
		domainChannelMap.put("m.tamil.samayam.com", "TAS");
		
		domainChannelMap.put("telugu.samayam.com", "TES");
		domainChannelMap.put("m.telugu.samayam.com", "TES");
		
		domainChannelMap.put("maharashtratimes.com", "MT");
		domainChannelMap.put("m.maharashtratimes.com", "MT");
		
		domainChannelMap.put("malayalam.samayam.com", "MS");
		domainChannelMap.put("m.malayalam.samayam.com", "MS");
		
		//domainChannelMap.put("vijaykarnataka.indiatimes.com", "VK");
		domainChannelMap.put("m.vijaykarnataka.com", "VK");
		domainChannelMap.put("vijaykarnataka.com", "VK");
		
		
		// TGP sites enabled
		
		domainChannelMap.put("www.adageindia.in", "AD");
		domainChannelMap.put("m.adageindia.in", "AD");
		
		domainChannelMap.put("www.gizmodo.in", "GZ");
		
		domainChannelMap.put("www.businessinsider.in", "BI");
		domainChannelMap.put("m.businessinsider.in", "BI");
		
		domainChannelMap.put("www.in.techradar.com", "TR");
		domainChannelMap.put("m.in.techradar.com", "TR");
		
		domainChannelMap.put("www.in.techspot.com", "TS");
		
		domainChannelMap.put("www.lifehacker.co.in", "LH");
		
		//HappyTrips enabled
		domainChannelMap.put("timesofindia.indiatimes.com/travel", "HT");
		
		// GadgetsNow
		domainChannelMap.put("www.gadgetsnow.com", "GN");
	    domainChannelMap.put("m.gadgetsnow.com", "GN");
	    
	    // Photogallery
	    domainChannelMap.put("photogallery.indiatimes.com", "Photogallery");
	    domainChannelMap.put("m.photos.timesofindia.com", "MobilePhoto");
	    
	    domainChannelMap.put("recipes.timesofindia.com", "Recipes");
	    domainChannelMap.put("m.recipes.timesofindia.com", "Recipes");
	    
	    domainChannelMap.put("www.bombaytimes.com", "BombayTimes");
	    domainChannelMap.put("m.bombaytimes.com", "BombayTimes");
	    
	    domainChannelMap.put("beautypageants.indiatimes.com", "BeautyPageants");
	    domainChannelMap.put("m.beautypageants.in", "BeautyPageants");
	    
	    domainChannelMap.put("www.misskyra.com", "Misskyra");
	    domainChannelMap.put("m.misskyra.com", "Misskyra");
	    
	    domainChannelMap.put("auto.timesofindia.com", "AutoNow");
	    
	    domainChannelMap.put("navbharattimes.indiatimes.com/navbharatgold", "NBTGOLD");
	    domainChannelMap.put("eisamay.indiatimes.com/eisamaygold", "EISAMAYGOLD");
	    
	    domainChannelMap.put("www.iamgujarat.com", "IAMGUJARAT");

		
	}

	private ApplicationContext ctx = null;
	CMSILeftService cmsILeftService;
	ConfigObject configObj;
	MongoRestAPIService mongoRestAPIService;

	@Autowired
	private MappingUsers mappingusers;
	
	@Autowired
	private AppRequestContext appRequestContext;

	private static final Logger logger = LoggerFactory.getLogger(CMSShortUrlController.class);

	/*
	 * @RequestMapping(value = "/edit**", method = RequestMethod.GET)
	 */public ModelAndView showPage(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView mav = new ModelAndView();
		mav.addObject("username", appRequestContext.getEmail());
		mav.setViewName("shorturls");
		return mav;
	}

	public  ModelAndView permissionDenied(HttpServletRequest request, HttpServletResponse response){
        ModelAndView mav = new ModelAndView();
        mav.setViewName("permissionDenied");
        return mav;
    }

	/*
	 * @RequestMapping(value = "/add**", method = RequestMethod.GET)
	 */public void addAndUpdateShortUrls(HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {
			

			String username = appRequestContext.getEmail();
			String inserDateTime = new SimpleDateFormat("E, dd MMM yyyy, HH:mm:ss").format(new Date());
			String shorturl = request.getParameter("shorturl");
			String actualurl = request.getParameter("actualurl");
			String action = request.getParameter("action");
			String key = request.getParameter("key");
			String onMobile = request.getParameter("onmobile");
			String mappingStatus = null;
			JSONObject jo = new JSONObject();
			int count = 0;
			int mobileCount = 0;
			boolean isLocal = false;

			/*
			 * Map<String, String> domainChannelMap = new HashMap<>();
			 * domainChannelMap.put("timesofindia.indiatimes.com", "TOI");
			 * domainChannelMap.put("m.timesofindia.com", "TOI");
			 * domainChannelMap.put("economictimes.indiatimes.com", "ET");
			 * domainChannelMap.put("m.economictimes.com", "ET");
			 * 
			 */
			
			if(username == null || "".equals(username)){
				mappingStatus = "Sorry!!! Your session expired, try logging again";
				jo.put("status", false);
				jo.put("message", mappingStatus);
				String output = jo.toString();
				if (request.getParameter("callback") != null && !"".equals(request.getParameter("callback"))) {
					output = request.getParameter("callback") + "(" + output + ");";
				}
				String mimeType = "application/json";
				response.setContentType(mimeType);
				response.setHeader("Content-Type", mimeType + "; charset=UTF-8");
				int pageLength = output.getBytes("UTF-8").length;
				response.setHeader("Content-Length", pageLength + "");
				response.getWriter().write(output);
				response.getWriter().flush();
				response.getWriter().close();
			}else{

			URI shortUri = null;
			URI actualUri = null;
			if (shorturl.startsWith("http://"))
				shortUri = new URI(shorturl);
			else
				shortUri = new URI("http://" + shorturl);

			if (actualurl.startsWith("http://"))
				actualUri = new URI(actualurl);
			else
				actualUri = new URI("http://" + actualurl);

			actualurl = actualUri.toString();
			shorturl = shortUri.toString();

			if (actualurl.contains(".cms?") || shorturl.contains(".cms?")) {
				mappingStatus = "Sorry!!! Cannot map urls containing parameters";
				jo.put("status", false);
				jo.put("message", mappingStatus);
				logger.info("Long url has parameters- " + actualurl);
			} else if (shorturl.endsWith(".cms")) {
				mappingStatus = "Cannot map!! Short url cannot end with .cms";
				jo.put("status", false);
				jo.put("message", mappingStatus);
			} else if (!actualurl.endsWith(".cms")) {
				mappingStatus = "Cannot map!! Actual url should end with .cms only";
				jo.put("status", false);
				jo.put("message", mappingStatus);
			} else if (shorturl.contains("dev.indiatimes.") || actualurl.contains("dev.indiatimes.")) {
				mappingStatus = "Cannot map staging urls";
				jo.put("status", false);
				jo.put("message", mappingStatus);
			} else if (shorturl.endsWith(".indiatimes.com/") || shorturl.endsWith(".indiatimes.com") 
					   || shorturl.endsWith(".com") || shorturl.endsWith(".com/") || shorturl.endsWith(".in") 
					   || shorturl.endsWith(".in/") || actualurl.endsWith("/default.cms") 
					   || actualurl.endsWith("/defaultinterstitial.cms")) {
				mappingStatus = "Cannot map homepage for redirection";
				jo.put("status", false);
				jo.put("message", mappingStatus);
			} else {

				CMSILeftObject cmsObj = new CMSILeftObject();
				CMSILeftObject cmsObjForMobile = new CMSILeftObject();

				if ("LOCAL MODE".equalsIgnoreCase(getConfigObj().getMode()))
					isLocal = true;

				String domainOfActualUri = actualUri.getHost().toString();
				String domainOfShortUri = shortUri.getHost().toString();
				
				domainOfActualUri += handleSpecialHosts(actualurl);
				domainOfShortUri += handleSpecialHosts(shorturl);
				
				
				if (domainOfActualUri.equals(domainOfShortUri)) {
					if (!mappingusers.isValidUser(username, domainChannelMap.get(domainOfShortUri))) {
						mappingStatus = "Sorry!! You are not authorized for mapping urls for this domain";
						jo.put("Status", false);
						jo.put("message", mappingStatus);

						String output = jo.toString();
						if (request.getParameter("callback") != null && !"".equals(request.getParameter("callback"))) {
							output = request.getParameter("callback") + "(" + output + ");";
						}
						String mimeType = "application/json";
						response.setContentType(mimeType);
						response.setHeader("Content-Type", mimeType + "; charset=UTF-8");
						int pageLength = output.getBytes("UTF-8").length;
						response.setHeader("Content-Length", pageLength + "");
						response.getWriter().write(output);
						response.getWriter().flush();
						response.getWriter().close();
					} else {
                             if(shorturl.contains("/articleshow/") || actualurl.contains("/articleshow/")){
                            	 
                            	 mappingStatus = "Sorry!!  Cannot map articlshow urls";
         						jo.put("Status", false);
         						jo.put("message", mappingStatus);

         						String output = jo.toString();
         						if (request.getParameter("callback") != null && !"".equals(request.getParameter("callback"))) {
         							output = request.getParameter("callback") + "(" + output + ");";
         						}
         						String mimeType = "application/json";
         						response.setContentType(mimeType);
         						response.setHeader("Content-Type", mimeType + "; charset=UTF-8");
         						int pageLength = output.getBytes("UTF-8").length;
         						response.setHeader("Content-Length", pageLength + "");
         						response.getWriter().write(output);
         						response.getWriter().flush();
         						response.getWriter().close();
                            	 
                             }else{
						DBObject headerObj = CMSPortalService.getHostInstance(domainOfActualUri);
						int instanceIdForMobile = 5;
						if(domainOfActualUri.equals("navbharattimes.indiatimes.com")|| domainOfActualUri.equals("eisamay.indiatimes.com") 
						   || domainOfActualUri.equals("www.gadgetsnow.com") || domainOfActualUri.equals("photogallery.indiatimes.com")){
							instanceIdForMobile=4;
						}
						cmsObj.setSeolocation(shortUri.toString());
						cmsObj.setOverridelink(actualUri.toString());
						cmsObj.setCatkey(getMsid(actualurl));
						cmsObj.setSname(getSname(shorturl));
						cmsObj.setSnameseo(getSname(shorturl));
						cmsObj.setHostId((int)headerObj.get("hostid"));
						cmsObj.setInstanceId((int)headerObj.get("instanceid"));
						cmsObj.setMappedBy(username);
						cmsObj.setMappedAt(inserDateTime);
						//cmsObj.setStatus(1);  This will be used for enable/disable mapping  
						
						DBObject countObject = new BasicDBObject();
						countObject.put("hostId", (int)headerObj.get("hostid"));
						countObject.put("instanceId", (int)headerObj.get("instanceid"));
						
						count = cmsILeftService.getMappingCount(countObject);
						int remCount = 5000-count;
						

						if ("1".equals(key)) {
							key = CMSILeftObject.SEOLOCATION;
						} else if ("2".equals(key)) {
							key = CMSILeftObject.OVERRIDELINK;
						}
                        if(remCount <=0 && onMobile != null){
                        	mappingStatus = "WEB mapping limit achieved. Map WAP urls explicitly"; 
                        }else if(remCount <= 0){
                        	mappingStatus = "mapping limit of this domain achieved. Cannot map!";
                        }else{
						if (action.equals("a")) {
							if (cmsILeftService.addCMSObject(cmsObj)) {
								remCount -= 1;
								logger.info(
										"Adding mapping for short Url- " + shorturl + " and actual Url " + actualurl);
								logger.info("Mapping added by " + username + " at " + inserDateTime);
								mappingStatus = "Url is mapped successfully. "+remCount+" mapping remaining for this domain";
								if (onMobile != null) {
									
									DBObject countMobObj = new BasicDBObject();
									countMobObj.put("hostId", (int)headerObj.get("hostid"));
									countMobObj.put("instanceId", instanceIdForMobile);
									
									mobileCount = cmsILeftService.getMappingCount(countMobObj);
									int remMobileCount = 5000-mobileCount;
									if(remMobileCount <=0){
										mappingStatus = "Web mapping done.Wap not mapped. Wap mapping limit achieved.";
									}else{
									
									DBObject mobObj = CMSPortalService.getHeaderInfo(headerObj.get("hostid").toString(), String.valueOf(instanceIdForMobile));
									if (mobObj != null) {
										String mobileDomain = mobObj.get("header").toString();
										String actualMobileDomain = mobileDomain + actualUri.getRawPath();
										String shortMobileDomain = mobileDomain + shortUri.getRawPath();
										URI shortMobileDomainUri = null;
										URI actualMobileDomainUri = null;

										if (shortMobileDomain.startsWith("http://"))
											shortMobileDomainUri = new URI(shortMobileDomain);
										else
											shortMobileDomainUri = new URI("http://" + shortMobileDomain);

										if (actualMobileDomain.startsWith("http://"))
											actualMobileDomainUri = new URI(actualMobileDomain);
										else
											actualMobileDomainUri = new URI("http://" + actualMobileDomain);

										logger.info("Adding mapping for mobile short Url - " + shortMobileDomain
												+ " and actual Url - " + actualMobileDomain);
										logger.info(
												"Mapping added for mobile by -" + username + " at " + inserDateTime);

										cmsObjForMobile.setSeolocation(shortMobileDomainUri.toString());
										cmsObjForMobile.setOverridelink(actualMobileDomainUri.toString());
										cmsObjForMobile.setCatkey(getMsid(actualMobileDomain));
										cmsObjForMobile.setSname(getSname(shortMobileDomain));
										cmsObjForMobile.setSnameseo(getSname(shortMobileDomain));
										cmsObjForMobile.setHostId((int) mobObj.get("hostid"));
										cmsObjForMobile.setInstanceId(instanceIdForMobile);
										cmsObjForMobile.setMappedBy(username);
										cmsObjForMobile.setMappedAt(inserDateTime);
										cmsObjForMobile.setStatus(1);

										if (cmsILeftService.addCMSObject(cmsObjForMobile)) {
											remMobileCount -= 1;
											mappingStatus = "Both Web and Mobile domains are mapped."+ 
										                     remCount+" web mapping remaining and "+
													         remMobileCount +" wap mapping remaining for this channel";
										}
									}
								}
								}

								jo.put("Status", true);
								jo.put("message", mappingStatus);
							} else {
								jo.put("Status", false);
								jo.put("message", "Error in Adding. Try again.");
							}
						} else if (action.equals("u")) {
							if (cmsILeftService.updateCMSObject(cmsObj, key)) {
								jo.put("Status", true);
								jo.put("message", "Successfully Updated.");
							} else {
								jo.put("Status", false);
								jo.put("message", "Error in Updating. Try again.");
							}
						} else if (action.equals("r")) {
							if (cmsILeftService.removeCMSObject(cmsObj, key)) {
								jo.put("Status", true);
								jo.put("message", "Successfully Removed.");
							} else {
								jo.put("Status", false);
								jo.put("message", "Error in Deleting. Try again.");
							}
						}
						jo.put("surl", cmsObj.getSeolocation());
						jo.put("aurl", cmsObj.getOverridelink());
						jo.put("msid", cmsObj.getCatkey());
						jo.put("sname", cmsObj.getSname());
						jo.put("hostid", cmsObj.getHostId());
						jo.put("instanceid", cmsObj.getInstanceId());
						jo.put("status", cmsObj.getStatus());
					}
                             }
					}
				}
				else {
					mappingStatus = "Cannot map inter-domain urls";
					jo.put("Status", false);
					jo.put("message", mappingStatus);
				}
			}

			String output = jo.toString();
			if (request.getParameter("callback") != null && !"".equals(request.getParameter("callback"))) {
				output = request.getParameter("callback") + "(" + output + ");";
			}
			String mimeType = "application/json";
			response.setContentType(mimeType);
			response.setHeader("Content-Type", mimeType + "; charset=UTF-8");
			int pageLength = output.getBytes("UTF-8").length;
			response.setHeader("Content-Length", pageLength + "");
			response.getWriter().write(output);
			response.getWriter().flush();
			response.getWriter().close();
			}

		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			ex.printStackTrace();
		}

	}

	/*
	 * @RequestMapping(value = "/refresh**", method = RequestMethod.GET)
	 */public void refreshShortUrls(HttpServletRequest request, HttpServletResponse response) throws Exception {

		JSONObject jo = new JSONObject();
		try {
			if (cmsILeftService.init()) {
				jo.put("result", true);
			} else {
				jo.put("result", false);
			}
			String output = jo.toString();
			if (request.getParameter("callback") != null && !"".equals(request.getParameter("callback"))) {
				output = request.getParameter("callback") + "(" + output + ");";
			}
			String mimeType = "application/json";
			response.setContentType(mimeType);
			response.setHeader("Content-Type", mimeType + "; charset=UTF-8");
			int pageLength = output.getBytes("UTF-8").length;
			response.setHeader("Content-Length", pageLength + "");
			response.getWriter().write(output);
			response.getWriter().flush();
			response.getWriter().close();
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
	}

	/*
	 * @RequestMapping(value = "/get**", method = RequestMethod.GET)
	 */public void getURLMapping(HttpServletRequest request, HttpServletResponse response) throws IOException {
		try {
			String requesturl = request.getParameter("url");
			String type = "shorturl";
			String mobileDomain = "";
			String mobileurl = "mobileurl"; // This will be updated(with corresponding wap url) if requesturl is web url.
			if (request.getParameter("type") != null) {
				type = request.getParameter("type");
			}
			//Replace space with %20
			//Url to be mapped may contain %20 but here requesturl comes as decoded form
			requesturl = requesturl.replaceAll("\\s","%20");
			
			URI resulturi = null;
			if (requesturl.startsWith("http://"))
				resulturi = new URI(requesturl);
			else {
				resulturi = new URI("http://" + requesturl);
			}
			
		    // This will give exception if the domain of requesturl is wrong.
			String domainOfResultUri = resulturi.getHost().toString();
			DBObject headerObj = CMSPortalService.getHostInstance(domainOfResultUri);
			if (headerObj != null && headerObj.get("hostid")!=null) {

				// This is done to check if corresponding mobile mapping is
				// present or not
				if (!requesturl.startsWith("http://m.") && !requesturl.startsWith("m.") && !domainOfResultUri.equals("mumbaimirror.indiatimes.com")) {
					int instanceIdForMobile = 5;
					if( domainOfResultUri.equals("navbharattimes.indiatimes.com") || domainOfResultUri.equals("eisamay.indiatimes.com")){
						instanceIdForMobile = 4;
					}
					
					DBObject mobObj = CMSPortalService.getHeaderInfo(headerObj.get("hostid").toString(), String.valueOf(instanceIdForMobile));
					if (mobObj != null) {
						

						mobileDomain = mobObj.get("header").toString();
						mobileurl = mobileDomain + resulturi.getRawPath();
						
						URI resultMobileuri = null;
						if (mobileurl.startsWith("http://"))
							resultMobileuri = new URI(mobileurl);
						else {
							resultMobileuri = new URI("http://" + mobileurl);
						}

						if (type.equals("shorturl")) {
							requesturl = getCmsILeftService().getActualURL(resulturi.toString());
						} else if (type.equals("actualurl")) {
							requesturl = getCmsILeftService().getShortURL(resulturi.toString());
						}
						
						if (type.equals("shorturl")) {
							mobileurl = getCmsILeftService().getActualURL(resultMobileuri.toString());
						} else if (type.equals("actualurl")) {
							mobileurl = getCmsILeftService().getShortURL(resultMobileuri.toString());
						}
						
						if(type.equals("shorturl")){
							if ((requesturl == null || "".equals(requesturl)) && (mobileurl == null || "".equals(mobileurl))){
								requesturl = "ERROR1:- both are new";	
							}else if ((requesturl != null || !"".equals(requesturl)) && (mobileurl == null || "".equals(mobileurl))){
								requesturl = "ERROR2:- mobile not mapped and web mapped";	
							}else if ((requesturl == null || "".equals(requesturl)) && (mobileurl != null || !"".equals(mobileurl))){
								requesturl = "ERROR3:- mobile mapped and web not mapped";	
							}else{
								requesturl = "ERROR4:- both are already mapped";
							}
						}
						if(type.equals("actualurl")){
							if ((requesturl == null || "".equals(requesturl)) && (mobileurl == null || "".equals(mobileurl))){
								requesturl = "ERROR1:- both are new";	
							}else if ((requesturl != null || !"".equals(requesturl)) && (mobileurl == null || "".equals(mobileurl))){
								requesturl = "ERROR2:- mobile not mapped and web mapped";	
							}else if ((requesturl == null || "".equals(requesturl)) && (mobileurl != null || !"".equals(mobileurl))){
								requesturl = "ERROR3:- mobile mapped and web not mapped";	
							}else{
								requesturl = "ERROR4:- both are already mapped";
							}
						}
						
					}else{
						if (type.equals("shorturl")) {
							requesturl = getCmsILeftService().getActualURL(resulturi.toString());
						} else if (type.equals("actualurl")) {
							requesturl = getCmsILeftService().getShortURL(resulturi.toString());
						}
						
						if ((requesturl == null || "".equals(requesturl)) && type.equals("shorturl")) {
							requesturl = "ERROR:- No actual URL found for this tiny URL";
						}
						if ((requesturl == null || "".equals(requesturl)) && type.equals("actualurl")) {
							requesturl = "ERROR:- No short URL found for this actual URL";
						}
					}
				}else{
					if (type.equals("shorturl")) {
						requesturl = getCmsILeftService().getActualURL(resulturi.toString());
					} else if (type.equals("actualurl")) {
						requesturl = getCmsILeftService().getShortURL(resulturi.toString());
					}
					
					if ((requesturl == null || "".equals(requesturl)) && type.equals("shorturl")) {
						requesturl = "ERROR:- No actual URL found for this tiny URL";
					}
					if ((requesturl == null || "".equals(requesturl)) && type.equals("actualurl")) {
						requesturl = "ERROR:- No short URL found for this actual URL";
					}
					
				}
				response.getWriter().write(requesturl);
				response.getWriter().flush();
				response.getWriter().close();
			} else {
				response.getWriter().write("ERROR5:- Something wrong with the domain");
				response.getWriter().flush();
				response.getWriter().close();
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			response.getWriter().write("ERROR6:- Something wrong with the url");
			response.getWriter().flush();
			response.getWriter().close();
		}
	}

	public void getAllMapping(HttpServletRequest request, HttpServletResponse response) throws IOException {
		try {

			List<CMSILeftObject> cmsObjList = getCmsILeftService().getAllShortUrls();
			JSONArray ja = new JSONArray();

			for (CMSILeftObject obj : cmsObjList) {
				JSONObject jo = new JSONObject();
				jo.put(CMSILeftObject.ID, obj.getId());
				jo.put(CMSILeftObject.CATKEY, obj.getCatkey());
				jo.put(CMSILeftObject.SEOLOCATION, obj.getSeolocation());
				jo.put(CMSILeftObject.OVERRIDELINK, obj.getOverridelink());
				jo.put(CMSILeftObject.S_NAME, obj.getSname());
				jo.put(CMSILeftObject.STATUS, obj.getStatus());
				jo.put(CMSILeftObject.HOSTID, obj.getHostId());
				jo.put(CMSILeftObject.INSTANCEID, obj.getInstanceId());
				jo.put(CMSILeftObject.REDIRECTION_TYPE, obj.getRedirectionType());
				ja.put(jo);
			}
			String output = ja.toString();
			String mimeType = "application/json";
			response.setContentType(mimeType);
			response.setHeader("Content-Type", mimeType + "; charset=UTF-8");
			int pageLength = output.getBytes("UTF-8").length;
			response.setHeader("Content-Length", pageLength + "");
			response.getWriter().write(output);
			response.getWriter().flush();
			response.getWriter().close();

		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
	}

	public void searchAndUpdateShortUrls(HttpServletRequest request, HttpServletResponse response) throws Exception {

		String username = appRequestContext.getEmail();
		String mappingStatus = null;
		String jsonString = request.getParameter("js");
		String option = request.getParameter("o");
		String updateDateTime = new SimpleDateFormat("E, dd MMM yyyy, HH:mm:ss").format(new Date());

		JSONObject jo = new JSONObject();
		String result = "false";
		String output = null;
		
		if(username == null || "".equals(username)){
			mappingStatus = "ERROR: Your session expired, try logging again";
			jo.put("status", false);
			jo.put("message", mappingStatus);
			output = jo.toString();
			if (request.getParameter("callback") != null && !"".equals(request.getParameter("callback"))) {
				output = request.getParameter("callback") + "(" + output + ");";
			}
			String mimeType = "application/json";
			response.setContentType(mimeType);
			response.setHeader("Content-Type", mimeType + "; charset=UTF-8");
			int pageLength = output.getBytes("UTF-8").length;
			response.setHeader("Content-Length", pageLength + "");
			response.getWriter().write(output);
			response.getWriter().flush();
			response.getWriter().close();
		}else{

		if (jsonString != null && !jsonString.trim().equals("")) {
			DBObject dbObject = (DBObject) JSON.parse(jsonString);
			URI shortUri = new URI(dbObject.get("seolocation").toString().replaceAll("\\s","%20"));
			URI actualUri = new URI(dbObject.get("overridelink").toString().replaceAll("\\s","%20"));
			String domainOfActualUri = actualUri.getHost().toString();
			String domainOfShortUri = shortUri.getHost().toString();
			
			domainOfActualUri += handleSpecialHosts(actualUri.toString());
			domainOfShortUri += handleSpecialHosts(shortUri.toString());
			
			String shortUrl = shortUri.toString();
			String actualUrl = actualUri.toString();
			if (domainOfActualUri.equals(domainOfShortUri)) {
				if (!mappingusers.isValidUser(username, domainChannelMap.get(domainOfShortUri))) {
					mappingStatus = "Sorry!! You are not authorized to update mapping for this domain";
					jo.put("Status", false);
					jo.put("message", mappingStatus);
					output = jo.toString();
					if (request.getParameter("callback") != null && !"".equals(request.getParameter("callback"))) {
						output = request.getParameter("callback") + "(" + output + ");";
					}
				} else {
					if(shortUri.toString().contains(".cms")){
						mappingStatus = "ERROR: Cannot update, short url cannot end with .cms";
						jo.put("Status", false);
						jo.put("message", mappingStatus);
						output = jo.toString();
						
					}else if(!actualUri.toString().contains(".cms")){
						mappingStatus = "ERROR: Cannot update, long url should end with .cms";
						jo.put("Status", false);
						jo.put("message", mappingStatus);
						output = jo.toString();
					}else{
					dbObject.put("updatedBy", username);
					dbObject.put("updatedAt", new SimpleDateFormat("E, dd MMM yyyy, HH:mm:ss").format(new Date()));

					logger.info("Mapping updated for seolocation- " + dbObject.get("seolocation")
							+ " and overridelink- " + dbObject.get("overridelink"));
					logger.info("Mapping updated by " + username + " at " + updateDateTime);

					if (option != null && option.equals("1")) {
						result = getCmsILeftService().searchandupdateCMSObject(dbObject, "_id", true).toString();
					} else {
						if ((Boolean) getCmsILeftService().searchandupdateCMSObject(dbObject, "_id", false)) {
							result = "true";
						}
					}
					mappingStatus = "Mapping updated successfully";
					jo.put("result", result);
					jo.put("message", mappingStatus);
					output = jo.toString();
				}
			}
				
			} else {
				mappingStatus = "ERROR: Cannot update!! Urls have different domain";
				jo.put("Status", false);
				jo.put("message", mappingStatus);
				output = jo.toString();
			}
		}
		String mimeType = "application/json";
		response.setContentType(mimeType);
		response.setHeader("Content-Type", mimeType + "; charset=UTF-8");
		int pageLength = output.getBytes("UTF-8").length;
		response.setHeader("Content-Length", pageLength + "");
		response.getWriter().write(output);
		response.getWriter().flush();
		response.getWriter().close();
		}

	}

	/*
	 * @RequestMapping(value = { "/", "/welcome**" }, method =
	 * RequestMethod.GET)
	 */public ModelAndView welcomePage() {

		ModelAndView model = new ModelAndView();
		model.addObject("title", "Spring Security Custom Login Form");
		model.addObject("message", "This is welcome page!");
		model.setViewName("hello");
		return model;

	}

	/*
	 * @RequestMapping(value = "/admin**", method = RequestMethod.GET)
	 */public ModelAndView adminPage() {

		ModelAndView model = new ModelAndView();
		model.addObject("title", "Spring Security Custom Login Form");
		model.addObject("message", "This is protected page!");
		model.setViewName("admin");

		return model;

	}

	/*
	 * @RequestMapping(value = "/login", method = RequestMethod.GET)
	 */public ModelAndView login(HttpServletRequest request, HttpServletResponse response) {

		System.out.println("Inside login controller");
		String error = null;
		String logout = null;

		logger.debug(request.getRequestURI());
		if (request.getParameter("error") != null) {
			error = request.getParameter("error");
		}
		if (request.getParameter("logout") != null) {
			logout = request.getParameter("logout");
		}
		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject("error", "Invalid username and password!");
		}

		if (logout != null) {
			model.addObject("msg", "You've been logged out successfully.");
		}
		model.setViewName("login");

		return model;

	}

	public ModelAndView getResource(HttpServletRequest request, HttpServletResponse response) throws IOException {
		ModelAndView model = new ModelAndView();
		logger.debug("Inside get resource ");

		try {
			String requesturl = request.getServletPath();
			InputStream resourceContent = ctx.getResource(requesturl).getInputStream();
			int length = resourceContent.available();
			String fileName = requesturl.substring(requesturl.lastIndexOf('/') + 1, requesturl.length());
			String mimetype = MimeType
					.getMimeType(fileName.substring(fileName.lastIndexOf('.') + 1, fileName.length()));
			response.setContentType(mimetype);
			response.setContentLength(length);
			logger.debug("Inside get resource " + requesturl + mimetype);
			ServletOutputStream out = response.getOutputStream();
			out.write(IOUtils.toByteArray(resourceContent));
			out.flush();
			out.close();

		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
		}
		return model;
	}

	private String getMsid(String url) {
		String msid = "0";
		if (url.indexOf(".cms") > 0) {
			msid = url.substring(url.lastIndexOf('/') + 1, url.indexOf(".cms"));
		}
		if (isInteger(msid, 10)) {
			return msid;
		}

		return "0";
	}

	public static boolean isInteger(String s, int radix) {
		if (s.isEmpty())
			return false;
		for (int i = 0; i < s.length(); i++) {
			if (i == 0 && s.charAt(i) == '-') {
				if (s.length() == 1)
					return false;
				else
					continue;
			}
			if (Character.digit(s.charAt(i), radix) < 0)
				return false;
		}
		return true;
	}

	private String getSname(String url) {
		String sname = "";
		if (url.indexOf(".cms") == -1) {
			sname = url.substring(url.lastIndexOf('/') + 1, url.length());
		}
		return sname;
	}

	private Map<String, Integer> getHostInstanceId(String actualurl, boolean isLocal) throws URISyntaxException {
		URI uri = null;
		if (actualurl != null && actualurl.startsWith("http://"))
			uri = new URI(actualurl);
		else
			uri = new URI("http://" + actualurl);
		Map<String, Integer> hostInstanceId = cmsILeftService.getHostInstanceId(uri.getHost(), uri.getPort(), isLocal);
		return hostInstanceId;
	}

	private String getHref(String url) {
		return "<a href=\"" + url + "\" target=\"_blank\"" + " style=\"color: #000000\"" + ">" + url + "<a/>";
		// return
		// "<a href=\"#\" id=\"username\" data-type=\"text\" data-pk=\"1\"
		// data-title=\"Enter username\" class=\"editable editable-click\"
		// data-original-title=\"\" title=\"\" style=\"background-color: rgba(0,
		// 0, 0, 0)\">"+url+"<a/>";
	}
	
	private String handleSpecialHosts(String url) {
		
		if(url.contains("timesofindia.indiatimes.com/travel")) {
			return "/travel";
		}else if(url.contains("eisamay.indiatimes.com/eisamaygold")) {
			return "/eisamaygold";
		}else if(url.contains("navbharattimes.indiatimes.com/navbharatgold")) {
			return "/navbharatgold";
		}
		
		return "";
		
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.ctx = applicationContext;

	}

	public CMSILeftService getCmsILeftService() {
		return cmsILeftService;
	}

	public void setCmsILeftService(CMSILeftService cmsILeftService) {
		this.cmsILeftService = cmsILeftService;
	}

	public ConfigObject getConfigObj() {
		return configObj;
	}

	public void setConfigObj(ConfigObject configObj) {
		this.configObj = configObj;
	}

	public MongoRestAPIService getMongoRestAPIService() {
		return mongoRestAPIService;
	}

	public void setMongoRestAPIService(MongoRestAPIService mongoRestAPIService) {
		this.mongoRestAPIService = mongoRestAPIService;
	}

}
