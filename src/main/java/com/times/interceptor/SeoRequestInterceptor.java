package com.times.interceptor;

import java.io.IOException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

import javax.annotation.PostConstruct;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpException;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.times.constants.SeoConstants;
import com.times.dao.ValidUserDao;
import com.times.model.AppRequestContext;
import com.times.users.ValidUsers;
import com.times.utility.ExternalHttpUtil;

/**
 * 
 * @author pankaj.kumar1
 */

public class SeoRequestInterceptor implements HandlerInterceptor {

	@Autowired
	private AppRequestContext appRequestContext;

	@Autowired
	private ValidUserDao validUserDao;

	@Override
	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {

	}

	/**
	 * {@link com.times.utility.Schedulers#updateValidUsers()}
	 * {@link #getSsoEmail(String)}
	 */
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object arg2) throws Exception {

		String requestesuri = request.getRequestURI().trim();
		
		if (!setSsoEmail(request, response, requestesuri)) {
			if (request.isSecure()) {
				response.sendRedirect( SeoConstants.LDAP_HOST + SeoConstants.LOGIN_URI + SeoConstants.HTTP_SECURE
						+ request.getServerName() + SeoConstants.REDIRECT_URI);
			} else {
				response.sendRedirect(SeoConstants.LDAP_HOST + SeoConstants.LOGIN_URI + SeoConstants.HTTP
						+ request.getServerName() + SeoConstants.REDIRECT_URI);
			}

			return false;
		}

		String email = appRequestContext.getEmail();
		response.setCharacterEncoding(SeoConstants.UTF_8);

		if (SeoConstants.validAPISet.contains(requestesuri)) {
			return true;
		}
		
		if (ValidUsers.seoPanelUsers.contains(email)) {
			return true;
		} else {
			response.sendRedirect(SeoConstants.PERMISSION_DENIED_URI);
		}

		
		return false;

	}

	private boolean setSsoEmail(HttpServletRequest request, HttpServletResponse response, String requestesuri)
			throws IOException, JSONException {
		String accessToken = "";

		// 1. First get from url params
		if (SeoConstants.REDIRECT_URI.equalsIgnoreCase(requestesuri)) {
			String tempCode = request.getParameter(SeoConstants.TEMPCODE);

			if (tempCode != null) {
				String extData = null;
				JSONObject params = new JSONObject();
				params.put(SeoConstants.TEMPCODE, tempCode.trim());
				try {
					extData = ExternalHttpUtil
							.getExtData_PostCall(SeoConstants.LDAP_HOST + SeoConstants.ACCESS_CODE_URI, params);
				} catch (HttpException e) {
					e.printStackTrace();
				}

				int code = (Integer) new JSONObject(extData).get(SeoConstants.CODE);
				if (code == 200) {
					accessToken = (String) ((JSONObject) new JSONObject(extData).get(SeoConstants.DATA))
							.get(SeoConstants.ACCESSTOKEN);
					response.setHeader("SET-COOKIE",
							"SEOTOKEN=" + accessToken + "; HttpOnly;Expires=" + addHours(24 * 30) + ";path=/");
				} else {
					return false;
				}

			}
		}
		// 2. Fetch from cookie
		if (accessToken.isEmpty() || accessToken == null) {

			Cookie[] cookies = request.getCookies();
			if (cookies != null) {
				for (Cookie cookie : cookies) {
					if (cookie.getName().equals(SeoConstants.COOKIE_NAME)) {
						accessToken = cookie.getValue();
					}
				}
			}
		}

		// 3. if still acessToken is empty, then redirect
		if (accessToken.isEmpty() || accessToken == null) {
			return false;
		}

		String ssoemail = getSsoEmail(accessToken);
		if (ssoemail == null) {
			return false;
		}
		appRequestContext.setRequest(request);
		appRequestContext.setEmail(ssoemail);
		return true;
	}

	private String getSsoEmail(String accessToken) throws IOException, JSONException {
		String extData1 = null;
		String ssoemail = "";
		JSONObject params1 = new JSONObject();
		params1.put(SeoConstants.ACCESSTOKEN, accessToken);
		try {
			extData1 = ExternalHttpUtil.getExtData_PostCall(SeoConstants.LDAP_HOST + SeoConstants.USER_DETAILS_URI,
					params1);
		} catch (HttpException e) {
			e.printStackTrace();
		}
		int code = (Integer) new JSONObject(extData1).get(SeoConstants.CODE);
		if (code == 200) {
			ssoemail = (String) ((JSONObject) new JSONObject(extData1).get(SeoConstants.DATA)).get(SeoConstants.EMAIL);
		} else {
			return null;
		}
		return ssoemail;
	}

	public String addHours(int hrs) {
		DateFormat formatter = new SimpleDateFormat("E,dd-MMM-yyyy HH:mm:ss");
		formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.HOUR_OF_DAY, hrs);
		String expiry = formatter.format(cal.getTime());

		return expiry;
	}

	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
			throws Exception {
		// TODO Auto-generated method stub

	}

	/**
	 * This method loads the valid user for seo panel at startup.
	 */
	@PostConstruct
	public void loadValidUsers() {
		validUserDao.updateValidUserList();

	}

}
