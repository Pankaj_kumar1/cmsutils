package com.times.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.model.CMSILeftObject;
import com.times.service.MongoRestAPIService;
import com.times.utility.CMSUtility;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

public class CMSILeftTagHandlerDAOImpl extends JdbcDaoSupport implements CMSILeftTagHandlerDAO{

	private final static Logger logger = LoggerFactory.getLogger(CMSILeftTagHandlerDAOImpl.class);
	
	public static final String CMS_ILEFT_COLLECTION = "virtualurlmapping";
	
	MongoRestAPIService mongoRestAPIService;
	
	@Override
	public boolean addCMSIleftInfo(CMSILeftObject cmsILeftObj) throws Exception{
		DBObject cmsDBObj = getDBObjectByCMSILeftObj(cmsILeftObj);
		try {
			return getMongoRestAPIService().insert(CMS_ILEFT_COLLECTION, cmsDBObj);

		} catch (Exception e) {
			logger.error("Generic Exception caught while adding virtual mapping  ", e);
			throw e;
		} finally {
			
		}
	}
	
	public boolean updateCMSIleftInfo(CMSILeftObject cmsILeftObj, String key) throws Exception{
		DBObject cmsDBObj = getDBObjectByCMSILeftObj(cmsILeftObj);
		try {
			DBObject query = new BasicDBObject();
			query.put(key, cmsDBObj.get(key));
			
			return getMongoRestAPIService().findAndUpdate(CMS_ILEFT_COLLECTION, query, cmsDBObj);

		} catch (Exception e) {
			logger.error("Generic Exception caught while adding virtual mapping  ", e);
			throw e;
		} finally {
			
		}
	}
	
	public Object searchandupdateCMSIleftInfo(DBObject cmsDBObj, String key, boolean delete) throws Exception{
		try {
			cmsDBObj  = getDBObjectByCMSILeftObj(getCMSILeftObjFromBson(cmsDBObj));
			DBObject query = new BasicDBObject(key,new ObjectId(cmsDBObj.get(key).toString()));
			cmsDBObj.removeField(CMSILeftObject.ID);
			if(delete){
				return getMongoRestAPIService().delete(CMS_ILEFT_COLLECTION, query);
			}
			else{
				return getMongoRestAPIService().findAndUpdate(CMS_ILEFT_COLLECTION, query, cmsDBObj);
			}

		} catch (Exception e) {
			logger.error("Generic Exception caught while adding virtual mapping  ", e);
			throw e;
		} finally {
			
		}
	}
	
	public boolean removeCMSIleftInfo(CMSILeftObject cmsILeftObj, String key) throws Exception{
		DBObject cmsDBObj = getDBObjectByCMSILeftObj(cmsILeftObj);
		try {
			DBObject query = new BasicDBObject();
			query.put(key, cmsDBObj.get(key));
			
			return getMongoRestAPIService().delete(CMS_ILEFT_COLLECTION, query);

		} catch (Exception e) {
			logger.error("Generic Exception caught while adding virtual mapping  ", e);
			throw e;
		} finally {
			
		}
	}
	
	public CMSILeftObject getCMSIleftInfo(String seolocation) throws Exception {
		
		CMSILeftObject cmsObj = null;
		DBObject result= new BasicDBObject();
		try {
			DBObject query=new BasicDBObject();
			
			query.put(CMSILeftObject.SEOLOCATION, seolocation.toLowerCase());

			result = getMongoRestAPIService().get(CMS_ILEFT_COLLECTION, query);
			cmsObj = getCMSILeftObjFromBson(result);
			
		} catch (Exception e) {
			logger.error("Generic Exception caught while getting virtual mapping  ", e);
			throw e;
		} finally {
		}
		
		return cmsObj;
	}
	
	public List<Map<String, String>> getCMSIleftInfo() throws Exception {
		
		Map<String, String> iLeftMapSEOtoOL = new HashMap<String, String>();
		Map<String, String> iLeftMapOLtoSEO = new HashMap<String, String>();
		
		List<Map<String, String>> iLeftMaps=new ArrayList<Map<String,String>>();
		
		DBObject cmsDBObject= new BasicDBObject();
		DBObject query = new BasicDBObject();		
		String key=null;
		String value=null;
		try {
			
			List<DBObject> dbList = getMongoRestAPIService().getList(CMS_ILEFT_COLLECTION, query);
			if(dbList != null){
				Iterator<DBObject> iter = dbList.iterator();
				while( iter.hasNext() ){
					cmsDBObject = iter.next();
					if(CMSUtility.getIntVal(String.valueOf(cmsDBObject.get(CMSILeftObject.REDIRECTION_TYPE))) == 0){
						key = String.valueOf(cmsDBObject.get(CMSILeftObject.SEOLOCATION));
						value = String.valueOf(cmsDBObject.get(CMSILeftObject.OVERRIDELINK));
						iLeftMapSEOtoOL.put(key, value);
						iLeftMapOLtoSEO.put(value, key);
					}
				}
			}
			
		} catch (Exception e) {
			logger.error("Generic Exception caught while adding Subscription ", e);
			throw e;
		} finally {
		}
		
		iLeftMaps.add(iLeftMapSEOtoOL);
		iLeftMaps.add(iLeftMapOLtoSEO);
		
		return iLeftMaps;
	}
	
	public CMSILeftObject getCMSIleftInfoFromOverLink (String seolocation) throws Exception {
		
		CMSILeftObject cmsObj = null;
		DBObject result= new BasicDBObject();
		try {
			DBObject query=new BasicDBObject();
			
			query.put(CMSILeftObject.OVERRIDELINK, seolocation.toLowerCase());

			result = getMongoRestAPIService().get(CMS_ILEFT_COLLECTION, query);
			cmsObj = getCMSILeftObjFromBson(result);
			
		} catch (Exception e) {
			logger.error("Generic Exception caught while adding Subscription ", e);
			throw e;
		} finally {
		}
		
		return cmsObj;
	}

	private DBObject getDBObjectByCMSILeftObj(CMSILeftObject cmsILeftObj) {
		DBObject cmsDBObject = new BasicDBObject();
		cmsDBObject.put(CMSILeftObject.ID, cmsILeftObj.getId());
		cmsDBObject.put(CMSILeftObject.CATKEY, cmsILeftObj.getCatkey());
		cmsDBObject.put(CMSILeftObject.S_NAME, cmsILeftObj.getSname());
		cmsDBObject.put(CMSILeftObject.S_NAMESEO, cmsILeftObj.getSnameseo());
		cmsDBObject.put(CMSILeftObject.OVERRIDELINK, cmsILeftObj.getOverridelink());
		cmsDBObject.put(CMSILeftObject.SEOLOCATION, cmsILeftObj.getSeolocation());
		cmsDBObject.put(CMSILeftObject.HOSTID, cmsILeftObj.getHostId());
		cmsDBObject.put(CMSILeftObject.INSTANCEID, cmsILeftObj.getInstanceId());
		cmsDBObject.put(CMSILeftObject.REDIRECTION_TYPE,0);
		cmsDBObject.put(CMSILeftObject.MAPPEDBY, cmsILeftObj.getMappedBy());
		cmsDBObject.put(CMSILeftObject.MAPPEDAT, cmsILeftObj.getMappedAt());
		cmsDBObject.put(CMSILeftObject.UPDATEDBY, cmsILeftObj.getUpdatedBy());
		cmsDBObject.put(CMSILeftObject.UPDATEDAT, cmsILeftObj.getUpdatedAt());
		//cmsDBObject.put(CMSILeftObject.STATUS, cmsILeftObj.getStatus());
		
		if(cmsILeftObj.getRedirectionType()>=0){
			cmsDBObject.put(CMSILeftObject.REDIRECTION_TYPE,cmsILeftObj.getRedirectionType());
		}
		return cmsDBObject;
	}

	private CMSILeftObject getCMSILeftObjFromBson(DBObject cmsDBObject) throws Exception {
		CMSILeftObject cmsILeftObj = new CMSILeftObject();
		if (cmsDBObject != null) {
			cmsILeftObj.setId(cmsDBObject.get(CMSILeftObject.ID));
			cmsILeftObj.setCatkey(String.valueOf(cmsDBObject.get(CMSILeftObject.CATKEY)));
			cmsILeftObj.setSname(String.valueOf(cmsDBObject.get(CMSILeftObject.S_NAME)));
			cmsILeftObj.setSnameseo(String.valueOf(cmsDBObject.get(CMSILeftObject.S_NAMESEO)));
			cmsILeftObj.setSeolocation(String.valueOf(cmsDBObject.get(CMSILeftObject.SEOLOCATION)).replaceAll("\\s","%20"));
			cmsILeftObj.setOverridelink(String.valueOf(cmsDBObject.get(CMSILeftObject.OVERRIDELINK)).replaceAll("\\s","%20"));
			cmsILeftObj.setHostId(CMSUtility.getIntVal((String.valueOf(cmsDBObject.get(CMSILeftObject.HOSTID)))));
			cmsILeftObj.setInstanceId(CMSUtility.getIntVal((String.valueOf(cmsDBObject.get(CMSILeftObject.INSTANCEID)))));
			cmsILeftObj.setRedirectionType(0);
			//cmsILeftObj.setStatus(CMSUtility.getIntVal(String.valueOf(cmsDBObject.get(CMSILeftObject.STATUS))));
			cmsILeftObj.setMappedBy((String.valueOf(cmsDBObject.get(CMSILeftObject.MAPPEDBY))));
			cmsILeftObj.setMappedAt((String.valueOf(cmsDBObject.get(CMSILeftObject.MAPPEDAT))));
			cmsILeftObj.setUpdatedBy(String.valueOf(cmsDBObject.get(CMSILeftObject.UPDATEDBY)));
			cmsILeftObj.setUpdatedAt(String.valueOf(cmsDBObject.get(CMSILeftObject.UPDATEDAT)));
			if(cmsDBObject.containsField(CMSILeftObject.REDIRECTION_TYPE)){
				cmsILeftObj.setRedirectionType(CMSUtility.getIntVal(String.valueOf(cmsDBObject.get(CMSILeftObject.REDIRECTION_TYPE))));
			}
			return cmsILeftObj;
		} else {
			logger.debug("Passed in BSON object for CMSILeftObject is null.");
			return null;
		}
	}
	
	public List<CMSILeftObject> getAllShortUrls() throws Exception {
		
		List<CMSILeftObject> iLeftList=new ArrayList<CMSILeftObject>();
		
		DBObject cmsDBObject= new BasicDBObject();
		DBObject query = new BasicDBObject();		
		try {
			
			List<DBObject> dbList = getMongoRestAPIService().getList(CMS_ILEFT_COLLECTION, query);
			if(dbList != null){
				Iterator<DBObject> iter = dbList.iterator();
				while( iter.hasNext() ){
					cmsDBObject = iter.next();
					if(!cmsDBObject.containsField("virtualfolderid")){
					CMSILeftObject cmsILeftObj = getCMSILeftObjFromBson(cmsDBObject);
					iLeftList.add(cmsILeftObj);
					}
				}
			}
			
		} catch (Exception e) {
			logger.error("Generic Exception caught while getAllShortUrls ", e);
			throw e;
		} finally {
		}
		return iLeftList;
	}
	
	public String getActualURL(String seolocation) throws Exception {
		
		CMSILeftObject cmsObj = null;
		DBObject result= new BasicDBObject();
		String actualURL = null;
		try {
			DBObject query=new BasicDBObject();
			
			query.put(CMSILeftObject.SEOLOCATION, seolocation);

			result = getMongoRestAPIService().get(CMS_ILEFT_COLLECTION, query);
			if(result!=null){
				cmsObj = getCMSILeftObjFromBson(result);
				actualURL = cmsObj.getOverridelink();
			}
			
		} catch (Exception e) {
			logger.error("Generic Exception caught while adding Subscription ", e);
			throw e;
		} finally {
		}
		
		return actualURL;
	}
	
	public String getShortURL(String overridelink) throws Exception {
		
		CMSILeftObject cmsObj = null;
		DBObject result= new BasicDBObject();
		String shortURL = null;
		try {
			DBObject query=new BasicDBObject();
			
			query.put(CMSILeftObject.OVERRIDELINK, overridelink);

			result = getMongoRestAPIService().get(CMS_ILEFT_COLLECTION, query);
			if(result!=null){
				cmsObj = getCMSILeftObjFromBson(result);
				shortURL = cmsObj.getSeolocation();
			}
			
		} catch (Exception e) {
			logger.error("Generic Exception caught while adding Subscription ", e);
			throw e;
		} finally {
		}
		
		return shortURL;
	}

	public MongoRestAPIService getMongoRestAPIService() {
		return mongoRestAPIService;
	}

	public void setMongoRestAPIService(MongoRestAPIService mongoRestAPIService) {
		this.mongoRestAPIService = mongoRestAPIService;
	}

	@Override
	public Map getHostInfo(String hostName, int port) {
		try{
			if(port == -1)
				port = 80;
			
			Map<String, Object> instanceMap = getJdbcTemplate().queryForMap("SELECT instanceId from HostHeader where header = '"+hostName+"'");
			String instanceId = String.valueOf(instanceMap.get("instanceId"));
			return getJdbcTemplate().queryForMap("SELECT hostId,instanceId from host where instanceId = '"+instanceId +"'"+ "AND LocalPort= '"+port+"'");
			
		} catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}

	@Override
	public Map getHeaderInfo(String hostName) {
		try{
			return getJdbcTemplate().queryForMap("SELECT top 1 hostId,instanceId from HostHeader where header = '"+hostName+"'"+"order by instanceId desc");
		} catch( Exception ex){
			ex.printStackTrace();
		}
		return null;
	}
	
	@Override
	public Map<String, Object> getDomain(int hostid, int instanceId) {
		try{
			return getJdbcTemplate().queryForMap("SELECT header from HostHeader where hostid = "+hostid+" and instanceId = " + instanceId + " and isDefault=1");
		} catch( Exception ex){
			ex.printStackTrace();
		}
		return null;
	}

	@Override
	public int getMappingCount(DBObject obj) {
		return mongoRestAPIService.getCount(CMS_ILEFT_COLLECTION, obj);
		
	}
	
}
