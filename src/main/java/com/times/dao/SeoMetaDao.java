package com.times.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import sun.security.action.PutAllAction;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.controller.SeoMetaController;
import com.times.model.SeoMeta;
import com.times.service.MongoRestAPIService;

@Repository
public class SeoMetaDao {
	private static final Logger logger = LoggerFactory.getLogger(SeoMetaDao.class);
	@Autowired
	MongoRestAPIService mongoRestAPIService;
	
	private final String MONGO_COLLECTION = "urlSeoMapping";
	
	public boolean insert(SeoMeta seoMeta) {
		if (StringUtils.isEmpty(seoMeta.getUri()) || StringUtils.isEmpty(String.valueOf(seoMeta.getHostid())))  {
			return false;
		}
		logger.debug("insert method called");
		DBObject seoMetaDBOject = new BasicDBObject();
		DBObject seoMetaDBOject1 = new BasicDBObject();
		BasicDBObject seoMetaDBOject2 = new BasicDBObject();
		seoMetaDBOject.put("URI", seoMeta.getUri());
		seoMetaDBOject.put("Channel", seoMeta.getChannel());
		seoMetaDBOject.put("HostId", seoMeta.getHostid());
		seoMetaDBOject.put("Meta-description", seoMeta.getLongdesc());
		seoMetaDBOject.put("Alt-title", seoMeta.getShortdesc());
		seoMetaDBOject.put("H1-title", seoMeta.getTitle());
		seoMetaDBOject.put("Atf-content", seoMeta.getAtfContent());
		seoMetaDBOject.put("Btf-content", seoMeta.getBtfContent());
		seoMetaDBOject.put("Canonical-url", seoMeta.getCanonicalUrl());
		seoMetaDBOject.put("m-url", seoMeta.getmUrl());
		seoMetaDBOject.put("amp-url", seoMeta.getAmpUrl());
		seoMetaDBOject.put("meta-keyws", seoMeta.getMetakeywords());
//		seoMetaDBOject.put("Mode", seoMeta.getMode());
		
		try {
			seoMetaDBOject.put("rel_map", getDBObjectByTitleAndUrl(seoMeta.getTitle_url_map()));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if("update".equals(seoMeta.getAction())){
			logger.info("update is called for " + seoMetaDBOject);
			seoMetaDBOject.put("lastUpdatedAt", new SimpleDateFormat("E, dd MMM yyyy, HH:mm:ss").format(new Date()));
			seoMetaDBOject.put("lastUpdatedBy", seoMeta.getUserName());
		}
		if("create".equals(seoMeta.getAction())){
			logger.info("insert is called for " + seoMetaDBOject);
		}
		if("upload".equals(seoMeta.getAction())){
			seoMetaDBOject.put("lastUpdatedAt", new SimpleDateFormat("E, dd MMM yyyy, HH:mm:ss").format(new Date()));
			seoMetaDBOject.put("lastUpdatedBy", seoMeta.getUserName());
			logger.info("upload is called for " + seoMetaDBOject);
		}
			seoMetaDBOject1.put("CreatedAt", new SimpleDateFormat("E, dd MMM yyyy, HH:mm:ss").format(new Date()));
			seoMetaDBOject1.put("CreatedBy", seoMeta.getUserName());
			
			seoMetaDBOject2.append("$set",seoMetaDBOject);
			seoMetaDBOject2.append("$setOnInsert", seoMetaDBOject1);
		
		DBObject selectQuery = new BasicDBObject();
		selectQuery.put("URI", seoMeta.getUri());
		selectQuery.put("HostId", seoMeta.getHostid());
		
		//Boolean findStatus = mongoRestAPIService.update(MONGO_COLLECTION, selectQuery, seoMetaDBOject2, true);
		Boolean findStatus = mongoRestAPIService.updateWithPostWithEncodeQuery(MONGO_COLLECTION, selectQuery, seoMetaDBOject2, true);
		logger.debug("inserted | updated " + findStatus);
		return findStatus;
	}

	/*
	 * This function returns gadgetList based on searched URI
	 */
	public List<DBObject> getUriList(SeoMeta seoMeta) {
		logger.debug("getUriList method called");
		DBObject selectQuery = new BasicDBObject();
		selectQuery.put("URI", seoMeta.getUri());
		selectQuery.put("HostId", seoMeta.getHostid());
		logger.info("query for " + selectQuery);
		List<DBObject> seoMetaList = mongoRestAPIService.getListWithEncodeQuery(MONGO_COLLECTION, selectQuery);
		logger.info("Data rececived " + seoMetaList);
		return seoMetaList;
	}
	
	/*
	 * This method checks if meta for the combination (URI,Channel) is present or not
	 */
	
   public boolean isMetaPresent(String uri, String channel){
		
		DBObject dbObj = new BasicDBObject();
		dbObj.put("URI", uri);
		dbObj.put("Channel", channel);
		
		if(mongoRestAPIService.get(MONGO_COLLECTION, dbObj) !=null){
			return true;
		}else{
			return false;
		}
		
	}
   
   public ArrayList<DBObject> getDBObjectByTitleAndUrl(Map title_url_map) throws Exception{
	   ArrayList<DBObject> dboList = new ArrayList<>();
	   if (title_url_map != null && title_url_map.size() > 0) {
		   Iterator titles = title_url_map.keySet().iterator();
		   while (titles.hasNext()) {
			   DBObject tpDBObject = new BasicDBObject();
			   String title = (String) titles.next();
			   tpDBObject.put("title", title);
			   tpDBObject.put("url", title_url_map.get(title));
			   dboList.add(tpDBObject);
		   }

	   }
	   return dboList;
   }


}
