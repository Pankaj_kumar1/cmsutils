package com.times.dao;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.times.service.MongoRestAPIService;
import com.times.users.ValidUsers;

@Repository
public class ValidUserDao {
	
	private static final Logger logger = LoggerFactory.getLogger(ValidUserDao.class);
	
	@Autowired
	MongoRestAPIService mongoRestAPIService;
	
	private static String validUserCollection = "seoPanelUsers";
	
	public void updateValidUserList() {
		 List<DBObject> userList =  new ArrayList<DBObject>();
		 DBObject queryObj = new BasicDBObject();
		 queryObj.put("status", "active");
		 userList = mongoRestAPIService.getList(validUserCollection, queryObj);
		 logger.info("valid user fetched with size {}", userList.size());
		 
		 for(DBObject obj : userList) {
			 ValidUsers.seoPanelUsers.add(obj.get("username").toString());
		 }
		
	}

}
