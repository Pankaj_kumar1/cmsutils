package com.times.dao;

import java.util.List;
import java.util.Map;

import com.mongodb.DBObject;
import com.times.model.CMSILeftObject;

public interface CMSILeftTagHandlerDAO {

	public boolean addCMSIleftInfo(CMSILeftObject cmsILeftObj) throws Exception;
	public boolean updateCMSIleftInfo(CMSILeftObject cmsILeftObj, String key) throws Exception;
	public boolean removeCMSIleftInfo(CMSILeftObject cmsILeftObj, String key) throws Exception;
	public CMSILeftObject getCMSIleftInfo(String seoname) throws Exception;
	public CMSILeftObject getCMSIleftInfoFromOverLink(String seoname) throws Exception;
	public List<Map<String, String>> getCMSIleftInfo() throws Exception;
	public List<CMSILeftObject> getAllShortUrls() throws Exception;
	public String getActualURL(String seolocation) throws Exception;
	public String getShortURL(String overridelink) throws Exception;
	public Object searchandupdateCMSIleftInfo(DBObject cmsDBObj, String key, boolean searchOnly) throws Exception;
	public Map<String,Integer> getHostInfo(String hostName, int port);
	public Map<String,Integer> getHeaderInfo(String hostName);
	public Map<String,Object> getDomain(int hostid, int instanceId); 
	public int getMappingCount(DBObject obj);
}
