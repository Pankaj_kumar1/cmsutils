
package com.times.utility;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.util.UriUtils;




public class ExternalHttpUtil {

	 static int httpConnectTimeout = 50000;
	 static int httpReadTimeout = 50000;
	 private static final Logger logger = LoggerFactory.getLogger(ExternalHttpUtil.class);
	 
    public static String getExtData_PostCall(String strURL, JSONObject json) throws HttpException, IOException{
          String extData = null;
          HttpClient hClient = HttpClients.custom().setRetryHandler(new DefaultHttpRequestRetryHandler(3, false)).build();
		
		RequestConfig config = RequestConfig.custom().setMaxRedirects(2)
                      .setConnectTimeout(httpConnectTimeout).setSocketTimeout(httpReadTimeout).build();

          HttpRequestBase gMethod = null;
          HttpResponse hResponse = null;
          try{
                
                if(strURL.indexOf("?") != -1){
                      strURL = strURL.substring(0,strURL.indexOf("?")) + UriUtils.encodeQuery(strURL.replace("/", " ").substring(strURL.indexOf("?")),  "UTF-8");
                }
                
                gMethod = new HttpPost(strURL);
                gMethod.setConfig(config);
                StringEntity e = new StringEntity(json.toString(), StandardCharsets.UTF_8);
                e.setContentType("application/json");
                e.setContentEncoding(StandardCharsets.UTF_8.toString()); 
                ((HttpPost)gMethod).setEntity(e);
                
                hResponse = hClient.execute(gMethod);
                int status = hResponse.getStatusLine().getStatusCode();
                if (status == HttpStatus.SC_OK) {
                      extData = EntityUtils.toString(hResponse.getEntity(), StandardCharsets.UTF_8);
                      return extData;
                }
          }catch(Exception e){
               // logger.error(strURL + " - Exception Occured in get the external data Exception : " + e.getMessage(), e);             
          }finally{
                HttpClientUtils.closeQuietly(hResponse);
                HttpClientUtils.closeQuietly(hClient);
          }
          return null;
    }
    
    public static String getExtData(String strURL) throws HttpException, IOException{
        String extData = null;
        HttpClient hClient = HttpClients.custom().setRetryHandler(new DefaultHttpRequestRetryHandler(3, false)).build();
        RequestConfig config = RequestConfig.custom().setMaxRedirects(2)
                    .setConnectTimeout(httpConnectTimeout).setSocketTimeout(httpReadTimeout).build();

        HttpRequestBase gMethod = null;
        HttpResponse hResponse = null;
        try{
              
              if(strURL.indexOf("?") != -1){
                    strURL = strURL.substring(0,strURL.indexOf("?")) + UriUtils.encodeQuery(strURL.replace("/", " ").substring(strURL.indexOf("?")),  "UTF-8");
              }
              
              gMethod = new HttpGet(strURL);
              gMethod.setConfig(config);
              hResponse = hClient.execute(gMethod);
              int status = hResponse.getStatusLine().getStatusCode();
              if (status == HttpStatus.SC_OK) {
                    extData = new BasicResponseHandler().handleResponse(hResponse);
                    return extData;
              }
        }catch(Exception e){
             // logger.error(strURL + " - Exception Occured in get the external data Exception : " + e.getMessage(), e);
              
        }finally{
              HttpClientUtils.closeQuietly(hResponse);
              HttpClientUtils.closeQuietly(hClient);
        }
        return null;
       }
    
    public static String getExtDataSlike(String strURL) throws HttpException, IOException{
        String extData = null;
        HttpClient hClient = HttpClients.custom().setRetryHandler(new DefaultHttpRequestRetryHandler(3, false)).build();
        RequestConfig config = RequestConfig.custom().setMaxRedirects(2)
                    .setConnectTimeout(httpConnectTimeout).setSocketTimeout(httpReadTimeout).build();

        HttpRequestBase gMethod = null;
        HttpResponse hResponse = null;
        try{
                          
              gMethod = new HttpGet(strURL);
              gMethod.setConfig(config);
              hResponse = hClient.execute(gMethod);
              int status = hResponse.getStatusLine().getStatusCode();
              if (status == HttpStatus.SC_OK) {
                    extData = new BasicResponseHandler().handleResponse(hResponse);
                    return extData;
              }
        }catch(Exception e){
             // logger.error(strURL + " - Exception Occured in get the external data Exception : " + e.getMessage(), e);
              
        }finally{
              HttpClientUtils.closeQuietly(hResponse);
              HttpClientUtils.closeQuietly(hClient);
        }
        return null;

    }

}

