/*
 * Created on Dec 3, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.times.utility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Administrator
 * 
 *         TODO To change the template for this generated type comment go to
 *         Window - Preferences - Java - Code Style - Code Templates
 */
public class CMSUtility {

	public static final String CMS_DEFAULT_TEMPLATE = "/default";
	public static final String CMS_EXT = ".cms";
	public static final String CMS_PARAMS = "/params/";
	public static final Logger logger = LoggerFactory.getLogger(CMSUtility.class);

	public static boolean isValidInteger(String intValue) {
		//match a number with optional starting '-' character
		return intValue.matches("-?\\d+");
	}

	public static int getIntVal(String intValue, int defaultValue) {
		int returnValue = defaultValue;
		try {
			if (intValue != null && !intValue.trim().isEmpty() && !"null".equals(intValue.trim())) {
				returnValue = Integer.parseInt(intValue);
			}
		} catch (Exception e) {
			logger.debug("Not an integer: {}", intValue);
			returnValue = defaultValue;
		}
		return returnValue;
	}
	
	public static int getIntVal(String intValue) {
		return getIntVal(intValue, 0);
	}

	
}
