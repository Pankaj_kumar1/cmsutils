package com.times.utility;

/**
 * 
 */

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.util.StringUtils;


/**
 * This class is used as XLSX reader for the provided details.
 * 
 * @author Rajeev Khatri
 * 
 */
public class XlsxReader {

	private final static Logger logger = Logger.getLogger(XlsxReader.class);

	/**
	 * creates an {@link HSSFWorkbook} the specified OS filename.
	 */
	public static HSSFWorkbook readFile(InputStream file) throws IOException {

		return new HSSFWorkbook(file);
	}

	public static XSSFWorkbook readXLSXFile(InputStream file) throws IOException {

		return new XSSFWorkbook(file);

	}

	public static String[] COLUMN = {};




	/**
	 * @param file
	 * @return
	 */
	public static List<Map<String, Object>> getXLSRecordsList(InputStream file, String[] column) {
		COLUMN = column;
		List<Map<String, Object>> list = null;

		try {
			if (file != null ) {
				HSSFWorkbook wb = readFile(file);

				HSSFSheet sheet = wb.getSheetAt(0);
				int rows = sheet.getPhysicalNumberOfRows();

				list = getRows(sheet, rows);
				logger.debug("no of rows : " + rows);
				if (list != null && list.size() > 0) {
					logger.debug("readed... , size : " + list.size());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}


	public static List<Map<String, Object>> getXLSXRecordsList(InputStream file,  String[] column) {
		List<Map<String, Object>> list = null;

		try {
			COLUMN = column;
			if (file != null ) {

				XSSFWorkbook wb = readXLSXFile(file);

				XSSFSheet sheet = wb.getSheetAt(0);
				int rows = sheet.getPhysicalNumberOfRows();

				list = getRows(sheet, rows);
				logger.debug("no of rows : " + rows);
				if (list != null && list.size() > 0) {
					logger.debug("readed... , size : " + list.size());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}



	public static XSSFWorkbook updateXLSXRecordsList(InputStream file,  String[] column , HttpServletResponse response, 
			Map<String, String> resultMap) {
		List<Map<String, Object>> list = null;

		try {
			COLUMN = column;

			if (file != null ) {

				XSSFWorkbook wb = readXLSXFile(file);
				CellStyle style = wb.createCellStyle();
				style.setFillForegroundColor(IndexedColors.GREEN.getIndex());
				style.setFillPattern(CellStyle.SOLID_FOREGROUND);


				XSSFSheet sheet = wb.getSheetAt(0);
				int rows = sheet.getPhysicalNumberOfRows();

				checkRowupdated(sheet, rows, style, resultMap);
				logger.debug("no of rows : " + rows);
				return wb;

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}


	/**
	 * @param file
	 * @return
	 */
	public static HSSFSheet getWorkSheet(InputStream file) {
		HSSFWorkbook hssfWorkbook = null;
		HSSFSheet sheet = null;
		try {
			if (file != null) {
				hssfWorkbook = XlsxReader.readFile(file);
				sheet = hssfWorkbook.getSheetAt(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return sheet;
	}

	/**
	 * This method is used to get List of {@link Map<String, String>} by
	 * reading row by row of excel HSSFSheet sheet.
	 * 
	 * @param sheet
	 * @return
	 */
	public static List<Map<String, Object>> getRows(HSSFSheet sheet,  int noOfRows) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>(noOfRows);

		int rows = sheet.getPhysicalNumberOfRows();

		for (int r = 1; r < rows; r++) {
			HSSFRow row = sheet.getRow(r);
			if (row != null) {
				Map<String, Object> map = getRow(sheet.getRow(r));
				if (isContentInMap(map)) {
					list.add(map);
				}
			} else {
				continue;
			}
		}
		return list;
	}

	/**
	 * This method is used to get List of {@link Map<String, String>} by
	 * reading row by row of excel XSSFSheet sheet.
	 * 
	 * @param sheet
	 * @return
	 */
	public static List<Map<String, Object>> getRows(XSSFSheet sheet,  int noOfRows) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>(noOfRows);

		int rows = sheet.getPhysicalNumberOfRows();

		for (int r = 1; r < rows; r++) {
			XSSFRow row = sheet.getRow(r);
			if (row != null) {
				Map<String, Object> map = getRow(sheet.getRow(r));
				if (isContentInMap(map)) {
					list.add(map);
				}
			} else {
				continue;
			}
		}
		return list;
	}


	/**
	 * 
	 * This API check if each row in xls, updated in db (exist in resultMap).
	 * if row updated, add style to row with green backgorund.
	 * @param sheet
	 * @param noOfRows
	 * @param style
	 * @param resultMap
	 * @return
	 */
	public static List<Map<String, Object>> checkRowupdated(XSSFSheet sheet,  int noOfRows, CellStyle style, Map<String, String> resultMap) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>(noOfRows);

		int rows = sheet.getPhysicalNumberOfRows();

		for (int r = 1; r < rows; r++) {
			XSSFRow row = sheet.getRow(r);
			if (row != null) {
				Map<String, Object> map = getRow(sheet.getRow(r));
				if (!StringUtils.isEmpty(map.get("URI")) && resultMap.containsKey(map.get("URI").toString())) {	

					addStyleToCell(row,style);

				}
			} else {
				continue;
			}
		}
		return list;
	}



	/**
	 * This method is used to get for one HSSFRow Row.
	 * 
	 * @param row
	 * @return
	 */
	private static Map<String, Object> getRow(HSSFRow row) {
		Map<String, Object> map = new LinkedHashMap<String, Object>(COLUMN.length);

		int cells = row.getPhysicalNumberOfCells();

		for (int i = 0; i < COLUMN.length; i++) {
			map.put(COLUMN[i], getValueByCell(row.getCell(i)));
		}		

		return map;
	}


	/**
	 * This method is used to get for one XSSFRow Row.
	 * 
	 * @param row
	 * @return
	 */
	private static Map<String, Object> getRow(XSSFRow row) {
		Map<String, Object> map = new LinkedHashMap<String, Object>(COLUMN.length);

		int cells = row.getPhysicalNumberOfCells();

		for (int i = 0; i < COLUMN.length; i++) {
			map.put(COLUMN[i], getValueByCell(row.getCell(i)));
		}		

		return map;
	}

	/**
	 * Apply style to row.
	 * @param row
	 * @param style
	 * @return
	 */
	private static Map<String, Object> addStyleToCell(XSSFRow row, CellStyle style) {
		Map<String, Object> map = new LinkedHashMap<String, Object>(COLUMN.length);

		int cells = row.getPhysicalNumberOfCells();

		for (int i = 0; i < COLUMN.length; i++) {
			if (row.getCell(i) != null) {
				row.getCell(i).setCellStyle(style);
			}
		}		

		return map;
	}



	/**
	 * This method read the value from HSSFCell
	 * 
	 * @param cell
	 * @return
	 */
	private static String getValueByCell(HSSFCell cell) {
		String value = null;
		if (cell != null) {
			switch (cell.getCellType()) {

			/*
			 * case HSSFCell.CELL_TYPE_FORMULA: value = cell.getCellFormula();
			 * break;
			 */
			case HSSFCell.CELL_TYPE_STRING:
				value = cell.getStringCellValue();
				break;

			case HSSFCell.CELL_TYPE_NUMERIC:
				value = "" + cell.getNumericCellValue();
				break;

			case HSSFCell.CELL_TYPE_BOOLEAN:
				value = "" + cell.getBooleanCellValue();
				break;

			default:
			}
		}
		return value;
	}

	/**
	 * This method read the value from XSSFCell
	 * 
	 * @param cell
	 * @return
	 */
	private static String getValueByCell(XSSFCell cell) {
		String value = null;
		if (cell != null) {
			switch (cell.getCellType()) {

			/*
			 * case HSSFCell.CELL_TYPE_FORMULA: value = cell.getCellFormula();
			 * break;
			 */
			case HSSFCell.CELL_TYPE_STRING:
				value = cell.getStringCellValue();
				break;

			case HSSFCell.CELL_TYPE_NUMERIC:
				value = "" + cell.getNumericCellValue();
				break;

			case HSSFCell.CELL_TYPE_BOOLEAN:
				value = "" + cell.getBooleanCellValue();
				break;

			default:
			}
		}
		return value;
	}

	/**
	 * This method is used to check whether map contains value of not;
	 * 
	 * @param map
	 * @return
	 */
	private static boolean isContentInMap(Map<String, Object> map) {
		boolean status = false;
		try {
			if (map != null) {
				for (int i = 0; i < COLUMN.length; i++) {
					if (map.get(COLUMN[i]) != null && !"".equalsIgnoreCase(((String)map.get(COLUMN[i])).trim())) {
						status = true;
						break;
					}
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return status;
	}
}
