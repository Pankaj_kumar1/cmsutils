package com.times.utility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import com.times.dao.ValidUserDao;

/**
 * 
 * @author pankaj.kumar1
 *
 */

@Configuration
@EnableScheduling
public class Schedulers {
	
	private static final Logger logger = LoggerFactory.getLogger(Schedulers.class);
	
	@Autowired
	ValidUserDao validUserDao;
	
	/**
	 * This scheduler method updates the valid user list.
	 * 1hr from war deployment with fixed delay of 1hr.
	 *
	 */

	@Scheduled(fixedDelay = 3600000, initialDelay = 3600000)
    public void updateValidUsers(){
		logger.info("Valid user scheduler called");
		validUserDao.updateValidUserList();

    }
}
