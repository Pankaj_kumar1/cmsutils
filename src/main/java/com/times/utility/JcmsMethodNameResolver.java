package com.times.utility;

import java.util.Map;

import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;
import org.springframework.web.servlet.mvc.multiaction.AbstractUrlMethodNameResolver;

public class JcmsMethodNameResolver extends AbstractUrlMethodNameResolver {

	private Map<String, String> mappings;
	private PathMatcher pathMatcher = new AntPathMatcher();
	
	@Override
	protected String getHandlerMethodNameForUrlPath(String url) {
		//Look for the exact result first.
		if(mappings.containsKey(url)) {
			return mappings.get(url);
		}
		//If not found, take each mapping one-by-one and see if there is a regex matching (ant-style)
		for(Map.Entry<String, String> mapping : mappings.entrySet()) {
			String urlPattern = mapping.getKey();
			String methodName = mapping.getValue();
			if (this.pathMatcher.match(urlPattern, url)) {
				return methodName;
			}
		}
		return null;
	}

	public Map<String, String> getMappings() {
		return mappings;
	}

	public void setMappings(Map<String, String> mappings) {
		this.mappings = mappings;
	}

}
