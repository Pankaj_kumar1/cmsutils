
package com.times.users;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
@Service
public class ChannelUsers {
	private Map<String,List<String>> channelUserMap;
	
	public ChannelUsers(){
		List<String> commonUsers = new ArrayList<String>();
		
		commonUsers.add("pankaj.kumar1@timesinternet.in");
		commonUsers.add("ajit.kumar@timesinternet.in");
		commonUsers.add("daksh.verma@timesinternet.in");
		commonUsers.add("geetanjali.tiwari@timesinternet.in");
		commonUsers.add("anurag.singhal@timesinternet.in");
		commonUsers.add("dipali.patidar@timesinternet.in");
		commonUsers.add("rajeev.khatri@timesinternet.in");
		commonUsers.add("gautam.khurana@timesinternet.in");
		commonUsers.add("yug.arora@timesinternet.in");
		commonUsers.add("priyanka.kansal@timesinternet.in");
		
		
		List<String> toiUser = new ArrayList<String>();
		toiUser.addAll(commonUsers);
		toiUser.add("yorlotus@gmail.com");
		toiUser.add("manish.upadhyay@timesinternet.in");
		toiUser.add("sangeeta.soni@timesinternet.in");
		toiUser.add("mrudula.mallepaddi@timesinternet.in");
		toiUser.add("rajni.pandey1@indiatimes.co.in");
		toiUser.add("gautam@alleviatetech.com");
		toiUser.add("sunny.tewathia@timesinternet.in");
		toiUser.add("mohit.sharma@timesinternet.in");
		toiUser.add("sunil.kumar@timesinternet.in");
		toiUser.add("d.prasad@timesinternet.in");
		toiUser.add("chaitanya.chebrolu@timesinternet.in");
		toiUser.add("harshit.goel@timesinternet.in");
		toiUser.add("trapti.s@timesinternet.in");
		toiUser.add("vineet.kumar3@timesinternet.in");
		
		List<String> gnUsers = new ArrayList<String>();
		gnUsers.addAll(commonUsers);
		gnUsers.add("akshay.bhatla@timesinternet.in");//Akshay.bhatla@timesinternet.in
		gnUsers.add("abhishek.kumar@timesinternet.in");//Abhishek.kumar@timesinternet.in
		gnUsers.add("meenhaz.ahmed@timesinternet.in");
		gnUsers.add("ankit.arora@timesinternet.in");
		gnUsers.add("anil.bhatt@timesinternet.in");
		gnUsers.add("nitin.singh2@timesinternet.in");
		gnUsers.add("inbaraj.augustin@timesinternet.in");//Inbaraj.augustin@timesinternet.in,   Inbaraj.augustin@timesinternet.in
		gnUsers.add("sweta.verma@timesinternet.in");
		gnUsers.add("rajeev.khatri@timesinternet.in");
		gnUsers.add("sunil.kumar@timesinternet.in");
		gnUsers.add("d.prasad@timesinternet.in");
		
		List<String> frUsers = new ArrayList<String>();
		frUsers.addAll(commonUsers);
		frUsers.add("smita.mishra@timesinternet.in");
		frUsers.add("manjari.lakshmanan@timesinternet.in");
		frUsers.add("mohita.mourya@timesinternet.in");
		frUsers.add("deeksha.sarin@timesinternet.in");
		frUsers.add("rishabh.raj@timesinternet.in");
		frUsers.add("muneet.walia@timesinternet.in");
		frUsers.add("kritika.pushkarna@timesinternet.in");   
		frUsers.add("srishti.chaturvedi@timesinternet.in");
		frUsers.add("harshit.goel@timesinternet.in");
		frUsers.add("trapti.s@timesinternet.in");
		frUsers.add("attili.mangatayaru@timesinternet.in");
		
		List<String> nbtGNUsers = new ArrayList<String>();
		nbtGNUsers.addAll(commonUsers);
		nbtGNUsers.add("vasunaik.ramavathi@timesinternet.in");
		nbtGNUsers.add("c-satyavani.attili@timesinternet.in");
		nbtGNUsers.add("sweta.verma@timesinternet.in");
		nbtGNUsers.add("nitin.nagpal@timesinternet.in");
		
		List<String> malayalam_GNUsers = new ArrayList<String>();
		malayalam_GNUsers.addAll(commonUsers);
		malayalam_GNUsers.add("vasunaik.ramavathi@timesinternet.in");
		malayalam_GNUsers.add("nitin.nagpal@timesinternet.in");
		
		List<String> telugu_GNUsers = new ArrayList<String>();
		telugu_GNUsers.addAll(commonUsers);
		telugu_GNUsers.add("vasunaik.ramavathi@timesinternet.in");
		telugu_GNUsers.add("nitin.nagpal@timesinternet.in");
		
		List<String> tamil_GNUsers = new ArrayList<String>();
		tamil_GNUsers.addAll(commonUsers);
		tamil_GNUsers.add("vasunaik.ramavathi@timesinternet.in");
		tamil_GNUsers.add("nitin.nagpal@timesinternet.in");
		
		List<String> vk_GNUsers = new ArrayList<String>();
		vk_GNUsers.addAll(commonUsers);
		vk_GNUsers.add("vasunaik.ramavathi@timesinternet.in");
		vk_GNUsers.add("nitin.nagpal@timesinternet.in");
		
		List<String> eisamay_GNUsers = new ArrayList<String>();
		eisamay_GNUsers.addAll(commonUsers);
		eisamay_GNUsers.add("vasunaik.ramavathi@timesinternet.in");
		eisamay_GNUsers.add("nitin.nagpal@timesinternet.in");
		
		List<String> mt_GNUsers = new ArrayList<String>();
		mt_GNUsers.addAll(commonUsers);
		mt_GNUsers.add("vasunaik.ramavathi@timesinternet.in");
		mt_GNUsers.add("nitin.nagpal@timesinternet.in");
		
		List<String> autoNowUsers = new ArrayList<String>();
		autoNowUsers.addAll(commonUsers);
		autoNowUsers.add("gaurav.bhatt@timesinternet.in");
		autoNowUsers.add("gunjan.kumar@timesinternet.in");
        autoNowUsers.add("chaitanya.chebrolu@timesinternet.in");
        
        List<String> nbtUsers = new ArrayList<String>();
        nbtUsers.addAll(commonUsers);
        nbtUsers.add("vasunaik.ramavathi@timesinternet.in");
        nbtUsers.add("nitin.nagpal@timesinternet.in");
        nbtUsers.add("prateek.kumar@timesinternet.in");
        
        List<String> gadgetCentralUsers = new ArrayList<String>();
        gadgetCentralUsers.addAll(commonUsers);
        gadgetCentralUsers.add("mayank.pandey@timesgroup.com");
        gadgetCentralUsers.add("amit.srivastava@timesgroup.com");
        gadgetCentralUsers.add("ahmed.shaikh@timesgroup.com");
        gadgetCentralUsers.add("shashank.kumar2@timesgroup.com");
        
        List<String> mtUsers = new ArrayList<String>();
        mtUsers.addAll(commonUsers);
        mtUsers.add("vasunaik.ramavathi@timesinternet.in");
        mtUsers.add("nitin.nagpal@timesinternet.in");
        mtUsers.add("prateek.kumar@timesinternet.in");
        
        List<String> eisUsers = new ArrayList<String>();
        eisUsers.addAll(commonUsers);
        eisUsers.add("vasunaik.ramavathi@timesinternet.in");
        eisUsers.add("nitin.nagpal@timesinternet.in");
        eisUsers.add("prateek.kumar@timesinternet.in");
        
        List<String> vkUsers = new ArrayList<String>();
        vkUsers.addAll(commonUsers);
        vkUsers.add("vasunaik.ramavathi@timesinternet.in");
        vkUsers.add("nitin.nagpal@timesinternet.in");
        vkUsers.add("prateek.kumar@timesinternet.in");
        
        
        List<String> tamilSamayamUsers = new ArrayList<String>();
        tamilSamayamUsers.addAll(commonUsers);
        tamilSamayamUsers.add("vasunaik.ramavathi@timesinternet.in");
        tamilSamayamUsers.add("nitin.nagpal@timesinternet.in");
        tamilSamayamUsers.add("prateek.kumar@timesinternet.in");
        
        
        List<String> teluguSamayamUsers = new ArrayList<String>();
        teluguSamayamUsers.addAll(commonUsers);
        teluguSamayamUsers.add("vasunaik.ramavathi@timesinternet.in");
        teluguSamayamUsers.add("nitin.nagpal@timesinternet.in");
        teluguSamayamUsers.add("prateek.kumar@timesinternet.in");
        
        List<String> malayalamSamayamUsers = new ArrayList<String>();
        malayalamSamayamUsers.addAll(commonUsers);
        malayalamSamayamUsers.add("vasunaik.ramavathi@timesinternet.in");
        malayalamSamayamUsers.add("nitin.nagpal@timesinternet.in");
        malayalamSamayamUsers.add("prateek.kumar@timesinternet.in");
        
        List<String> iagUsers = new ArrayList<String>();
        iagUsers.addAll(commonUsers);
        iagUsers.add("vasunaik.ramavathi@timesinternet.in");
        iagUsers.add("nitin.nagpal@timesinternet.in");
        iagUsers.add("prateek.kumar@timesinternet.in");
        
        List<String> biUsers = new ArrayList<String>();
        biUsers.addAll(commonUsers);
        biUsers.add("ashish.garg@timesinternet.in");
        biUsers.add("gulshan.chugh@timesinternet.in");
        biUsers.add("abhinav.rattan@timesinternet.in");
        biUsers.add("vishakha.arora@timesinternet.in");
		
		channelUserMap = new HashMap<String, List<String>>();
		channelUserMap.put("TOI", toiUser);
		channelUserMap.put("GN", gnUsers);
		channelUserMap.put("FR", frUsers);
		channelUserMap.put("NBT_GN", nbtGNUsers);
		channelUserMap.put("MALAYALAM_GN", malayalam_GNUsers);
		channelUserMap.put("TELUGU_GN", telugu_GNUsers);
		channelUserMap.put("TAMIL_GN", tamil_GNUsers);
		channelUserMap.put("VK_GN", vk_GNUsers);
		channelUserMap.put("EISAMAY_GN", eisamay_GNUsers);
		channelUserMap.put("MT_GN", mt_GNUsers);
		channelUserMap.put("AUTO_NOW", autoNowUsers);
		channelUserMap.put("NBT", nbtUsers);
		channelUserMap.put("GADGET_CENTRAL", gadgetCentralUsers);
		channelUserMap.put("MT", mtUsers);
		channelUserMap.put("EISAMAY", eisUsers);
		channelUserMap.put("VK", vkUsers);
		channelUserMap.put("TAMIL_SAMAYAM", tamilSamayamUsers);
		channelUserMap.put("TELUGU_SAMAYAM", teluguSamayamUsers);
		channelUserMap.put("MALAYALAM_SAMAYAM", malayalamSamayamUsers);
		channelUserMap.put("IAG", iagUsers);
		channelUserMap.put("BI", biUsers);
		
	}
	public boolean isValidUser(String userid, String channel){
		List<String> userList = channelUserMap.get(channel);
		if(userList != null && userList.contains(userid.toLowerCase())){
			return true;
		}
		return false;
	}
}
