package com.times.users;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

@Service
public class MappingUsers {
	private List<String> users = new ArrayList<String>();

	private Map<String, List<String>> mappingUserMap;

	public MappingUsers() {
		
		List<String> commonUsers = new ArrayList<String>();
		commonUsers.add("pankaj.kumar1@timesinternet.in");
		commonUsers.add("ajit.kumar@timesinternet.in");
		commonUsers.add("daksh.verma@timesinternet.in");
		commonUsers.add("geetanjali.tiwari@timesinternet.in");
		commonUsers.add("anurag.singhal@timesinternet.in");
		commonUsers.add("deepak.bisht1@timesinternet.in");
		commonUsers.add("dipali.patidar@timesinternet.in");
		commonUsers.add("rajeev.khatri@timesinternet.in");
		commonUsers.add("gautam.khurana@timesinternet.in");
		
		
		
		
		// TOI users

		List<String> toiUser = new ArrayList<String>();
		toiUser.addAll(commonUsers);
		toiUser.add("puneet.walia@timesinternet.in");
		toiUser.add("subhashish.singh@timesinternet.in ");
		toiUser.add("prashant.verma@timesinternet.in");
		toiUser.add("mohit.sharma@timesinternet.in");
		toiUser.add("mrudula.mallepaddi@timesinternet.in");
		toiUser.add("harshit.goel@timesinternet.in");
		toiUser.add("chaitanya.chebrolu@timesinternet.in");
		toiUser.add("vineet.kumar3@timesinternet.in");
		toiUser.add("sweta.verma@timesinternet.in");
		
		
		
		
		// ET users

		List<String> etUser = new ArrayList<String>();
		etUser.addAll(commonUsers);
		etUser.add("mohita.mourya@timesinternet.in");
		etUser.add("inbaraj.augustin@timesinternet.in");
		etUser.add("praveen.bokka@timesinternet.in");
		etUser.add("sanchit.agrawal@timesinternet.in");
		etUser.add("sakshi.saini@timesinternet.in");
		etUser.add("arunima.saroha@timesinternet.in ");
		etUser.add("nidhi.kumar@timesinternet.in");
		etUser.add("raminder.kaur@timesinternet.in");
		etUser.add("nishant.singh1@timesinternet.in");
		etUser.add("aishwariya.bhatnagar@timesinternet.in");
		etUser.add("sankalp.madaan@timesinternet.in");
		etUser.add("karan.jaine@timesinternet.in");
		etUser.add("sumit.gugnani@timesinternet.in");
		
		
		//ET Lang
		List<String> etLangMobUser = new ArrayList<String>();
		etLangMobUser.addAll(etUser);
		
		List<String> etLangWebUser = new ArrayList<String>();
		etLangWebUser.addAll(etUser);
		
		
		
		// Timesjob users
		
		List<String> timesJobUser = new ArrayList<String>();
		timesJobUser.addAll(commonUsers);
		timesJobUser.add("asif.ajazi@timesinternet.in");
		timesJobUser.add("prasoon.singh@timesinternet.in");
		
		List<String> techGigUser = new ArrayList<String>();
		techGigUser.addAll(commonUsers);
		techGigUser.add("prasoon.singh@timesinternet.in");
		
		
		
		// Mumbai mirror users
		
		List<String> mMirrorUser = new ArrayList<String>();
		mMirrorUser.addAll(commonUsers);
		mMirrorUser.add("jasmine.sadana@timesinternet.in");
		
		// Bangalore mirror users
		
		List<String> bMirrorUser = new ArrayList<String>();
		bMirrorUser.addAll(commonUsers);
		//bMirrorUser.add("jasmine.sadana@timesinternet.in");
		
		// Pune mirror users
		List<String> pMirrorUser = new ArrayList<String>();
		pMirrorUser.addAll(commonUsers);
		
		// Ahmedabad mirror users
		
		List<String> aMirrorUser = new ArrayList<String>();
		aMirrorUser.addAll(commonUsers);
		
		
		// Language sites users
		
		List<String> nbtUser = new ArrayList<String>();
		nbtUser.addAll(commonUsers);
		nbtUser.add("nitin.singh1@timesinternet.in");
		nbtUser.add("vineet.chaturvedi@timesinternet.in");
		nbtUser.add("minakshi.bajpai@timesinternet.in");
		nbtUser.add("sandeep.kumar3@timesinternet.in");
		nbtUser.add("vasunaik.ramavathi@timesinternet.in");
		
		List<String> eisamayUser = new ArrayList<String>();
		eisamayUser.addAll(commonUsers);
		eisamayUser.add("nitin.singh1@timesinternet.in");
		eisamayUser.add("vineet.chaturvedi@timesinternet.in");
		eisamayUser.add("minakshi.bajpai@timesinternet.in");
		eisamayUser.add("sandeep.kumar3@timesinternet.in");
		eisamayUser.add("vasunaik.ramavathi@timesinternet.in");
		
		List<String> mtUsers = new ArrayList<String>();
		mtUsers.addAll(commonUsers);
		mtUsers.add("nitin.singh1@timesinternet.in");
		mtUsers.add("vineet.chaturvedi@timesinternet.in");
		mtUsers.add("minakshi.bajpai@timesinternet.in");
		mtUsers.add("sandeep.kumar3@timesinternet.in");
		mtUsers.add("vasunaik.ramavathi@timesinternet.in");
		
		List<String> tamilUser = new ArrayList<String>();
		tamilUser.addAll(commonUsers);
		tamilUser.add("nitin.singh1@timesinternet.in");
		tamilUser.add("vineet.chaturvedi@timesinternet.in");
		tamilUser.add("minakshi.bajpai@timesinternet.in");
		tamilUser.add("sandeep.kumar3@timesinternet.in");
		tamilUser.add("vasunaik.ramavathi@timesinternet.in");
		
		List<String> telguUser = new ArrayList<String>();
		telguUser.addAll(commonUsers);
		telguUser.add("nitin.singh1@timesinternet.in");
		telguUser.add("vineet.chaturvedi@timesinternet.in");
		telguUser.add("minakshi.bajpai@timesinternet.in");
		telguUser.add("sandeep.kumar3@timesinternet.in");
		telguUser.add("vasunaik.ramavathi@timesinternet.in");
		
		List<String>  vkUser = new ArrayList<String>();
		vkUser.addAll(commonUsers);
		vkUser.add("nitin.singh1@timesinternet.in");
		vkUser.add("vineet.chaturvedi@timesinternet.in");
		vkUser.add("minakshi.bajpai@timesinternet.in");
		vkUser.add("sandeep.kumar3@timesinternet.in");
		vkUser.add("vasunaik.ramavathi@timesinternet.in");
		
		List<String> malyalamUser = new ArrayList<String>();
		malyalamUser.addAll(commonUsers);
		malyalamUser.add("nitin.singh1@timesinternet.in");
		malyalamUser.add("vineet.chaturvedi@timesinternet.in");
		malyalamUser.add("minakshi.bajpai@timesinternet.in");
		malyalamUser.add("sandeep.kumar3@timesinternet.in");
		malyalamUser.add("vasunaik.ramavathi@timesinternet.in");
		
		
		
		// TGP users
		
		List<String> biUser = new ArrayList<String>();
		biUser.addAll(commonUsers);
		biUser.add("puneet.walia@timesinternet.in");
		
		List<String> adAgeUser = new ArrayList<String>();
		adAgeUser.addAll(commonUsers);
		adAgeUser.add("puneet.walia@timesinternet.in");
		
		List<String> gizmoUser = new ArrayList<String>();
		gizmoUser.addAll(commonUsers);
		gizmoUser.add("puneet.walia@timesinternet.in");
		
		List<String> lifeHackerUser = new ArrayList<String>();
		lifeHackerUser.addAll(commonUsers);
		lifeHackerUser.add("puneet.walia@timesinternet.in");
		
		List<String> techRadarUser = new ArrayList<String>();
		techRadarUser.addAll(commonUsers);
		techRadarUser.add("puneet.walia@timesinternet.in");
		
		List<String> techSpotUser = new ArrayList<String>();
		techSpotUser.addAll(commonUsers);
		techSpotUser.add("puneet.walia@timesinternet.in");
		
		//HappyTrips
		List<String> happyTripsUser = new ArrayList<String>();
        happyTripsUser.addAll(commonUsers);
       
        //GadgetsNow
        List<String> gadgetsNowUser = new ArrayList<String>();
        gadgetsNowUser.addAll(commonUsers);
        gadgetsNowUser.add("sweta.verma@timesinternet.in");
        
        // Photogallery
        List<String> photogalleryUser = new ArrayList<String>();
        photogalleryUser.addAll(commonUsers);
        
        List<String> mPhotogalleryUser = new ArrayList<String>();
        mPhotogalleryUser.addAll(commonUsers);
        
        List<String> timesFood = new ArrayList<String>();
        timesFood.addAll(commonUsers);
        timesFood.add("mohit.sharma@timesinternet.in");
        
        List<String> bombayTimes = new ArrayList<String>();
        bombayTimes.addAll(commonUsers);
        
        List<String> beautyPageants = new ArrayList<String>();
        beautyPageants.addAll(commonUsers);
        
        List<String> misskyra = new ArrayList<String>();
        misskyra.addAll(commonUsers);
        
        List<String> autoNowUsers = new ArrayList<String>();
        autoNowUsers.addAll(commonUsers);
        autoNowUsers.add("gaurav.bhatt@timesinternet.in");
        autoNowUsers.add("gunjan.kumar@timesinternet.in");
        autoNowUsers.add("chaitanya.chebrolu@timesinternet.in");
        
        List<String> nbtGoldUsers = new ArrayList<String>();
        nbtGoldUsers.addAll(commonUsers);
        
        List<String> eisamayGoldUsers = new ArrayList<String>();
        eisamayGoldUsers.addAll(commonUsers);
        
        List<String> iamGujaratUsers = new ArrayList<String>();
        iamGujaratUsers.addAll(commonUsers);
        iamGujaratUsers.add("sandeep.kumar3@timesinternet.in");
        iamGujaratUsers.add("vasunaik.ramavathi@timesinternet.in");
		
		
		
        mappingUserMap = new HashMap<String, List<String>>();
		mappingUserMap.put("TOI", toiUser);
		mappingUserMap.put("ET", etUser);
		mappingUserMap.put("ETLM", etLangMobUser);
		mappingUserMap.put("ETLW", etLangWebUser);
		mappingUserMap.put("MM", mMirrorUser);
		mappingUserMap.put("BM", bMirrorUser);
		mappingUserMap.put("PM", pMirrorUser);
		mappingUserMap.put("AM", aMirrorUser);
		mappingUserMap.put("NBT", nbtUser);
		mappingUserMap.put("EIS", eisamayUser);
		mappingUserMap.put("TAS", tamilUser);
		mappingUserMap.put("TES", telguUser);
		mappingUserMap.put("MT", tamilUser);
		mappingUserMap.put("MS", malyalamUser);
		mappingUserMap.put("VK", vkUser);
		mappingUserMap.put("TJ", timesJobUser);
		mappingUserMap.put("TG", techGigUser);
		mappingUserMap.put("BI", biUser);
		mappingUserMap.put("AD", adAgeUser);
		mappingUserMap.put("GZ", gizmoUser);
		mappingUserMap.put("LH", lifeHackerUser);
		mappingUserMap.put("TR", techRadarUser);
		mappingUserMap.put("TS", techSpotUser);
		mappingUserMap.put("HT", happyTripsUser);
		mappingUserMap.put("GN", gadgetsNowUser);
		mappingUserMap.put("Photogallery", photogalleryUser);
		mappingUserMap.put("MobilePhoto", mPhotogalleryUser);
		mappingUserMap.put("Recipes", timesFood);
		mappingUserMap.put("BombayTimes", bombayTimes);
		mappingUserMap.put("BeautyPageants", beautyPageants);
		mappingUserMap.put("Misskyra", misskyra);
		mappingUserMap.put("AutoNow", autoNowUsers);
		mappingUserMap.put("NBTGOLD", nbtGoldUsers);
		mappingUserMap.put("EISAMAYGOLD", eisamayGoldUsers);
		mappingUserMap.put("IAMGUJARAT", iamGujaratUsers);
		
	}

	public boolean isValidUser(String userid, String channel) {
		List<String> userList = mappingUserMap.get(channel);
		if (userList != null && userList.contains(userid.toLowerCase())) {
			return true;
		}
		return false;
	}

}
