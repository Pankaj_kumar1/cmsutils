package com.times.model;

import java.util.Date;
import java.util.Map;

public class SeoMeta {
		
	private int id;
	private String action;
	private String uri;
	private String channel;
	private int hostid;
	private String title;
	private String shortdesc;
	private String longdesc;
	private String atfContent;
	private String btfContent;
	private String canonicalUrl;
	private String mUrl;
	private String ampUrl;
	private String metakeywords;

	private String userName;
	private String mode;
	private Boolean status;
	private Date insertDate;
	private Date updateDate;
	
	private Map title_url_map;
	

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	public int getHostid() {
		return hostid;
	}
	public void setHostid(int hostid) {
		this.hostid = hostid;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getShortdesc() {
		return shortdesc;
	}
	public void setShortdesc(String shortdesc) {
		this.shortdesc = shortdesc;
	}
	public String getLongdesc() {
		return longdesc;
	}
	public void setLongdesc(String longdesc) {
		this.longdesc = longdesc;
	}
	public String getAtfContent() {
		return atfContent;
	}
	public void setAtfContent(String atfContent) {
		this.atfContent = atfContent;
	}
	public String getBtfContent() {
		return btfContent;
	}
	public void setBtfContent(String btfContent) {
		this.btfContent = btfContent;
	}
	public String getCanonicalUrl() {
		return canonicalUrl;
	}
	public void setCanonicalUrl(String canonicalUrl) {
		this.canonicalUrl = canonicalUrl;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public Date getInsertDate() {
		return insertDate;
	}
	public void setInsertDate(Date insertDate) {
		this.insertDate = insertDate;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	public String getmUrl() {
		return mUrl;
	}
	public void setmUrl(String mUrl) {
		this.mUrl = mUrl;
	}
	public String getAmpUrl() {
		return ampUrl;
	}
	public void setAmpUrl(String ampUrl) {
		this.ampUrl = ampUrl;
	}
	public String getMetakeywords() {
		return metakeywords;
	}
	public void setMetakeywords(String metakeywords) {
		this.metakeywords = metakeywords;
	}

	public Map getTitle_url_map() {
		return title_url_map;
	}
	public void setTitle_url_map(Map title_url_map) {
		this.title_url_map = title_url_map;
	}
}
