package com.times.model;

public class CMSILeftObject {
	
	public static String ID = "_id";
	public static String CATKEY = "catkey";
	public static String S_NAME = "sname";
	public static String S_NAMESEO = "snameseo";
	public static String OVERRIDELINK = "overridelink";
	public static String SEOLOCATION = "seolocation";
	public static String REDIRECTION_TYPE = "redirectiontype";
	public static String HOSTID ="hostId";
	public static String INSTANCEID = "instanceId";
	public static String MAPPEDBY ="mappedBy";
	public static String MAPPEDAT ="mappedAt";
	public static String UPDATEDAT ="updatedAt";
	public static String UPDATEDBY ="updatedBy";
	public static String STATUS = "status";
	
	private Object 	id;
	private String 	catkey;
	private String 	sname;
	private String 	snameseo;
	private String 	overridelink;
	private String	seolocation;
	private int redirectionType;
	private int instanceId;
	private int hostId;
	private String mappedBy;
	private String mappedAt;
	private String updatedAt;
	private String updatedBy;
	private int status;
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	public String getMappedBy() {
		return mappedBy;
	}
	public void setMappedBy(String mappedBy) {
		this.mappedBy = mappedBy;
	}
	public String getMappedAt() {
		return mappedAt;
	}
	public void setMappedAt(String mappedAt) {
		this.mappedAt = mappedAt;
	}
	
	public int getHostId() {
		return hostId;
	}
	public int getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(int instanceId) {
		this.instanceId = instanceId;
	}
	public void setHostId(int hostId) {
		this.hostId = hostId;
	}
	public Object getId() {
		return id;
	}
	public void setId(Object id) {
		this.id = id;
	}
	
	public int getRedirectionType() {
		return redirectionType;
	}
	public void setRedirectionType(int redirectionType) {
		this.redirectionType = redirectionType;
	}
	public String getCatkey() {
		return catkey;
	}
	public void setCatkey(String catkey) {
		this.catkey = catkey;
	}
	public String getSname() {
		return sname;
	}
	public void setSname(String sname) {
		this.sname = sname;
	}
	public String getSnameseo() {
		return snameseo;
	}
	public void setSnameseo(String snameseo) {
		this.snameseo = snameseo;
	}
	public String getOverridelink() {
		return overridelink;
	}
	public void setOverridelink(String overridelink) {		
		this.overridelink = overridelink;
	}
	public String getSeolocation() {
		return seolocation;
	}
	public void setSeolocation(String seolocation) {
		this.seolocation = seolocation;
	}
	
	
	
}
