<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<html>
<head>
<title>CMS ShortURL Editor Login Page</title>

<link rel="stylesheet" type="text/css" media="screen"
	href="css/bootstrap.css" />
<link rel="stylesheet" type="text/css" media="screen"
	href="css/login.css" />
<link rel="stylesheet" type="text/css" media="screen"
	href="css/iphonestyle.css" />

<script src="script/jquery.js"></script>
<script src="script/bootstrap.js"></script>
<script src="script/iphone-style-checkboxes.js" type="text/javascript"
	charset="utf-8"></script>

<script type="text/javascript">

$(window).load(function() {
  $('.on_off :checkbox').iphoneStyle();
  $('.disabled :checkbox').iphoneStyle();
  $('.css_sized_container :checkbox').iphoneStyle({ resizeContainer: false, resizeHandle: false });
  $('.long_tiny :checkbox').iphoneStyle({ checkedLabel: 'Very Long Text', uncheckedLabel: 'Tiny' });
  
  var onchange_checkbox = ($('.onchange :checkbox')).iphoneStyle({
    onChange: function(elem, value) { 
      $('span#status').html(value.toString());
    }
  });
  
  setInterval(function() {
    onchange_checkbox.prop('checked', !onchange_checkbox.is(':checked')).iphoneStyle("refresh");
    return
  }, 2500);
});

</script>
</head>
<body >

<table>
    <tbody><tr class="disabled">
      <th><label for="disabled">Disabled</label></th>
      <td>
        <div class="iPhoneCheckContainer iPhoneCheckDisabled" style="width: 99px;"><input type="checkbox" id="disabled" disabled="disabled"><label class="iPhoneCheckLabelOff" style="width: 94px;">
  <span>OFF</span>
</label><label class="iPhoneCheckLabelOn" style="width: 0px;">
  <span style="margin-left: -51px;">ON</span>
</label><div class="iPhoneCheckHandle" style="width: 42px;">
  <div class="iPhoneCheckHandleRight">
    <div class="iPhoneCheckHandleCenter"></div>
  </div>
</div></div>
      </td>
    </tr>
    <tr class="on_off">
      <th><label for="on_off">Default</label></th>
      <td>
        <div class="iPhoneCheckContainer" style="width: 99px;"><input type="checkbox" id="on_off"><label class="iPhoneCheckLabelOff" style="width: 94px;">
  <span style="margin-right: 0px;">OFF</span>
</label><label class="iPhoneCheckLabelOn" style="width: 4px;">
  <span style="margin-left: -51px;">ON</span>
</label><div class="iPhoneCheckHandle" style="width: 42px; left: 0px;">
  <div class="iPhoneCheckHandleRight">
    <div class="iPhoneCheckHandleCenter"></div>
  </div>
</div></div>
      </td>
    </tr>
    <tr class="on_off">
      <th><label for="on_off_on">Default On</label></th>
      <td>
        <div class="iPhoneCheckContainer" style="width: 99px;"><input type="checkbox" checked="checked" id="on_off_on"><label class="iPhoneCheckLabelOff" style="width: 94px;">
  <span style="margin-right: 0px;">OFF</span>
</label><label class="iPhoneCheckLabelOn" style="width: 4px;">
  <span style="margin-left: -51px;">ON</span>
</label><div class="iPhoneCheckHandle" style="width: 42px; left: 0px;">
  <div class="iPhoneCheckHandleRight">
    <div class="iPhoneCheckHandleCenter"></div>
  </div>
</div></div>
      </td>
    </tr>
    <tr class="css_sized_container">
      <th><label for="css_sized_container">CSS Sized Container and Handle</label></th>
      <td>
        <div class="iPhoneCheckContainer"><input type="checkbox" id="css_sized_container"><label class="iPhoneCheckLabelOff" style="width: 245px;">
  <span style="margin-right: 0px;">OFF</span>
</label><label class="iPhoneCheckLabelOn" style="width: 4px;">
  <span style="margin-left: -244px;">ON</span>
</label><div class="iPhoneCheckHandle" style="left: 0px;">
  <div class="iPhoneCheckHandleRight">
    <div class="iPhoneCheckHandleCenter"></div>
  </div>
</div></div>
      </td>
    </tr>
    <tr class="long_tiny">
      <th><label class="left" for="long_tiny">Long and short labels</label></th>
      <td>
        <div class="iPhoneCheckContainer" style="width: 277px;"><input type="checkbox" id="long_tiny"><label class="iPhoneCheckLabelOff" style="width: 272px;">
  <span>Tiny</span>
</label><label class="iPhoneCheckLabelOn" style="width: 0px;">
  <span style="margin-left: -140px;">Very Long Text</span>
</label><div class="iPhoneCheckHandle" style="width: 131px;">
  <div class="iPhoneCheckHandleRight">
    <div class="iPhoneCheckHandleCenter"></div>
  </div>
</div></div>
      </td>
    </tr>
    <tr class="onchange">
      <th>Event tracking</th>
      <td>
        <div class="iPhoneCheckContainer" style="width: 99px;"><input type="checkbox" id="onchange"><label class="iPhoneCheckLabelOff" style="width: 94px;">
  <span style="margin-right: 0px;">OFF</span>
</label><label class="iPhoneCheckLabelOn" style="width: 4px;">
  <span style="margin-left: -51px;">ON</span>
</label><div class="iPhoneCheckHandle" style="width: 42px; left: 0px;">
  <div class="iPhoneCheckHandleRight">
    <div class="iPhoneCheckHandleCenter"></div>
  </div>
</div></div>
        <p>Checkbox status is <strong><span id="status">true</span></strong>.</p>
      </td>
    </tr>
  </tbody></table>

	<div class="container">
		<div class="wrapper">

			<form name='loginForm'
				action="<c:url value='j_spring_security_check' />" method='POST'
				class="form-signin">
				<h3 class="form-signin-heading">Please Sign In</h3>
				<hr class="colorgraph">
				<br>
				<c:if test="${not empty error}">
					<div class="error">${error}</div>
				</c:if>
				<c:if test="${not empty msg}">
					<div class="msg">${msg}</div>
				</c:if>
				<table>
					<tr>
						<input type='text' name='username' class="form-control"
							placeholder="Username" required="" autofocus="" />
					</tr>
					<tr>
						<input type='password' name='password' class="form-control"
							placeholder="Password" required="" />
					</tr>
					<tr>
						
						<td><input type="checkbox" name="livemode" value="1" checked /> Live</td>
						<td><input type="checkbox" name="localmode" value="0"  />
							Local</td>
					</tr>



					<!-- 					<td colspan='2'><input name="submit" type="submit"
						value="Login" /></td> -->
				</table>
				<br /> <br />
				<button class="btn btn-lg btn-primary btn-block" name="submit"
					value="Login" type="Submit">Login</button>

				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />

			</form>

		</div>
	</div>

</body>
</html>