<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<html>
<head>
<title>CMS ShortURL Editor Login Page</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link rel="stylesheet" type="text/css" media="screen"
	href="css/bootstrap.css" />
<link rel="stylesheet" type="text/css" media="screen"
	href="css/login.css" />
<link rel="stylesheet" type="text/css" media="screen"
	href="css/iphonestyle.css" />

  <style type="text/css">
    body {
      padding: 10px; }
    th {
      text-align: right;
      padding: 4px;
      padding-right: 15px;
      vertical-align: top; }
    .css_sized_container .iPhoneCheckContainer {
      width: 250px; }
  </style>
  
<script src="script/jquery.js"></script>
<script src="script/bootstrap.js"></script>
<script src="script/iphone-style-checkboxes.js" type="text/javascript"
	charset="utf-8"></script>

<script type="text/javascript">

$(window).load(function() {
  $('.on_off :checkbox').iphoneStyle();
  $('.disabled :checkbox').iphoneStyle();
  $('.css_sized_container :checkbox').iphoneStyle({ resizeContainer: false, resizeHandle: false });
  $('.long_tiny :checkbox').iphoneStyle({ checkedLabel: 'Very Long Text', uncheckedLabel: 'Tiny' });
  
  var onchange_checkbox = ($('.onchange :checkbox')).iphoneStyle({
    onChange: function(elem, value) { 
      $('span#status').html(value.toString());
    }
  });
  
  setInterval(function() {
    onchange_checkbox.prop('checked', !onchange_checkbox.is(':checked')).iphoneStyle("refresh");
    return
  }, 2500);
});

</script>
</head>
<body style="background-color:blanchedalmond"/>
<div>
</div>
	<div class="container">
		<div class="wrapper">

			<form name='loginForm'
				action="<c:url value='j_spring_security_check' />" method='POST'
				class="form-signin">
				<h3 class="form-signin-heading">SSO Sign In</h3>
				<hr class="colorgraph">
				<br>
				<c:if test="${not empty error}">
					<div class="error">${error}</div>
				</c:if>
				<c:if test="${not empty msg}">
					<div class="msg">${msg}</div>
				</c:if>
				<table>
					<tr>
						<input type='text' id='j_username' name='j_username' class="form-control"
							placeholder="SSO Username" required="" autofocus="" />
					</tr>
					<tr>
						<input type='password' id='j_password' name='j_password' class="form-control"
							placeholder="Password" required="" />
					</tr>
					<br/>
<!-- 					<tr>
						<td><input type="checkbox" name="localmode" value="0" checked />
							Local</td>
						<td><input type="checkbox" name="livemode" value="1" /> Live</td>
					</tr> -->

    <tr class="on_off">
      <th><label for="on_off">Select Mode</label></th>
      <td>
        <input type="checkbox" id="on_off" name="mode"/>
      </td>
    </tr>


					<!-- 					<td colspan='2'><input name="submit" type="submit"
						value="Login" /></td> -->
				</table>
				<br /> <br />
				<button class="btn btn-lg btn-primary btn-block" name="submit"
					value="login" type="Submit">Login</button>
				<br/>
				<div style="text-align: center">
					<a href="http://timesofindia.indiatimes.com/loginview.cms?register=1" target="blank">Register Now</a>
				</div>
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />

			</form>

		</div>
	</div>

</body>
</html>