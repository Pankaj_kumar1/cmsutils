<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Seo Panel</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

	<div class="container">
		<div class="row">
			<div class="col-sm-2"></div>
			<div class="col-sm-8">
				<form>
					<div class="form-group">
						<input class="form-control" id="mappingname" name="mappingname"
							placeholder="Enter Mapping Name">
					</div>
					<div class="form-group">
						<input class="form-control" id="uri" name="uri" placeholder="Enter URI">
					</div>
					<div class="form-group">
						<select class="form-control" id="channel" name="channel">
							<option>Select Channel</option>
							<option>TOI</option>
							<option>NBT</option>
							<option>ET</option>
							<option>GN</option>
						
						</select>
						
					</div>
					<div class="form-group">
						<input class="form-control" id="title" name="title" placeholder="Enter H1 title">
					</div>
					<div class="form-group">

						<textarea class="form-control" rows="5" id="shortdesc" name="shortdesc"
							placeholder="Enter Alt-title"></textarea>
					</div>
					<div class="form-group">

						<textarea class="form-control" rows="5" id="longdesc" name="longdesc"
							placeholder="Enter Meta-description"></textarea>
					</div>

					<button type="button" id="submit" name="submit"  class="btn btn-default">Submit</button>
					<button type="button" id="delete" name="delete" class="btn btn-default">Delete</button>

					
					</form>
			</div>
			<div class="col-sm-2"></div>
		</div>

	</div>
	 <script>
	
		$(document).ready(function() {
			$("#submit").click(function(){
				
				var postdata = {};
				postdata["mappingname"] = $("#mappingname").val();
				postdata["uri"] = $("#uri").val();
				postdata["channel"] = $("#channel").val();
				postdata["title"] = $("#title").val();
				postdata["shortdesc"] = $("#shortdesc").val();
				postdata["longdesc"] = $("#longdesc").val();
		
				$.ajax({
		    		url: "/cmsutils/seo-meta/insert",
		    		data: postdata,
		    		type: "GET",
		    		dataType : "json",
				}).done(function( json ) {
					
					alert(JSON.stringify(json));
		  		});
			
				
			});
			
		});
	</script>

</body>
</html>

