<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@page session="true"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>SEO PANEL</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/shorturl_style.css" rel="stylesheet">
<link href="css/bootstrap-table.css" rel="stylesheet">
<link href="css/jquery-ui.css" rel="stylesheet">

<link rel="stylesheet" href="css/custombox.css">


<script src="script/jquery.js"></script>
<script src="script/bootstrap.js"></script>
<script src="script/run_prettify.js"></script>
<script src="script/shorturl.js"></script>
<script src="script/bootbox.min.js"></script>
<script src="script/bootstrap-table.js"></script>
<script src="script/modernizr.custom.js"></script>
<script src="script/jquery-ui.js"></script>
<script src="script/raphael.js"></script>
<script src="script/font.js"></script>
<script src="script/Courier_New_400.font.js"></script>
<script src="script/mustache.min.js"></script>
<script src="script/tinymce/tinymce.min.js"></script>
<script src="script/tinymce/plugin.js"></script>
<script src="script/tinymce/plugin.min.js"></script>
<script src="https://cdn.ckeditor.com/4.7.1/full/ckeditor.js"></script>
<script src="script/ckeconfig.js"></script>

</head>
<style>
#atf-content {
    height: auto;
    min-height: 150px;
}
#btf-content {
    height: auto;
    min-height: 150px;
}
#atf-contentt {
    height: auto;
    min-height: 150px;
}
#btf-contentt {
    height: auto;
    min-height: 150px;
}
#changeBg{
       background-image: linear-gradient(135deg, #0288d1,#b71c1c  );
 }
</style>

<body>

	<meta name="_csrf" content="${_csrf.token}" />
	<meta name="_csrf_header" content="${_csrf.headerName}" />
	<div id="container" class="container"
		style="width: 90%; padding-right: 0px; padding-left: 0px; margin-right: 0px; margin-left: 0px;">
		<div class="page-header" id="changeBg"
			style="z-index: 999; margin-top: 0px; margin-left: 0px; margin-right: 0px; position: fixed; width: 100%; border: 1px solid darkblue; height:81px;">
			<div class="unselectable"
				style="width: 40%; float: left; color: white;">
				<h3 style="position: absolute;margin-left: 550px;margin-top: 30px;"> SEO PANEL </h3>
			</div>
			
				<div class="unselectable"
					style="width: 40%; margin-left: 500px; margin-top: 10px;">
					<span id="spanmode"
						style="float: left; color: white">
						 <font size="6"> <c:out
								value="${data.mode}" />
					</font>
					</span>
				</div>
			

			<!-- 			<script type="text/javascript">
				if( ${data.mode}){
					$("#spanmode").html('<font color="green" size="6">LIVE MODE<\/font>');
				}
				else{
					$("#spanmode").html('<font color="green" size="6">LOCAL MODE<\/font>');
				}
			</script> -->

			<div class="unselectable" style="float: right; color: white;">
			<h3>
						<font size="4">${username}</font> <font
							color="#000000" size="4"> |</font>
					</h3>

			</div>
		</div>
		<div class="row">
			<div class="tabbable" id="sidebar" style="float: left; width: 252px">
				<div class="span3"
					style="width: 262px; margin-right: 30px; margin-left: 30px; float: left">
					<h3>What you want to do?</h3>
					<p></p>
					<div class="well" style="position: fixed;">
						<ul class="nav nav-pills nav-stacked" id="sidenav">
							<li class="active"><a data-toggle="tab" href="#tabs-basic"><strong>Add
										Short URL</strong></a></li>
							<li><a data-toggle="tab" href="#pills-stacked"><strong>Search
										& Update</strong></a></li>
							<li><a data-toggle="tab" href="#contact"><strong>Contact
										Us</strong></a></li>
							<li><a data-toggle="tab" href="#form"><strong>Add
										SEO Description</strong></a></li>
							<li><a data-toggle="tab" href="#edit"><strong>Update
										SEO Description</strong></a></li>
							<li><a data-toggle="tab" href="#doc"
								target="_blank"><strong> About</strong></a></li>
							<li><a data-toggle="tab" href="#uploadfile"
								target="_blank"><strong> Upload File</strong></a></li>
						</ul>
					</div>
					<!-- .well -->

				</div>
			</div>
			<div class="span9" style="margin-top: 100px;">
				<div class="tab-content">
					<div class="tab-pane active" id="tabs-basic" style="float: right">
						<h4 class="unselectable"
							style="font-style: italic; text-decoration: underline">
							Enter Actual URL and corresponding Short URL to be mapped:</h4>
						<br>
						<div id="main">
							
						</div>
					</div>
					<div
						style="margin-left: 300px; border: 1px dashed #999; padding: 10px; margin-bottom: 15px"
						class="tab-pane" id="tabs-side"">
						<div>
							<a href=""><u>GO BACK</u></a>
						</div>
						<br />
						<div>
							<span id="spannewp" style="float: left; vertical-align: middle"
								class="info"></span>
						</div>
						<br /> <br /> <br /> <br />
						<div style="text-align: center">
							<input type="button" data-inline="true" value="Push LIVE"
								name="live" onClick="javascript:makeLive()"
								style="height: 40px; width: 100px; background: red; border: 1px solid black; color: white"><br />
							<span id="spanlive" style="float: right" class="info"></span>
						</div>
						<br /> <br /> <br /> <br />
						<div>
							<table class="bordered" id="rtable">
								<thead>

									<tr>
										<th>#</th>
										<th>Short URL</th>
										<th>Msid</th>
										<th>Sname</th>
										<!-- <th>Status</th> -->
										<th>Message</th>
									</tr>
								</thead>

							</table>
						</div>
					</div>
					<div class="tab-pane"
						style="overflow-y: hidden; position: relative; margin-left: 300px;"
						id="pills-stacked">
						<br />

						<table
							class="table table-striped table-bordered table-hover dataTable"
							id="datatable" data-toggle="table" data-url="getall"
							data-pagination="true" data-height="450" data-show-refresh="true"
							data-show-toggle="true" data-show-columns="true"
							data-search="true" data-select-item-name="toolbar1">
							<thead>
								<tr>
									<!--  									<th data-field="state" data-align="centre" data-checkbox="true"></th>
 								<th data-field="_id" data-align="left" data-sortable="true">Id</th>
		-->                         <th data-field="operate" data-formatter="operateFormatter"
										data-events="operateEvents">Modify</th>
									<th data-field="seolocation" data-align="left"
										data-sortable="true">Short URL</th>
									<th data-field="overridelink" data-align="left"
										data-sortable="true">Actual URL</th>
									<!-- <th data-field="status" data-align="left"
										data-sortable="true">Status</th> -->	
									<th data-field="catkey" data-align="left" data-sortable="true">msid</th>
								</tr>
							</thead>
						</table>

						<div class="modal fade" id="editmodal" tabindex="-1" role="dialog"
							aria-labelledby="edit" aria-hidden="true">
							<div id="editdialog" class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header btn-warning">
										<button type="button" class="close" data-dismiss="modal"
											aria-hidden="true">
											<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
										</button>
										<h4 class="modal-title custom_align" id="Heading">Edit</h4>
									</div>
									<div class="modal-body">
										<div class="form-group">
											<label>Msid</label> <input class="form-control " id="msid"
												name="msid" type="text" placeholder="msid">
										</div>
										<!-- <strong>Status</strong>
										<div class="form-group">
								            <select class="form-control" name="status" id="status" value="Select Status"> 
								            	<option selected="selected">Select Status</option>
                                                <option>1</option> 
                                                 <option>0</option>
                                            </select> 
                                    </div> -->
										<div class="form-group">
											<lable>Short URL</lable>
											<input class="form-control " id="surl" name="surl" type="url"
												placeholder="Short URL">
										</div>
										<div class="form-group">
											<lable>Actual URL</lable>
											<input class="form-control " id="aurl" name="aurl" type="url"
												placeholder="Actual URL">
										</div>
									</div>
									<div class="modal-footer ">
										<button type="button" id="upbutton" data-dismiss="modal"
											class="btn btn-warning btn-lg" style="width: 100%;">
											<span class="glyphicon glyphicon-ok-sign"></span>Update
										</button>
									</div>
								</div>
								<!-- /.modal-content -->
							</div>
							<!-- /.modal-dialog -->
						</div>

						<div class="modal" id="deletemodal" tabindex="-1" role="dialog"
							aria-labelledby="delete" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal"
											aria-hidden="true">
											<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
										</button>
										<h4 class="modal-title custom_align" id="Heading">Delete
											this entry</h4>
									</div>
									<div class="modal-body">

										<div class="alert alert-danger">
											<span class="glyphicon glyphicon-warning-sign"></span> Are
											you sure you want to delete this Record?
										</div>

									</div>
									<div class="modal-footer ">
										<button type="button" id="ybutton" class="btn btn-success">
											<span class="glyphicon glyphicon-ok-sign"></span>Yes
										</button>
										<button type="button" id="nbutton" class="btn btn-default"
											data-dismiss="modal">
											<span class="glyphicon glyphicon-remove"></span>No
										</button>
									</div>
								</div>
								<!-- /.modal-content -->
							</div>
						</div>
					</div>

					<div class="tab-pane"
						style="overflow-y: hidden; position: relative; margin-left: 300px;"
						id="contact">
						<br /> <br /> <br />
						<div>
							email to : <a href="mailto:JCMS-DEV@timesinternet.in">JCMS-DEV@timesinternet.in</a>
						</div>
						<br /> <br /> <br /> <br />
						<!-- <div id="holder"></div> -->
						<br /> <br /> <br /> <br />
					</div>

					<div class="tab-pane"
						style="overflow-y: hidden; position: relative; margin-left: 300px;"
						id="form">

						<form id="insform">
							<!-- <div class="form-group">
						<input class="form-control" id="mappingname" name="mappingname"
							placeholder="Enter Mapping Name">
					
					          <br><strong>Username</strong></br>
                              <div class="form-group">
								<input class="form-control" id="username" name="username"
									value=${pageContext.request.userPrincipal.name} readonly>
							</div>
							<br><strong>Mode</strong></br>
							<div class="form-group">
								<input class="form-control" id="mode" name="mode"
									value=${data.mode} readonly>
							</div>
							</div> -->
							
					        <br><strong>URI</strong></br>
							<div class="form-group">
								<input class="form-control" id="uri" name="uri"
									placeholder="Enter URI" required>
							</div>
							<br><strong>Channel</strong></br>
							<div class="form-group">
								<select class="form-control" id="channel" name="channel" required>
									<option>Select Channel</option>
									<option>TOI</option>
									<option>NBT</option>
									<option>ET</option>
									<option>GN</option>
									<option>FR</option>
									<option>NBT_GN</option>
									<option>MALAYALAM_GN</option>
									<option>TELUGU_GN</option>
									<option>TAMIL_GN</option>
									<option>VK_GN</option>
									<option>EISAMAY_GN</option>
									<option>MT_GN</option>
									<option>AUTO_NOW</option>
									<option>GADGET_CENTRAL</option>
									<option>MT</option>
									<option>EISAMAY</option>
									<option>VK</option>
									<option>TAMIL_SAMAYAM</option>
									<option>TELUGU_SAMAYAM</option>
									<option>MALAYALAM_SAMAYAM</option>
									<option>IAG</option>
									<option>BI</option>


								</select>

							</div>
							<br><strong>H1-title</strong></br>
							<div class="form-group">
								<input class="form-control" id="title" name="title"
									placeholder="Enter H1 title">
							</div>
												<br><strong>Alt-title</strong></br>
							<div class="form-group">

								<textarea class="form-control" rows="2" id="shortdesc"
									name="shortdesc" placeholder="Enter Alt-title"></textarea>
							</div>
							<br><strong>Meta-description</strong></br>
							<div class="form-group">

								<textarea class="form-control" rows="5" id="longdesc"
									name="longdesc" placeholder="Enter Meta-description"></textarea>
							</div>

							<br> <strong>Atf-Content</strong></br>
							 
							<div class="form-group">

								<textarea class="form-control" rows="5" id="atf"
									name="atf-content" placeholder="Enter ATF Content"></textarea>
							</div>
                            
							 <!-- <div class="form-group">
								<div class="editableHyperlink form-control" id="atf-content" name="atf-content" contenteditable="true"></div>
							</div>  -->

							<br> <strong>Btf-Content</strong></br>
                          
							<div class="form-group">

								<textarea class="form-control" rows="5" id="btf"
									name="btf-content" placeholder="Enter BTF Content"></textarea>
							</div>
							
							
							<!-- <div class="form-group">
								<div class="editableHyperlink form-control" id="btf-content" name="btf-content" contenteditable="true"></div>
							</div> -->
							
							<br> <strong>Canonical Url</strong></br>
							<div class="form-group">

								<textarea class="form-control" rows="2" id="canonicalUrl"
									name="canonicalUrl" placeholder="Enter Canonical Url"></textarea>
							</div>
							
							<br> <strong>M URL</strong></br>
							<div class="form-group">

								<textarea class="form-control" rows="2" id="mUrl"
									name="mUrl" placeholder="Enter M URL"></textarea>
							</div>
							
							<br> <strong>AMP URL</strong></br>
							<div class="form-group">

								<textarea class="form-control" rows="2" id="ampUrl"
									name="ampUrl" placeholder="Enter AMP URL"></textarea>
							</div>
							
							<br> <strong>Meta Keywords</strong></br>
							<div class="form-group">

								<textarea class="form-control" rows="2" id="metakeywords"
									name="metakeywords" placeholder="Enter Meta Keywords"></textarea>
							</div>
							<span id="myForm"></span>
					
							<div class="form-group">
								<button type="button" id="AddTitle" name="Add Title"  onclick="add_fields();" 
								class="btn btn-default">AddTitle</button>
							</div>

							<button type="button" id="submit" name="submit"
								class="btn btn-default">Submit</button>
						<br></br>		
					<!--  <div class="alert alert-success " id ="messttt">
                       <strong>Success!</strong>Data inserted successfully.
                    </div> -->


						</form>

					</div>

					<div class="tab-pane"
						style="overflow-y: hidden; position: relative; margin-left: 300px;"
						id="edit">

						<form id="serform">
							<div class="form-group">
								<input class="form-control" id="Shorturl" name="Shorturl"
									placeholder="Enter Short Url">
						  </div>
						  <div class="form-group">
								<select class="form-control" id="Schannel" name="Schannel" required>
									<option>Select Channel</option>
									<option>TOI</option>
									<option>NBT</option>
									<option>ET</option>
									<option>GN</option>
									<option>FR</option>
									<option>NBT_GN</option>
									<option>MALAYALAM_GN</option>
									<option>TELUGU_GN</option>
									<option>TAMIL_GN</option>
									<option>VK_GN</option>
									<option>EISAMAY_GN</option>
									<option>MT_GN</option>
									<option>AUTO_NOW</option>
									<option>GADGET_CENTRAL</option>
									<option>MT</option>
									<option>EISAMAY</option>
									<option>VK</option>
									<option>TAMIL_SAMAYAM</option>
									<option>TELUGU_SAMAYAM</option>
									<option>MALAYALAM_SAMAYAM</option>
									<option>IAG</option>
									<option>BI</option>
                                </select>

							</div>

								<button type="button" id="sear" name="sear"
									class="btn btn-default">Search</button>
									</form>

								<div id="target"></div>

							</div>
							
							<div class="tab-pane"
						style="overflow-y: hidden; position: relative; margin-left: 300px;"
						id="uploadfile">
								<form method="post" enctype="multipart/form-data"
											  action="/cmsutils/upload/uploadfile">
											  
											    <input type="text" name='filenm' />
											    <input type="file" name="file" accept=".xls,.xlsx" /> 
											    
											    <input
											      type="submit" value="Upload file" id="upload"/>
											</form>
								
							</div>
							
							
						<!--  Documentation starts   -->	
							
							
							<div class="tab-pane"
						style="overflow-y: hidden; position: relative; margin-left: 300px;"
						id="doc">
						
							<p style="color:#455a64"><strong> Navigate the links below to know more about the SEO-PANEL.</strong></p>
						<a href="#access" data-toggle="collapse"><h4 style="color:#0e808b ">Access</h4></a>
						<div id="access" class="collapse">

							<p>
								Send feature to use,username and domain to <strong>
									pankaj.kumar1@timesinternet.in </strong> with approval from the SEO Team
							</p>
						</div>
						 <a href="#accessmode" data-toggle="collapse"><h4 style="color:#0e808b ;">Mode</h4></a>
						<div id="accessmode" class="collapse">

							<p>Use LIVE MODE</p>
								
						</div>
						<a href="#shorturlmapping" data-toggle="collapse"><h4 style="color:#0e808b ;">Short
								Url Mapping</h4></a>
						<div id="shorturlmapping" class="collapse">
							

							<ul>
								<ol>
									<a href="#addshorturlmapping" data-toggle="collapse">Add
										Short Url</a>
									<div id="addshorturlmapping" class="collapse">
										<br>
										<p>
											Enter the actual and short url in the textbox and submit. By
											default mobile is checked. If you need to map the
											corresponding mobile url also, then keep it checked otherwise
											not. For mapping only mobile url, uncheck the mobile and
											enter the mobile urls accordingly.<br>
											<br> A message will be thrown just below the textbox
											stating whether url entered is new or already mapped. If
											already mapped, then you can search that and see the existing
											mapping. Then coordinate with the concerned person whether to
											keep the existing one or update it based on new requirement.
										</p>
										<p>Mapping will reflect on live after an hour(for both new or updated ones). </p>
										<ul>
											<h4>Restrictions</h4>
											<li>Long url should end with .cms</li>
											<li>Urls should not contain any parameters</li>
											<li>Homepage cannot be mapped</li>
											<li>Domain of both urls should be same</li>
											<li>Dev urls cannot be mapped</li>
											<li>One short url for one long url (one to one mapping)</li>
											<li>Make sure you enter a valid url (leading or trailing
												spaces are not allowed)</li>
											<li>Articleshow cannot be mapped</li>
											<li>Maximum 10000 mappings (5000 each for web and wap) for a domain are allowed. </li>
										</ul>
									</div>
									<br>
									<a href="#updateshorturlmapping" data-toggle="collapse">Search
										and update</a>
									<div id="updateshorturlmapping" class="collapse">
										<br>
										<p>
											Search for short or long url you want to update. There is
											modify column which contains a button. Use that to update the
											entry. <br> You can even search for word, template or
											msid also. It will show all mapping that contains the
											searched pattern
										</p>
									</div>
									<br>
									<a href="#removeshorturlmapping" data-toggle="collapse">Remove</a>
									<div id="removeshorturlmapping" class="collapse">

										<br> Just update the existing mapping with some
										random/non-existing url. <br> EX: Initially mapping was like this:<br>
										<br> Short url - <br>
										http://timesofindia.indiatimes.com/videos/lifestyle/deepika-padukone-rules-the-red-carpet-in-marchesa-gown-at-cannes-film-festival
										<br>
										<p>
											Long/actual url : <br>
											http://timesofindia.indiatimes.com/videos/lifestyle/deepika-padukone-rules-the-red-carpet-in-marchesa-gown-at-cannes-film-festival/videoshow/58735159.cms
										</p>
										<p>
											So to remove this, goto search and update option and search
											this and while updating make <br> short url :
											http://timesofindia.indiatimes.com/eheufeweueufe <br>
											Long url : http://timesofindia.indiatimes.com/dhdhj/wve.cms <br>
											And update.
										</p>
									</div>
								</ol>
							</ul>
						</div>
						<a href="#seometadescription" data-toggle="collapse"><h4 style="color:#0e808b ;">SEO
								Meta Description</h4></a>
						<div id="seometadescription" class="collapse">

							<ul>
								<ol>
									<a href="#addseometadescription" data-toggle="collapse">Add
										Seodescrioption</a>
									<div id="addseometadescription" class="collapse">

										<ul>
											<br>
											<li>URI should start with / <br> Ex: URI should be
												like /...
											</li>
											<li>Channel must be selected</li>
											<li>Donot copy and paste directly from any website. Always use notepad.</li>
										</ul>
									</div>
									<br>

									<a href="#updateseometadescription" data-toggle="collapse">Update
										Seodescrioption</a>
									<div id="updateseometadescription" class="collapse">
										<br>
										<ul>
											<li>Search for the URI and channel whose meta tags needs
												to be updated. Make your changes and update</li>
										</ul>
									</div>
								</ol>
							</ul>
						</div>
						
					</div>
							
							
					<!--  Documentation ends   -->		
							
							
							
							
						




					</div>
				</div>
			</div>

		</div>
	</div>
	<hr>
	<!-- <footer
			style="position: absolute; margin-top: -5px; height: 10px; clear: both;" >
			<p>Times Internet Ltd.</p>
		</footer> -->

<script src="script/cke.js"></script>

	<script id="template" type="x-tmpl-mustache">
		<div class="row">
			<div class="col-sm-2"></div>
			<div class="col-sm-8">

				<form>
                             <!-- <br><strong>Username</strong></br>
                              <div class="form-group">
								<input class="form-control" id="usernamee" name="usernamee"
									value=${pageContext.request.userPrincipal.name} readonly>
							</div>
                                <br><strong>Mode</strong></br>
                                <div class="form-group">
								<input class="form-control" id="modee" name="modee"
									value=${data.mode} readonly>
							</div>
							-->
					<br><strong>URI</strong></br>
					<div class="form-group">
						<input class="form-control" id="urii" name="urii" placeholder="Enter URI" value="{{URI}}">
					</div>
					<br><strong>Channel</strong></br>
					<div class="form-group">
						<select class="form-control" id="channell" name="channell" >
							<option value="TOI">TOI</option>
							<option value="NBT">NBT</option>
							<option value="ET">ET</option>
							<option value="GN">GN</option>
                            <option value="FR">FR</option>
                            <option value="NBT_GN">NBT_GN</option>
                            <option value="MALAYALAM_GN">MALAYALAM_GN</option>
							<option value="TELUGU_GN">TELUGU_GN</option>
							<option value="TAMIL_GN">TAMIL_GN</option>
							<option value="VK_GN">VK_GN</option>
							<option value="EISAMAY_GN">EISAMAY_GN</option>
							<option value="MT_GN">MT_GN</option>
                            <option value="AUTO_NOW">AUTO_NOW</option>
                            <option value="GADGET_CENTRAL">GADGET_CENTRAL</option>
                            <option value="MT">MT</option>
							<option value="EISAMAY">EISAMAY</option>
							<option value="VK">VK</option>
							<option value="TAMIL_SAMAYAM">TAMIL_SAMAYAM</option>
							<option value="TELUGU_SAMAYAM">TELUGU_SAMAYAM</option>
							<option value="MALAYALAM_SAMAYAM">MALAYALAM_SAMAYAM</option>
							<option value="IAG">IAG</option>
                            <option value="BI">BI</option>
						</select>
						
					</div>

                    <br><strong>H1-title</strong></br>
					<div class="form-group">
						<input class="form-control" id="titlee" name="titlee" placeholder="Enter H1 title" value="{{H1-title}}">
					</div>
					<br><strong>Alt-title</strong></br>
					<div class="form-group">

						<textarea class="form-control" rows="2" id="shortdescc" name="shortdescc"
							placeholder="Enter Alt-title" >{{Alt-title}}</textarea>
					</div>
					<br><strong>Meta-description</strong></br>
					<div class="form-group">

						<textarea class="form-control" rows="5" id="longdescc"  name="longdescc"
							placeholder="Enter Meta-description">{{Meta-description}}</textarea>
					</div>

                   <br><strong>Atf-Content</strong></br>
           
                    <div class="form-group">

								<textarea class="form-control" rows="5" id="atfupdate"
									name="atf-contentt" placeholder="Enter ATF Content">{{Atf-content}}</textarea>
							</div>

							<br> <strong>Btf-Content</strong></br>
                          
							<div class="form-group">

								<textarea class="form-control" rows="5" id="btfupdate"
									name="btf-contentt" placeholder="Enter BTF Content">{{Btf-content}}</textarea>
							</div>

                   

                    <br> <strong>Canonical Url</strong></br>
							<div class="form-group">

								<textarea class="form-control" rows="2" id="canonicalUrll"
									name="canonicalUrll" placeholder="Enter Canonical Url">{{Canonical-url}}</textarea>
							</div>

					<br> <strong>M URL</strong></br>
							<div class="form-group">

								<textarea class="form-control" rows="2" id="mUrl_up"
									name="mUrl_up" placeholder="Enter M URL">{{m-url}}</textarea>
							</div>
							
					<br> <strong>AMP URL</strong></br>
							<div class="form-group">

								<textarea class="form-control" rows="2" id="ampUrl_up"
									name="ampUrl_up" placeholder="Enter AMP URL">{{amp-url}}</textarea>
							</div>
							
					<br> <strong>Meta Keywords</strong></br>
							<div class="form-group">

								<textarea class="form-control" rows="2" id="metakeywords_up"
									name="metakeywords_up" placeholder="Enter Meta Keywords">{{meta-keyws}}</textarea>
							</div>
						<span id="updtForm">

						{{#rel_map}}
							<div>
							<strong>Add Title </strong>
							<input class="form-control" id="upTitleElement" name="upTitleElement" placeholder="Add Title" value="{{title}}">

							<strong>Add Url </strong>
							<input class="form-control" id="upUrlElement" name="upUrlElement" placeholder="Add Url" value="{{url}}">
							<br/>
							</div>
						{{/rel_map}}

						</span>
					<br/>
					
					<div class="form-group">
						<button type="button" id="uptTitle" name="uptTitle"  class="btn btn-default">AddTitle</button>
					</div>
                           <button type="button" id="upt" name="upt"  class="btn btn-default">Update</button>
                      
                    <br></br>
            <!--
                    <div class="alert alert-success " id ="messtttt">
                       <strong>Success!</strong>Data updated successfully.
                    </div>
					
           -->
					</form>
			</div>
			<div class="col-sm-2"></div>
		</div>
</script>

<!-- Order is important here -->
<script src="script/ckeupd.js"></script>
<script src="script/seometa.js"></script>

<script>

/*
----------------------------------------------------------------------------

Functions that will be called upon, when user click on the Name text field.

----------------------------------------------------------------------------
*/
function add_fields(){
var div = document.createElement('div');
div.setAttribute("class", "form-group");

var strong = document.createElement("strong");
var text = document.createTextNode("Add Title");
strong.appendChild(text);


var y1 = document.createElement("input");
y1.setAttribute("class", "form-control");
y1.setAttribute("placeholder", "Add Title");

y1.setAttribute("name", "titleElement");
y1.setAttribute("id", "titleElement");

div.appendChild(strong);
div.appendChild(y1);

var br = document.createElement("br");
div.appendChild(br);
div.appendChild(br);


var strong = document.createElement("strong");
var text = document.createTextNode("Add Url");
strong.appendChild(text);
div.appendChild(strong);
var y2 = document.createElement("input");
y2.setAttribute("class", "form-control");
y2.setAttribute("placeholder", "Add Url");

y2.setAttribute("name", "urlElement");
y2.setAttribute("id", "urlElement");
div.appendChild(y2);
div.appendChild(br);
div.appendChild(br);


document.getElementById("myForm").appendChild(div);
}

</script>


</body>
</html>
