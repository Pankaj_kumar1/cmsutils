<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Welcome to Edit Page</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src=" /cmsutils/script/mustache.min.js"></script>

</head>
<body>

	<div class="container">
		<div class="row">
			<div class="col-sm-2"></div>
			<div class="col-sm-8">
				<form>
					<div class="form-group">
						<input class="form-control" id="shorturl" name="shorturl"
							placeholder="Enter Short Url">
						<select class="form-control" id="channel" name="channel">
							<option>Select Channel</option>
							<option>TOI</option>
							<option>NBT</option>
							<option>ET</option>
							<option>GN</option>
						</select>
						<div id="target"><button type="button" id="submit" name="submit"
							class="btn btn-default">Edit</button></div>

					</div>
				</form>
			</div>
		</div>
	</div>

	<script id="template" type="x-tmpl-mustache">
	<div class="container">
		<div class="row">
			<div class="col-sm-2"></div>
			<div class="col-sm-8">

				<form>
					<div class="form-group">
						<input class="form-control" id="mappingname" name="mappingname"
							placeholder="Enter Mapping Name" value="{{mappingname}}">
					</div>
					<div class="form-group">
						<input class="form-control" id="uri" name="uri" placeholder="Enter URI" value="{{uri}}">
					</div>
					<div class="form-group">
						<select class="form-control" id="channel" name="channel" >
							<option>Select Channel</option>
							<option value="TOI">TOI</option>
							<option value="NBT" >NBT</option>
							<option value="ET">ET</option>
							<option value="GN<">GN</option>
						
						</select>
						
					</div>

					<div class="form-group">
						<input class="form-control" id="title" name="title" placeholder="Enter H1 title" value="{{title}}">
					</div>
					<div class="form-group">

						<textarea class="form-control" rows="5" id="shortdesc" name="shortdesc"
							placeholder="Enter Alt-title" >{{shortdesc}}</textarea>
					</div>
					<div class="form-group">

						<textarea class="form-control" rows="5" id="longdesc"  name="longdesc"
							placeholder="Enter Meta-description">{{longdesc}}</textarea>
					</div>
                           <button type="button" id="update" name="update"  class="btn btn-default">Update</button>
					
					</form>
			</div>
			<div class="col-sm-2"></div>
		</div>

	</div>
</script>


	<script>
		$(document).ready(function() {


function myFunction() {
    alert("update button clicked");
}



			$("#submit").click(function() {

				var postdata = {};
				postdata["shorturl"] = $("#shorturl").val();

				$.ajax({
					url : "/cmsutils/seo-meta/geturilist",
					data : postdata,
					type : "GET",
					dataType : "json",
				}).done(function(json) {
					var data=json[0];
					 var template = $('#template').html();
					  Mustache.parse(template);   // optional, speeds up future uses
					  var rendered = Mustache.render(template, data);
					  $('#target').html(rendered);
					  $('#channel option[value=' + data["channel"] + ']').attr('selected','selected');
			                       
						$("#update").click(function(){
							
							var postdata = {};
							postdata["mappingname"] = $("#mappingname").val();
							postdata["uri"] = $("#uri").val();
							postdata["channel"] = $("#channel").val();
							postdata["title"] = $("#title").val();
							postdata["shortdesc"] = $("#shortdesc").val();
							postdata["longdesc"] = $("#longdesc").val();
					
							$.ajax({
					    		url: "/cmsutils/seo-meta/insert",
					    		data: postdata,
					    		type: "GET",
					    		dataType : "json",
							}).done(function( json ) {
								
								alert(JSON.stringify(json));
					  		});
			
				
			});
			          



				});

			});

			
		





		});
	</script>

</body>
</html>