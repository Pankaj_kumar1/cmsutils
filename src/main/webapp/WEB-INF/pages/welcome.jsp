
<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="description" content="Text Opening Sequence with CSS Animations" />
		<link rel="stylesheet" type="text/css" href="css/default.css" />
		<link rel="stylesheet" type="text/css" href="css/component.css" />
	</head>
	<body onclick="goto();">
		<div class="container">	
			<div class="os-phrases" id="os-phrases">
				<h2>WELCOME  ${name}</span></h2>
				<h2>this is ${data.mode}</h2>
			 	<h2>TAKE CARE</h2>
			</div>
		</div><!-- /container -->
		<script src="script/jquery.js"></script>
		<script src="script/jquery.lettering.js"></script>
		<script>
		 	window.setTimeout(function(){
				 window.location.href = "?w";
		    }, 10);
			$(document).ready(function() {
				$("#os-phrases > h2").lettering('words').children("span").lettering().children("span").lettering();
			});
			$(document).keyup(function(e) { 
			    window.location.href = "?w";
			});

			function goto(){
			       location.href = "?w"
			}
		</script>
	</body>
</html>