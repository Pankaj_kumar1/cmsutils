var iCnt = 1;
var ver = {};
var pageVer = {};
var keyUrl = [];
var editor;
var isvalidUrl = 0;

$(document)
		.ready(
				function() {
					var container = $(document.createElement('div')).css({
						padding : '15px',
						margin : '0px',
						width : '850px',
						border : '0px dashed',
						borderTopColor : '#999',
						borderBottomColor : '#999',
						borderLeftColor : '#999',
						borderRightColor : '#999'
					});

					var formele = $(document.createElement('form'));
					$(formele).attr('id', 'testform').attr('method', 'post');

					var divSubmit = $(document.createElement('div'));
					$(divSubmit).attr('id', 'divsubmit');
					$(divSubmit)
							.append(
									'<p align="right"><span id="spanr" style="float: left" class="info"></span><input type="button" id="mainsub" data-inline="true" title="Submit all entries" value="Submit" name="Submit" onClick="subForm()" style="height:40px;width:100px;background:#128849;border:1px solid black;color:white" disabled=true><\/p>');
					$(container)
							.append(
									'<div id="urldiv'
											+ '"><span id="num" style="width: 40px; float: left;" class="info"><font color="grey" size="6"><i>'
											+ '</i></font></span><div style="margin-left: 45px; border:1px dashed #999;padding:10px;margin-bottom:15px"><div><i>Actual URL: <\/i>&emsp;&emsp;<input type=text style="width: 750px;background-color:#FCF5D8;" id=actualurl'
											+ ' '
											+ 'name="actualurl'
											+ '"onfocus="if(this.value==this.defaultValue)this.value=\'\'" onblur="if(this.value==\'\')this.value=this.defaultValue"  value="" /><span class="info"><\/div><br/><div><i>Short URL:<\/i>&emsp;&emsp;<input type="text" style="width: 750px;background-color:#FCF5D8" id=shorturl'
											+ ' '
											+ 'name="shorturl'
											+ '" onfocus="if(this.value==this.defaultValue)this.value=\'\'" onblur="if(this.value==\'\')this.value=this.defaultValue" value="" /><span class="info"><\/div><div><input type="checkbox" id="onmobile'
											+ '" value="onmobile" name="onmobile'
											+ '" />On Mobile'
											+ '<input type="hidden" value="a" name="action"/></div></div></div>');

					$(formele).append(container);
					$('#main').before(formele);
					$(container).before(divSubmit);
					$('input[type=text]').on('change', OntextChange);
				});


function subForm() {
	var result = true;
	var id = this.id;
	for ( var i in pageVer) {
		if (pageVer[i] == false) {
			result = false;
			break;
		}
	}

	if (result) {

		bootbox
				.confirm(
						"Are you sure? Please confirm.",
						function(alertbox) {

							if (alertbox) {
								var formData = $('#testform').serializeArray();
								for (j = 0; j < keyUrl.length; j++) {
									formData.push({
										name : 'key' + j,
										value : keyUrl[j]
									});
								}

								formData.push({
									name : 'count',
									value : iCnt
								});

//								var token = $("meta[name='_csrf']").attr(
//										"content");
//								var header = $("meta[name='_csrf_header']")
//										.attr("content");
//								$(document).ajaxSend(function(e, xhr, options) {
//									xhr.setRequestHeader(header, token);
//								});

								$.ajax({
											url : "add",
											type : "GET",
											data : $.param(formData),
											processData : false,
											contentType : false,
											error : function() {
												$("#spanr")
														.html(
																'<font color="red" size="6">&#x2717;  <i>Error in submitting form. Please try again...<\/i><\/font>');
												return false;
											},
											success : function(response) {
												alert(response.message);
												window.location.reload(true);
											}
										});
							}
						});
	} else {
		
		bootbox
				.alert(
						'<font color="red" size="4">&#x2717;  <i>Errors Exist. Please resolve before submitting...<\/i><\/font>',
						function() {
						});

		return false;
	}

}

function showStatus(status) {
	if (status == true) {
		return '<font color="green" size="5">&#x2713</font>';
	}
	return '<font color="red" size="5">&#x2717</font>';

}

function OntextChange() {
	var input = $(this);
	var IsValidURL = ValidUrl(input.val());
	if (IsValidURL == true) {
		var id = this.id;
		if (id != undefined) {
			var i = id.substr(id.length - 1);
			var type = "";
			if (id.indexOf("shorturl") >= 0) {
				type = "shorturl";
			} else if (id.indexOf("actualurl") >= 0) {
				type = "actualurl";
			}
//			var token = $("meta[name='_csrf']").attr("content");
//			var header = $("meta[name='_csrf_header']").attr("content");
//			$(document).ajaxSend(function(e, xhr, options) {
//				xhr.setRequestHeader(header, token);
//			});
			
			$.ajax({
						url : 'get?url=' + input.val() + "&type=" + type,
						error : function() {
							$('#info').html('<p>An error has occurred<\/p>');
						},
						dataType : 'text',
						success : function(data) {
							if (data.indexOf("ERROR:") >= 0) {
								ver[id] = true;
								pageVer[i] = true;
								isvalidUrl++;
								input
										.next('span.info')
										.html(
												'<font color="green">&#x2713; <i>Url entered is new and not mapped..<\/i><\/font>');
								if(isvalidUrl==2){
								$("#mainsub").attr('disabled',false);
								}
								var replaceid = id.replace('s', 'a');

								if ($('#' + replaceid).is(':disabled') == true) {
									$('#' + replaceid).prop('disabled', false);
									$('#' + replaceid).css('background-color','#FCF5D8');
								}
							}else if(data.indexOf("ERROR1:") >= 0){
								ver[id] = true;
								pageVer[i] = true;
								isvalidUrl++;
								input
										.next('span.info')
										.html(
												'<font color="green">&#x2713; <i>Url entered is new and not mapped (both web and mobile) ..<\/i><\/font>');
								if(isvalidUrl==2){
								$("#mainsub").attr('disabled',false);
								}
								var replaceid = id.replace('s', 'a');

								if ($('#' + replaceid).is(':disabled') == true) {
									$('#' + replaceid).prop('disabled', false);
									$('#' + replaceid).css('background-color','#FCF5D8');
						         }
								}else if(data.indexOf("ERROR2:") >= 0){
									ver[id] = true;
									pageVer[i] = true;
									isvalidUrl++;
									input
											.next('span.info')
											.html(
													'<font color="green">&#x2717; <i>Mobile domain not mapped<\/i><\/font><br>'+
													'<font color="red">&#x2713; <i>Web domain mapped<\/i><\/font>');
									if(isvalidUrl==2){
									$("#mainsub").attr('disabled',false);
									}
									var replaceid = id.replace('s', 'a');

									if ($('#' + replaceid).is(':disabled') == true) {
										$('#' + replaceid).prop('disabled', false);
										$('#' + replaceid).css('background-color','#FCF5D8');
							         }
									}else if(data.indexOf("ERROR3:") >= 0){
										ver[id] = true;
										pageVer[i] = true;
										isvalidUrl++;
										input
												.next('span.info')
												.html(
														'<font color="green">&#x2717; <i>Web domain not mapped<\/i><\/font><br>'+
														'<font color="red">&#x2713; <i>Mobile domain mapped<\/i><\/font>');
										if(isvalidUrl==2){
										$("#mainsub").attr('disabled',false);
										}
										var replaceid = id.replace('s', 'a');

										if ($('#' + replaceid).is(':disabled') == true) {
											$('#' + replaceid).prop('disabled', false);
											$('#' + replaceid).css('background-color','#FCF5D8');
								         }
										}else if(data.indexOf("ERROR4:") >= 0){
											ver[id] = true;
											pageVer[i] = true;
											isvalidUrl++;
											input
													.next('span.info')
													.html(
															'<font color="red">&#x2717; <i>Mapped already (both web and mobile) ..<\/i><\/font>');
											if(isvalidUrl==2){
											$("#mainsub").attr('disabled',false);
											}
											var replaceid = id.replace('s', 'a');

											if ($('#' + replaceid).is(':disabled') == true) {
												$('#' + replaceid).prop('disabled', false);
												$('#' + replaceid).css('background-color','#FCF5D8');
									         }
											}else if(data.indexOf("ERROR5:") >= 0){
									
									ver[id] = false;
									input
											.next('span.info')
											.html(
													'<font color="red">&#x2717; <i>Something wrong with the domain. Please check.<\/i><\/font>');
									
									                
									var replaceid = id.replace('s', 'a');
								}else if(data.indexOf("ERROR6:") >= 0){
									
									ver[id] = false;
									input
											.next('span.info')
											.html(
													'<font color="red">&#x2717; <i>Something wrong with the url. Please check.<\/i><\/font>');
									
									                
									var replaceid = id.replace('s', 'a');
								}else{
								
								ver[id] = false;
								input
										.next('span.info')
										.html(
												'<font color="red">&#x2717; <i>Url entered already mapped. Please change.<\/i><\/font>');
								
								                
								var replaceid = id.replace('s', 'a');
								// $('#' + replaceid).val(data);
								/*
								 * if ($('#' + replaceid).is(':disabled') ==
								 * false) { $('#' + replaceid).prop('disabled',
								 * true); $('#' +
								 * replaceid).css('background-color','#DEDEDE'); $(
								 * 'input:radio[name="r' + i +
								 * '"][value="u"]').attr( 'checked', true); }
								 */
							}
							var checkedButton = $(
									'input:radio[name="r' + i + '"]:checked')
									.val();
							if (checkedButton == 'a') {
								pageVer[i] = true;
								keyUrl[i] = 0;
							} else if ((checkedButton == "u")
									&& (ver['tba' + i] || ver['tbs' + i])) {
								pageVer[i] = true;
								if (ver['tba' + i]) {
									keyUrl[i] = 1;
								} else {
									keyUrl[i] = 2;
								}
							} else if ((checkedButton == "r")
									&& (!ver['tba' + i] && !ver['tbs' + i])) {
								pageVer[i] = true;
								keyUrl[i] = 1;
							}

						},
						type : 'GET'
					});
		}
	}else {
		input
				.next('span.info')
				.html(
						'<font color="red">&#x2717; <i>Error: Not Valid URL.<\/i><\/font>');
	}
}

var divValue, values = '';

function GetTextValue() {

	$(divValue).empty();
	$(divValue).remove();
	values = '';

	$('.input').each(function() {
		divValue = $(document.createElement('div')).css({
			padding : '5px',
			width : '200px'
		});
		values += this.value + '<br />'
	});

	$(divValue).append('<p><b>Your selected values<\/b><\/p>' + values);
	$('body').append(divValue);

}

function ValidUrl(str) {
	var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
	'((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
	'((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
	'(\\:\\d+)?(\\/[-a-z\\d%_.~+,]*)*' + // port and path
	'(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
	'(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
	if (!pattern.test(str)) {
		return false;
	} else {
		return true;
	}
}

function formSubmit() {
	document.getElementById("logoutForm").submit();
}

function getCookie(c_name) {
	if (document.cookie.length > 0) {
		c_start = document.cookie.indexOf(c_name + "=");
		if (c_start != -1) {
			c_start = c_start + c_name.length + 1;
			c_end = document.cookie.indexOf(";", c_start);
			if (c_end == -1)
				c_end = document.cookie.length;
			return unescape(document.cookie.substring(c_start, c_end));
		}
	}
	return "";
}
function operateFormatter(value, row, index) {
	return [

			'<a class="edit ml10" id="e'
					+ index
					+ '" data-toggle="modal" data-target="#editmodal" href="javascript:void(0)" title="Edit">',
			'<i class="glyphicon glyphicon-edit"></i>',
			'</a>&nbsp;&nbsp;&nbsp;'
			].join('');
}

window.operateEvents = {
	'click .edit' : function(e, value, row, indexx) {
		// alert('You click edit icon, row: ' + JSON.stringify(row))
		var $id = $(this);
		$('#editmodal').find('[name="msid"]').val(row.catkey).end().find(
				'[name="surl"]').val(row.seolocation).end().find(
				'[name="aurl"]').val(row.overridelink).end().find(
				'[name="hostid"]').val(row.hostid).end().find(
				'[name="instanceid"]').val(row.instanceid).end().find(
				'[name="status"]').val(row.status).end();
		// $("#editmodal").show( "fade", 500 );
		$("#upbutton")
				.off('click')
				.on(
						'click',
						function() {
							// alert('You click edit icon, row: ' +
							// JSON.stringify(row));
							bootbox
									.confirm(
											"Are you sure you want to update? Please confirm.",
											function(alertbox) {
												if (alertbox) {
													row.catkey = $('#editmodal')
															.find(
																	'[name="msid"]')
															.val();
													row.seolocation = $(
															'#editmodal').find(
															'[name="surl"]')
															.val();
													row.overridelink = $(
															'#editmodal').find(
															'[name="aurl"]')
															.val();
													row.hostid = $('#editmodal')
															.find(
																	'[name="hostid"]')
															.val();
													row.instanceid = $(
															'#editmodal')
															.find(
																	'[name="instanceid"]')
															.val();
													row.status = $(
													'#editmodal')
													.find(
															'[name="status"]')
													.val();

//													var token = $(
//															"meta[name='_csrf']")
//															.attr("content");
//													var header = $(
//															"meta[name='_csrf_header']")
//															.attr("content");
													
//													$(document).ajaxSend(
//																	function(e, xhr, options) {
//																		xhr.setRequestHeader(header,token);
//																	});

													$.ajax({
																url : "update?js="
																		+ JSON.stringify(row),
																type : "GET",
																contentType : false,
																error : function() {
																},
																success : function(response) {
																	if (response.result) {
																		var $killrow = $id.parent('td').parent('tr');
																		$killrow.addClass("danger");
																		$killrow.fadeOut(1000,function() {
																							$('.tr').hover(function() {
																												$(this).css("background-color","white")
																											});
																							$("#datatable").bootstrapTable('updateRow',
																											{
																												index : indexx,
																												row : row
																											});
																							$("#editmodal").removeData("modal");
																						});
																	}
																	var str = response.message;
																	if(str.indexOf("ERROR")>=0){
																		alert(response.message);
																		window.location.reload();
																	}else{
																	alert(response.message);
																	}
																	
																}
															});
												}
												$("#editmodal").removeData(
														"modal");
											});

						});

		// $(this).parents('tr').toggle( "highlight" );
	},
	// alert("button");
	'click .remove' : function(e, value, row, index) {
		// alert('You click remove icon, row: ' + JSON.stringify(row));
		// e.preventDefault();
		var $id = $(this);
		// $("#deletemodal").show( "fade", 500 );

		$("#ybutton").off('click').on('click', function() {
//			var token = $("meta[name='_csrf']").attr("content");
//			var header = $("meta[name='_csrf_header']").attr("content");
//			$(document).ajaxSend(function(e, xhr, options) {
//				xhr.setRequestHeader(header, token);
//			});

			$.ajax({
				url : "update?js=" + JSON.stringify(row) + "&o=1",
				type : "GET",
				contentType : false,
				error : function() {
				},
				success : function(response) {
					if (response.result) {
						$("#deletemodal").hide();
						var $killrow = $id.parent('td').parent('tr');
						$killrow.addClass("danger");
						$killrow.fadeOut(2000, function() {
							// $killrow.remove();
							$("#datatable").bootstrapTable('remove', {
								index : index,
								row : row
							});
						});
					}

				}
			});
		});

		$("#nbutton").button().click(function() {
			$("#deletemodal").hide("fade", 500);
			// alert("button");
		});
		// $( "#delete" ).draggable({ containment: "parent" });
		// console.log(value, row, index);
	}
}

$('body').on('hidden.bs.modal', '.modal', function() {
	$(this).removeData('bs.modal');
});

window.onload = function() {

	var r = Raphael("holder", 400, 50);
	var font1 = r.getFont("Sans");
	var fonts = [ 0, r.getFont("1") ];
	// w1 = r.print(30, 30, "CMS Editor", fonts[2], 60).attr({fill: "none",
	// stroke: "#000"}),
	// w2 = r.print(180, 30, "Editor", fonts[2], 60).attr({fill: "none", stroke:
	// "#000"}),
	var chars = [ r.print(20, 40, "Short", font1, 60).attr({
		fill : Raphael.getColor(),
		stroke : "#000"
	}), r.print(190, 40, "URL", font1, 60).attr({
		fill : Raphael.getColor(),
		stroke : "#000"
	}),
	// r.print(260, 30, "URL", fonts[2], 60).attr({fill: "none", stroke:
	// "#000"}),
	r.print(300, 40, "Editor", font1, 60).attr({
		fill : Raphael.getColor(),
		stroke : "#000"
	}), ];

	/*        nums = [
	 r.print(450, 50, "1", fonts[1], 60).attr({fill: "#fff"})  ];
	 */
	/*   w1.animate({
	 2: {fill: Raphael.getColor()},
	 4: {transform: "s3"},
	 6: {transform: ""}
	 }, 5000);*/
	/*   w2.animate({
	 2: {fill: Raphael.getColor()},
	 4: {transform: "s3"},
	 6: {transform: ""}
	 }, 5000);*/
	chars[0].animate({
		4 : {
			transform : "t300,300r-30s3"
		},
		6 : {
			transform : ""
		}
	}, 4000);
	chars[1].animate({
		4 : {
			transform : "t400,80r45s3"
		},
		6 : {
			transform : ""
		}
	}, 4000);
	chars[2].animate({
		4 : {
			transform : "t300,300r-30s3"
		},
		6 : {
			transform : ""
		}
	}, 4000);

	$('path').click(function() {
		chars[0].animate({
			2 : {
				fill : Raphael.getColor(),
				stroke : "#000"
			},
			4 : {
				transform : "t300,300r-30s3"
			},
			6 : {
				transform : ""
			}
		}, 4000);
		chars[1].animate({
			2 : {
				fill : Raphael.getColor(),
				stroke : "#000"
			},
			4 : {
				transform : "t400,80r45s3"
			},
			6 : {
				transform : ""
			}
		}, 4000);
		chars[2].animate({
			2 : {
				fill : Raphael.getColor(),
				stroke : "#000"
			},
			4 : {
				transform : "t300,300r-30s3"
			},
			6 : {
				transform : ""
			}
		}, 4000);

	});

	chars[0].hover(function() {
		this.animate({
			transform : "s3"
		}, 2000, 'super_elastic');
	}, function() {
		this.animate({
			transform : "s1"
		}, 2000, 'super_elastic');
	});
	chars[1].hover(function() {
		this.animate({
			transform : "s3"
		}, 2000, 'super_elastic');
	}, function() {
		this.animate({
			transform : "s1"
		}, 2000, 'super_elastic');
	});
	chars[2].hover(function() {
		this.animate({
			transform : "s3"
		}, 2000, 'super_elastic');
	}, function() {
		this.animate({
			transform : "s1"
		}, 2000, 'super_elastic');
	});

	// modify this function
	Raphael.easing_formulas.super_elastic = function(n) {
		if (n == !!n) {
			return n;
		}
		return Math.pow(2, -10 * n) * Math.sin((n - .075) * (2 * Math.PI) / .3)
				+ 1;
	};

	/*
	chars[3].animate({
	    2: {fill: Raphael.getColor()},
	    4: {transform: "s3"},
	    6: {transform: ""}
	}, 5000);
	nums[0].animate({
	    2: {fill: Raphael.getColor()},
	    3: {transform: "s3"},
	    4: {transform: ""}
	}, 4000);*/
};