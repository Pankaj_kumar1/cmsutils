CKEDITOR.replace('atf', {
	toolbar : [ {
		name : 'document',
		groups : [ 'mode', 'document', 'doctools' ],
		items : [ 'Source' ]
	}, {
		name : 'basicstyles',
		groups : [ 'basicstyles' ],
		items : [ 'Bold', 'Italic', 'Underline' ]
	}, {
		name : 'paragraph',
		groups : [ 'list', 'indent' ],
		items : [ 'NumberedList', 'BulletedList' ]
	}, {
		name : 'links',
		items : [ 'Link', 'Unlink' ]
	}, {
		name : 'insert',
		items : [ 'Table' ]
	}, {
		name : 'styles',
		items : [ 'Format' ]
	},

	]
});

CKEDITOR.replace('btf', {
	toolbar : [ {
		name : 'document',
		groups : [ 'mode', 'document', 'doctools' ],
		items : [ 'Source' ]
	}, {
		name : 'basicstyles',
		groups : [ 'basicstyles' ],
		items : [ 'Bold', 'Italic', 'Underline' ]
	}, {
		name : 'paragraph',
		groups : [ 'list', 'indent' ],
		items : [ 'NumberedList', 'BulletedList' ]
	}, {
		name : 'links',
		items : [ 'Link', 'Unlink' ]
	}, {
		name : 'insert',
		items : [ 'Table' ]
	}, {
		name : 'styles',
		items : [ 'Format' ]
	},

	]
});