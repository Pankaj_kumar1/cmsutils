var bindCkeditor = function() {

	CKEDITOR.replace('atfupdate', {
		toolbar : [ {
			name : 'document',
			groups : [ 'mode', 'document', 'doctools' ],
			items : [ 'Source' ]
		}, {
			name : 'basicstyles',
			groups : [ 'basicstyles' ],
			items : [ 'Bold', 'Italic', 'Underline' ]
		}, {
			name : 'paragraph',
			groups : [ 'list', 'indent' ],
			items : [ 'NumberedList', 'BulletedList' ]
		}, {
			name : 'links',
			items : [ 'Link', 'Unlink' ]
		}, {
			name : 'insert',
			items : [ 'Table' ]
		}, {
			name : 'styles',
			items : [ 'Format' ]
		},

		]
	});

	CKEDITOR.replace('btfupdate', {
		toolbar : [ {
			name : 'document',
			groups : [ 'mode', 'document', 'doctools' ],
			items : [ 'Source' ]
		}, {
			name : 'basicstyles',
			groups : [ 'basicstyles' ],
			items : [ 'Bold', 'Italic', 'Underline' ]
		}, {
			name : 'paragraph',
			groups : [ 'list', 'indent' ],
			items : [ 'NumberedList', 'BulletedList' ]
		}, {
			name : 'links',
			items : [ 'Link', 'Unlink' ]
		}, {
			name : 'insert',
			items : [ 'Table' ]
		}, {
			name : 'styles',
			items : [ 'Format' ]
		},

		]
	});
}


var bindDynaTitle = function() {

	CKEDITOR.replace('atfupdate', {
		toolbar : [ {
			name : 'document',
			groups : [ 'mode', 'document', 'doctools' ],
			items : [ 'Source' ]
		}, {
			name : 'basicstyles',
			groups : [ 'basicstyles' ],
			items : [ 'Bold', 'Italic', 'Underline' ]
		}, {
			name : 'paragraph',
			groups : [ 'list', 'indent' ],
			items : [ 'NumberedList', 'BulletedList' ]
		}, {
			name : 'links',
			items : [ 'Link', 'Unlink' ]
		}, {
			name : 'insert',
			items : [ 'Table' ]
		}, {
			name : 'styles',
			items : [ 'Format' ]
		},

		]
	});

	CKEDITOR.replace('btfupdate', {
		toolbar : [ {
			name : 'document',
			groups : [ 'mode', 'document', 'doctools' ],
			items : [ 'Source' ]
		}, {
			name : 'basicstyles',
			groups : [ 'basicstyles' ],
			items : [ 'Bold', 'Italic', 'Underline' ]
		}, {
			name : 'paragraph',
			groups : [ 'list', 'indent' ],
			items : [ 'NumberedList', 'BulletedList' ]
		}, {
			name : 'links',
			items : [ 'Link', 'Unlink' ]
		}, {
			name : 'insert',
			items : [ 'Table' ]
		}, {
			name : 'styles',
			items : [ 'Format' ]
		},

		]
	});
}


/*
----------------------------------------------------------------------------

Functions that will be called upon, when user click on the Name text field.

----------------------------------------------------------------------------
*/
var uptd_fields = function() {


var div = document.createElement('div');


var strong = document.createElement("strong");
var text = document.createTextNode("Add Title");
strong.appendChild(text);


var y1 = document.createElement("input");
y1.setAttribute("class", "form-control");
y1.setAttribute("placeholder", "Add Title");

y1.setAttribute("name", "upTitleElement");
y1.setAttribute("id", "upTitleElement");

div.appendChild(strong);
div.appendChild(y1);

var br = document.createElement("br");



var strong = document.createElement("strong");
var text = document.createTextNode("Add Url");
strong.appendChild(text);
div.appendChild(strong);
var y2 = document.createElement("input");
y2.setAttribute("class", "form-control");
y2.setAttribute("placeholder", "Add Url");

y2.setAttribute("name", "upUrlElement");
y2.setAttribute("id", "upUrlElement");
div.appendChild(y2);

div.appendChild(br);

document.getElementById("updtForm").appendChild(div);
}
