
/**/(function($){$.fn.makeEditable=function(options){var iDisplayStart=0;function fnGetCellID(cell){//
 return properties.fnGetRowID($(cell.parentNode));}
function _fnSetRowIDInAttribute(row,id,overwrite){if(overwrite){row.attr("id",id);}else{if(row.attr("id")==null||row.attr("id")=="")
row.attr("id",id);}}
function _fnGetRowIDFromAttribute(row){return row.attr("id");}
function _fnSetRowIDInFirstCell(row,id){//
$("td:first",row).html(id);}
function _fnGetRowIDFromFirstCell(row){return $("td:first",row).html();}
var oTable;var oAddNewRowButton,oDeleteRowButton,oConfirmRowAddingButton,oCancelRowAddingButton;var oAddNewRowForm;var properties;function _fnShowError(errorText,action){//
alert(errorText);}
function _fnStartProcessingMode(){if(oTable.fnSettings().oFeatures.bProcessing){$(".dataTables_processing").css('visibility','visible');}}
function _fnEndProcessingMode(){if(oTable.fnSettings().oFeatures.bProcessing){$(".dataTables_processing").css('visibility','hidden');}}
var sOldValue,sNewCellValue,sNewCellDislayValue;function fnApplyEditable(aoNodes){if(properties.bDisableEditing)
return;var oDefaultEditableSettings={event:'dblclick',"onsubmit":function(settings,original){sOldValue=original.revert;sNewCellValue=null;sNewCellDisplayValue=null;if(settings.type=="text"||settings.type=="select"||settings.type=="textarea")
{var input=$("input,select,textarea",this);sNewCellValue=$("input,select,textarea",$(this)).val();if(input.length==1){var oEditElement=input[0];if(oEditElement.nodeName.toLowerCase()=="select"||oEditElement.tagName.toLowerCase()=="select")
sNewCellDisplayValue=$("option:selected",oEditElement).text();else
sNewCellDisplayValue=sNewCellValue;}
if(!properties.fnOnEditing(input))
return false;var x=settings;if(settings.cssclass!=null){input.addClass(settings.cssclass);if(!input.valid()||0==input.valid())
return false;else
 return true;}}
iDisplayStart=fnGetDisplayStart();properties.fnStartProcessingMode();},"submitdata":function(value,settings){var id=fnGetCellID(this);var rowId=oTable.fnGetPosition(this)[0];var columnPosition=oTable.fnGetPosition(this)[1];var columnId=oTable.fnGetPosition(this)[2];var sColumnName=oTable.fnSettings().aoColumns[columnId].sName;if(sColumnName==null||sColumnName=="")
sColumnName=oTable.fnSettings().aoColumns[columnId].sTitle;var updateData=null;if(properties.aoColumns==null||properties.aoColumns[columnId]==null){updateData=$.extend({},properties.oUpdateParameters,{"id":id,"rowId":rowId,"columnPosition":columnPosition,"columnId":columnId,"columnName":sColumnName});}
else{updateData=$.extend({},properties.oUpdateParameters,properties.aoColumns[columnId].oUpdateParameters,{"id":id,"rowId":rowId,
"columnPosition":columnPosition,"columnId":columnId,"columnName":sColumnName});}
return updateData;},"callback":function(sValue,settings){ properties.fnEndProcessingMode();var status="";var aPos=oTable.fnGetPosition(this);if(properties.sSuccessResponse=="IGNORE"||(properties.aoColumns!=null&&properties.aoColumns[aPos[2]]!=null&&properties.aoColumns[aPos[2]].sSuccessResponse=="IGNORE")||(sNewCellValue==sValue)||properties.sSuccessResponse==sValue){if(sNewCellDisplayValue==null)
{oTable.fnUpdate(sValue,aPos[0],aPos[2]);}else{oTable.fnUpdate(sNewCellDisplayValue,aPos[0],aPos[2]);}
$("td.last-updated-cell",oTable).removeClass("last-updated-cell");$(this).addClass("last-updated-cell");status="success";}else{oTable.fnUpdate(sOldValue,aPos[0],aPos[2]);properties.fnShowError(sValue,"update");status="failure";}
properties.fnOnEdited(status,sOldValue,sNewCellDisplayValue,aPos[0],aPos[1],aPos[2]);if(settings.fnOnCellUpdated!=null){settings.fnOnCellUpdated(status,sValue,aPos[0],aPos[2],settings);}
fnSetDisplayStart();},"onerror":function(){properties.fnEndProcessingMode();properties.fnShowError("Cell cannot be updated","update");properties.fnOnEdited("failure");},"height":properties.sEditorHeight,"width":properties.sEditorWidth};var cells=null;if(properties.aoColumns!=null){for(var iDTindex=0,iDTEindex=0;iDTindex<oSettings.aoColumns.length;iDTindex++){if(oSettings.aoColumns[iDTindex].bVisible){if(properties.aoColumns[iDTEindex]==null){iDTEindex++;continue;}
cells=$("td:nth-child("+(iDTEindex+1)+")",aoNodes);var oColumnSettings=oDefaultEditableSettings;oColumnSettings=$.extend({},oDefaultEditableSettings,properties.oEditableSettings,properties.aoColumns[iDTEindex]);iDTEindex++;var sUpdateURL=properties.sUpdateURL;try{if(oColumnSettings.sUpdateURL!=null)
sUpdateURL=oColumnSettings.sUpdateURL;}catch(ex){}
cells.each(function(){if(!$(this).hasClass(properties.sReadOnlyCellClass)){$(this).editable(sUpdateURL,oColumnSettings);}});}}}else{cells=$('td:not(.'+properties.sReadOnlyCellClass+')',aoNodes);cells.editable(properties.sUpdateURL,$.extend({},oDefaultEditableSettings,properties.oEditableSettings));}}
function fnOnRowAdding(event){//
if(properties.fnOnAdding()){if(oAddNewRowForm.valid()){iDisplayStart=fnGetDisplayStart();properties.fnStartProcessingMode();if(properties.bUseFormsPlugin){$(oAddNewRowForm).ajaxSubmit({dataType:'xml',success:function(response,statusString,xhr){if(xhr.responseText.toLowerCase().indexOf("error")!=-1){properties.fnEndProcessingMode();properties.fnShowError(xhr.responseText.replace("Error",""),"add");properties.fnOnAdded("failure");}else{fnOnRowAdded(xhr.responseText);}},error:function(response){properties.fnEndProcessingMode();properties.fnShowError(response.responseText,"add");properties.fnOnAdded("failure");}});}else{var params=oAddNewRowForm.serialize();$.ajax({'url':properties.sAddURL,'data':params,'type':properties.sAddHttpMethod,'dataType':properties.sAddDataType,success:fnOnRowAdded,error:function(response){properties.fnEndProcessingMode(); properties.fnShowError(response.responseText,"add");properties.fnOnAdded("failure");}});}}}
event.stopPropagation();event.preventDefault();}
function _fnOnNewRowPosted(data){return true;}
function fnAddRowFromForm(oForm){//
var oSettings=oTable.fnSettings();var iColumnCount=oSettings.aoColumns.length;var values=new Array();$("input:text[rel],input:radio[rel][checked],input:hidden[rel],select[rel],textarea[rel],span.datafield[rel],input:checkbox[rel]",oForm).each(function(){var rel=$(this).attr("rel");var sCellValue="";if(rel>=iColumnCount)
properties.fnShowError("In the add form is placed input element with the name '"+$(this).attr("name")+"' with the 'rel' attribute that must be less than a column count - "+iColumnCount,"add");else{if(this.nodeName.toLowerCase()=="select"||this.tagName.toLowerCase()=="select"){sCellValue=$.map($.makeArray($("option:selected",this)),function(n,i){return $(n).text();}).join(",");}
else if(this.nodeName.toLowerCase()=="span"||this.tagName.toLowerCase()=="span")
sCellValue=$(this).html();else{if(this.type=="checkbox"){if(this.checked)
sCellValue=(this.value!="on")?this.value:"true";else
sCellValue=(this.value!="on")?"":"false";}else
sCellValue=this.value;}
sCellValue=sCellValue.replace(properties.sIDToken,data);values[rel]=sCellValue;}
});var rtn=oTable.fnAddData(values);var oTRAdded=oTable.fnGetNodes(rtn);fnApplyEditable(oTRAdded);}
function fnOnRowAdded(data){properties.fnEndProcessingMode();if(properties.fnOnNewRowPosted(data)){var oSettings=oTable.fnSettings();if(!oSettings.oFeatures.bServerSide){var iColumnCount=oSettings.aoColumns.length;var values=new Array();var rowData=new Object();$("input:text[rel],input:radio[rel][checked],input:hidden[rel],select[rel],textarea[rel],span.datafield[rel],input:checkbox[rel]",oAddNewRowForm).each(function(){var rel=$(this).attr("rel");var sCellValue="";if(rel>=iColumnCount)
properties.fnShowError("In the add form is placed input element with the name '"+$(this).attr("name")+"' with the 'rel' attribute that must be less than a column count - "+iColumnCount,"add");else{if(this.nodeName.toLowerCase()=="select"||this.tagName.toLowerCase()=="select"){sCellValue=$.map($.makeArray($("option:selected",this)),function(n,i){return $(n).text();}).join(",");}
else if(this.nodeName.toLowerCase()=="span"||this.tagName.toLowerCase()=="span")
sCellValue=$(this).html();else{if(this.type=="checkbox"){if(this.checked)
sCellValue=(this.value!="on")?this.value:"true";else
sCellValue=(this.value!="on")?"":"false";}else
sCellValue=this.value;}
sCellValue=sCellValue.replace(properties.sIDToken,data);if(oSettings.aoColumns!=null&&oSettings.aoColumns[rel]!=null&&isNaN(parseInt(oSettings.aoColumns[0].mDataProp))){rowData[oSettings.aoColumns[rel].mDataProp]=sCellValue;}else{values[rel]=sCellValue;}}});var rtn;if(oSettings.aoColumns!=null&&isNaN(parseInt(oSettings.aoColumns[0].mDataProp))){rtn=oTable.fnAddData(rowData);}
else{rtn=oTable.fnAddData(values);}
var oTRAdded=oTable.fnGetNodes(rtn);properties.fnSetRowID($(oTRAdded),data,true);fnApplyEditable(oTRAdded);$("tr.last-added-row",oTable).removeClass("last-added-row");$(oTRAdded).addClass("last-added-row");}else{oTable.fnDraw(false);}
oAddNewRowForm.dialog('close');$(oAddNewRowForm)[0].reset();$(".error",$(oAddNewRowForm)).html("");fnSetDisplayStart();properties.fnOnAdded("success");}}
function fnOnCancelRowAdding(event){//
$(oAddNewRowForm).validate().resetForm();$(oAddNewRowForm)[0].reset();$(".error",$(oAddNewRowForm)).html("");$(".error",$(oAddNewRowForm)).hide();oAddNewRowForm.dialog('close');event.stopPropagation();event.preventDefault();}
function fnDisableDeleteButton(){if(properties.oDeleteRowButtonOptions!=null){oDeleteRowButton.button("option","disabled",true);}else{oDeleteRowButton.attr("disabled","true");}}
function fnEnableDeleteButton(){if(properties.oDeleteRowButtonOptions!=null){oDeleteRowButton.button("option","disabled",false);}else{oDeleteRowButton.removeAttr("disabled");}}
function fnDeleteRow(id,sDeleteURL){var sURL=sDeleteURL;if(sDeleteURL==null)
sURL=properties.sDeleteURL;properties.fnStartProcessingMode();var data=$.extend(properties.oDeleteParameters,{"id":id});$.ajax({'url':sURL,'type':properties.sDeleteHttpMethod,'data':data,"success":fnOnRowDeleted,"dataType":properties.sDeleteDataType,"error":function(response){properties.fnEndProcessingMode();properties.fnShowError(response.responseText,"delete");properties.fnOnDeleted("failure");}});}
function _fnOnRowDelete(event){
iDisplayStart=fnGetDisplayStart();if($('tr.'+properties.sSelectedRowClass+' td',oTable).length==0){fnDisableDeleteButton();return;}
var id=fnGetCellID($('tr.'+properties.sSelectedRowClass+' td',oTable)[0]);if(properties.fnOnDeleting($('tr.'+properties.sSelectedRowClass,oTable),id,fnDeleteRow)){fnDeleteRow(id);}}
function fnOnRowDeleted(response){properties.fnEndProcessingMode();var oTRSelected=$('tr.'+properties.sSelectedRowClass,oTable)[0];if(response==properties.sSuccessResponse||response==""){oTable.fnDeleteRow(oTRSelected);fnDisableDeleteButton();fnSetDisplayStart();properties.fnOnDeleted("success");}
else{properties.fnShowError(response,"delete");properties.fnOnDeleted("failure");}}
function _fnOnDeleting(tr,id,fnDeleteRow){
return confirm("Are you sure that you want to delete this record?");;}
function _fnOnDeleted(result){}
function _fnOnEditing(input){return true;}
function _fnOnEdited(result,sOldValue,sNewValue,iRowIndex,iColumnIndex,iRealColumnIndex){}
function fnOnAdding(){return true;}
function _fnOnAdded(result){}
var oSettings;function fnGetDisplayStart(){return oSettings._iDisplayStart;}
function fnSetDisplayStart(){if(oSettings.oFeatures.bServerSide===false){oSettings._iDisplayStart=iDisplayStart;oSettings.oApi._fnCalculateEnd(oSettings);oSettings.oApi._fnDraw(oSettings);}}
function _fnOnBeforeAction(sAction){return true;}
function _fnOnActionCompleted(sStatus){}
function fnGetActionSettings(sAction){if(properties.aoTableAction)
properties.fnShowError("Configuration error - aoTableAction setting are not set",sAction);var i=0;for(i=0;i<properties.aoTableActions.length;i++){if(properties.aoTableActions[i].sAction==sAction)
return properties.aoTableActions[i];}
properties.fnShowError("Cannot find action configuration settings",sAction);}
function fnUpdateRow(oActionForm){var sAction=$(oActionForm).attr("id");sAction=sAction.replace("form","");var sActionURL=$(oActionForm).attr("action");if(properties.fnOnBeforeAction(sAction)){if($(oActionForm).valid()){iDisplayStart=fnGetDisplayStart();properties.fnStartProcessingMode();if(properties.bUseFormsPlugin){var oAjaxSubmitOptions={success:function(response,statusString,xhr){properties.fnEndProcessingMode();if(response.toLowerCase().indexOf("error")!=-1||statusString!="success"){properties.fnShowError(response,sAction);properties.fnOnActionCompleted("failure");}else{fnUpdateRowOnSuccess(oActionForm);properties.fnOnActionCompleted("success");}},error:function(response){properties.fnEndProcessingMode();properties.fnShowError(response.responseText,sAction);properties.fnOnActionCompleted("failure");}};var oActionSettings=fnGetActionSettings(sAction);oAjaxSubmitOptions=$.extend({},properties.oAjaxSubmitOptions,oAjaxSubmitOptions);$(oActionForm).ajaxSubmit(oAjaxSubmitOptions);}else{var params=$(oActionForm).serialize();$.ajax({'url':sActionURL,'data':params,'type':properties.sAddHttpMethod,'dataType':properties.sAddDataType,success:function(response){properties.fnEndProcessingMode();fnUpdateRowOnSuccess(oActionForm);properties.fnOnActionCompleted("success");},error:function(response){properties.fnEndProcessingMode();properties.fnShowError(response.responseText,sAction);properties.fnOnActionCompleted("failure");}});}}}}
function fnUpdateRowOnSuccess(oActionForm){var iRowID=jQuery.data(oActionForm,'ROWID');var oSettings=oTable.fnSettings();var iColumnCount=oSettings.aoColumns.length;var values=new Array();var sAction=$(oActionForm).attr("id");sAction=sAction.replace("form","");$("input:text[rel],input:radio[rel][checked],input:hidden[rel],select[rel],textarea[rel],span.datafield[rel],input:checkbox[rel]",oActionForm).each(function(){var rel=$(this).attr("rel");var sCellValue="";if(rel>=iColumnCount)
properties.fnShowError("In the add form is placed input element with the name '"+$(this).attr("name")+"' with the 'rel' attribute that must be less than a column count - "+iColumnCount,"add");else{if(this.nodeName.toLowerCase()=="select"||this.tagName.toLowerCase()=="select"){sCellValue=$.map($.makeArray($("option:selected",this)), function(n,i){return $(n).text();}).join(",");}
else if(this.nodeName.toLowerCase()=="span"||this.tagName.toLowerCase()=="span")
sCellValue=$(this).html();else{if(this.type=="checkbox"){if(this.checked)
sCellValue=(this.value!="on")?this.value:"true";else
sCellValue=(this.value!="on")?"":"false";}else
sCellValue=this.value;}
oTable.fnUpdate(sCellValue,iRowID,rel);}});$(oActionForm).dialog('close');}
oTable=this;var defaults={sUpdateURL:"UpdateData",sAddURL:"AddData",sDeleteURL:"DeleteData",sAddNewRowFormId:"formAddNewRow",oAddNewRowFormOptions:{autoOpen:false,modal:true},sAddNewRowButtonId:"btnAddNewRow",oAddNewRowButtonOptions:null,sAddNewRowOkButtonId:"btnAddNewRowOk",sAddNewRowCancelButtonId:"btnAddNewRowCancel",oAddNewRowOkButtonOptions:{label:"Ok"},oAddNewRowCancelButtonOptions:{label:"Cancel"},sDeleteRowButtonId:"btnDeleteRow",oDeleteRowButtonOptions:null,sSelectedRowClass:"row_selected",sReadOnlyCellClass:"read_only",sAddDeleteToolbarSelector:".add_delete_toolbar",fnShowError:_fnShowError,fnStartProcessingMode:_fnStartProcessingMode,fnEndProcessingMode:_fnEndProcessingMode,aoColumns:null,fnOnDeleting:_fnOnDeleting,fnOnDeleted:_fnOnDeleted,fnOnAdding:fnOnAdding,fnOnNewRowPosted:_fnOnNewRowPosted,fnOnAdded:_fnOnAdded,fnOnEditing:_fnOnEditing,fnOnEdited:_fnOnEdited,sAddHttpMethod:'POST',sAddDataType:"text",sDeleteHttpMethod:'POST',sDeleteDataType:"text",fnGetRowID:_fnGetRowIDFromAttribute,fnSetRowID:_fnSetRowIDInAttribute,sEditorHeight:"100%",sEditorWidth:"100%",bDisableEditing:false,oDeleteParameters:{},oUpdateParameters:{},sIDToken:"DATAROWID",aoTableActions:null,fnOnBeforeAction:_fnOnBeforeAction,bUseFormsPlugin:false,fnOnActionCompleted:_fnOnActionCompleted,sSuccessResponse:"ok"};properties=$.extend(defaults,options);oSettings=oTable.fnSettings();return this.each(function(){if(oTable.fnSettings().sAjaxSource!=null){oTable.fnSettings().aoDrawCallback.push({"fn":function(){fnApplyEditable(oTable.fnGetNodes());$(oTable.fnGetNodes()).each(function(){var position=oTable.fnGetPosition(this);var id=oTable.fnGetData(position)[0];properties.fnSetRowID($(this),id);});},"sName":"fnApplyEditable"});}else{fnApplyEditable(oTable.fnGetNodes());}
 oAddNewRowForm=$("#"+properties.sAddNewRowFormId);if(oAddNewRowForm.length!=0){var oSettings=oTable.fnSettings();var iColumnCount=oSettings.aoColumns.length;for(i=0;i<iColumnCount;i++){if($("[rel="+i+"]",oAddNewRowForm).length==0)
properties.fnShowError("In the form that is used for adding new records cannot be found an input element with rel="+i+" that will be bound to the value in the column "+i+". See http://code.google.com/p/jquery-datatables-editable/wiki/AddingNewRecords#Add_new_record_form for more details","init");}
if(properties.oAddNewRowFormOptions!=null){properties.oAddNewRowFormOptions.autoOpen=false;}else{properties.oAddNewRowFormOptions={autoOpen:false};}
oAddNewRowForm.dialog(properties.oAddNewRowFormOptions);oAddNewRowButton=$("#"+properties.sAddNewRowButtonId);if(oAddNewRowButton.length!=0){oAddNewRowButton.click(function(){oAddNewRowForm.dialog('open');});}else{if($(properties.sAddDeleteToolbarSelector).length==0){throw "Cannot find a button with an id '"+properties.sAddNewRowButtonId+"', or placeholder with an id '"+properties.sAddDeleteToolbarSelector+"' that should be used for adding new row although form for adding new record is specified";}else{oAddNewRowButton=null;}}
if(oAddNewRowForm[0].nodeName.toLowerCase()=="form"){oAddNewRowForm.unbind('submit');oAddNewRowForm.submit(function(event){fnOnRowAdding(event);return false;});}else{$("form",oAddNewRowForm[0]).unbind('submit');$("form",oAddNewRowForm[0]).submit(function(event){fnOnRowAdding(event);return false;});}
var aAddNewRowFormButtons=[];oConfirmRowAddingButton=$("#"+properties.sAddNewRowOkButtonId,oAddNewRowForm); if(oConfirmRowAddingButton.length==0){if(properties.oAddNewRowOkButtonOptions.text==null||properties.oAddNewRowOkButtonOptions.text==""){properties.oAddNewRowOkButtonOptions.text="Ok";}
properties.oAddNewRowOkButtonOptions.click=fnOnRowAdding;properties.oAddNewRowOkButtonOptions.id=properties.sAddNewRowOkButtonId;
aAddNewRowFormButtons.push(properties.oAddNewRowOkButtonOptions);}else{oConfirmRowAddingButton.click(fnOnRowAdding);}
oCancelRowAddingButton=$("#"+properties.sAddNewRowCancelButtonId);if(oCancelRowAddingButton.length==0){if(properties.oAddNewRowCancelButtonOptions.text==null||properties.oAddNewRowCancelButtonOptions.text==""){properties.oAddNewRowCancelButtonOptions.text="Cancel";}
properties.oAddNewRowCancelButtonOptions.click=fnOnCancelRowAdding;properties.oAddNewRowCancelButtonOptions.id=properties.sAddNewRowCancelButtonId;aAddNewRowFormButtons.push(properties.oAddNewRowCancelButtonOptions);}else{oCancelRowAddingButton.click(fnOnCancelRowAdding);}
if(aAddNewRowFormButtons.length>0){oAddNewRowForm.dialog('option','buttons',aAddNewRowFormButtons);}
oConfirmRowAddingButton=$("#"+properties.sAddNewRowOkButtonId);oCancelRowAddingButton=$("#"+properties.sAddNewRowCancelButtonId);}else{oAddNewRowForm=null;}
oDeleteRowButton=$('#'+properties.sDeleteRowButtonId);if(oDeleteRowButton.length!=0)
oDeleteRowButton.click(_fnOnRowDelete);else{oDeleteRowButton=null;}
oAddDeleteToolbar=$(properties.sAddDeleteToolbarSelector);if(oAddDeleteToolbar.length!=0){if(oAddNewRowButton==null&&properties.sAddNewRowButtonId!=""&&oAddNewRowForm!=null){oAddDeleteToolbar.append("<button id='"+properties.sAddNewRowButtonId+"' class='add_row'>Add</button>");oAddNewRowButton=$("#"+properties.sAddNewRowButtonId);oAddNewRowButton.click(function(){oAddNewRowForm.dialog('open');});}
if(oDeleteRowButton==null&&properties.sDeleteRowButtonId!=""){oAddDeleteToolbar.append("<button id='"+properties.sDeleteRowButtonId+"' class='delete_row'>Delete</button>");oDeleteRowButton=$("#"+properties.sDeleteRowButtonId);oDeleteRowButton.click(_fnOnRowDelete);}}
if(oDeleteRowButton!=null){if(properties.oDeleteRowButtonOptions!=null){oDeleteRowButton.button(properties.oDeleteRowButtonOptions);}
fnDisableDeleteButton();}
if(oAddNewRowButton!=null){if(properties.oAddNewRowButtonOptions!=null){oAddNewRowButton.button(properties.oAddNewRowButtonOptions);}}
if(oConfirmRowAddingButton!=null){if(properties.oAddNewRowOkButtonOptions!=null){oConfirmRowAddingButton.button(properties.oAddNewRowOkButtonOptions);}}
if(oCancelRowAddingButton!=null){if(properties.oAddNewRowCancelButtonOptions!=null){oCancelRowAddingButton.button(properties.oAddNewRowCancelButtonOptions);}}
$(".table-action-deletelink",oTable).live("click",function(e){e.preventDefault();e.stopPropagation();var sURL=$(this).attr("href");if(sURL==null||sURL=="")
sURL=properties.sDeleteURL;iDisplayStart=fnGetDisplayStart();var oTD=($(this).parents('td'))[0];var oTR=($(this).parents('tr'))[0];$(oTR).addClass(properties.sSelectedRowClass);var id=fnGetCellID(oTD);if(properties.fnOnDeleting(oTD,id,fnDeleteRow)){fnDeleteRow(id,sURL);}});$(".table-action-editlink",oTable).live("click",function(e){e.preventDefault();e.stopPropagation();var sURL=$(this).attr("href");if(sURL==null||sURL=="")
sURL=properties.sDeleteURL;iDisplayStart=fnGetDisplayStart();var oTD=($(this).parents('td'))[0];var oTR=($(this).parents('tr'))[0];$(oTR).addClass(properties.sSelectedRowClass);var id=fnGetCellID(oTD);if(properties.fnOnDeleting(oTD,id,fnDeleteRow)){fnDeleteRow(id,sURL);}});$("tbody",oTable).click(function(event){if($(event.target.parentNode).hasClass(properties.sSelectedRowClass)){$(event.target.parentNode).removeClass(properties.sSelectedRowClass);if(oDeleteRowButton!=null){fnDisableDeleteButton();}}else{$(oTable.fnSettings().aoData).each(function(){$(this.nTr).removeClass(properties.sSelectedRowClass);});$(event.target.parentNode).addClass(properties.sSelectedRowClass);if(oDeleteRowButton!=null){fnEnableDeleteButton();}}});if(properties.aoTableActions!=null){for(var i=0;i<properties.aoTableActions.length;i++){var oTableAction=$.extend({sType:"edit"},properties.aoTableActions[i]);var sAction=oTableAction.sAction;var sActionFormId=oTableAction.sActionFormId;var oActionForm=$("#form"+sAction);if(oActionForm.length!=0){var oFormOptions={autoOpen:false,modal:true};oFormOptions=$.extend({},oTableAction.oFormOptions,oFormOptions);oActionForm.dialog(oFormOptions);oActionForm.data("action-options",oTableAction);var oActionFormLink=$(".table-action-"+sAction);if(oActionFormLink.length!=0){oActionFormLink.live("click",function(){var sClass=this.className;var classList=sClass.split(/\s+/);var sActionFormId="";var sAction="";for(i=0;i<classList.length;i++){if(classList[i].indexOf("table-action-")>-1){sAction=classList[i].replace("table-action-","");sActionFormId="#form"+sAction;}}
if(sActionFormId==""){properties.fnShowError("Cannot find a form with an id "+sActionFormId +" that should be associated to the action - "+sAction,sAction)}
var oTableAction=$(sActionFormId).data("action-options");if(oTableAction.sType=="edit"){var oTD=($(this).parents('td'))[0];var oTR=($(this).parents('tr'))[0];$(oTR).addClass(properties.sSelectedRowClass);var iRowID=oTable.fnGetPosition(oTR);var id=fnGetCellID(oTD);$(sActionFormId).validate().resetForm();jQuery.data($(sActionFormId)[0],'DATARECORDID',id);$("input.DATARECORDID",$(sActionFormId)).val(id);jQuery.data($(sActionFormId)[0],'ROWID',iRowID);$("input.ROWID",$(sActionFormId)).val(iRowID);var oSettings=oTable.fnSettings();var iColumnCount=oSettings.aoColumns.length;$("input:text[rel],input:radio[rel][checked],input:hidden[rel],select[rel],textarea[rel],input:checkbox[rel]",$(sActionFormId)).each(function(){var rel=$(this).attr("rel");if(rel>=iColumnCount)
properties.fnShowError("In the action form is placed input element with the name '"+$(this).attr("name")+"' with the 'rel' attribute that must be less than a column count - "+iColumnCount,"add");else{var sCellValue=oTable.fnGetData(oTR)[rel];if(this.nodeName.toLowerCase()=="select"||this.tagName.toLowerCase()=="select"){$(this).attr("value",sCellValue);}
else if(this.nodeName.toLowerCase()=="span"||this.tagName.toLowerCase()=="span")
$(this).html(sCellValue);else{if(this.type=="checkbox"){if(sCellValue=="true"){$(this).attr("checked",true);}}else
{if(this.type=="radio"){if(this.value==sCellValue){this.checked=true;}}else{this.value=sCellValue;}}}

}});}
$(sActionFormId).dialog('open');});}
oActionForm.submit(function(event){var oTableAction=$(this).data("action-options");if(oTableAction.sType=="edit"){ fnUpdateRow(this);}else{fnAddRowFromForm(this);}
return false;});var aActionFormButtons=new Array();var oActionFormCancel=$("#form"+sAction+"Cancel",oActionForm);if(oActionFormCancel.length!=0){aActionFormButtons.push(oActionFormCancel);oActionFormCancel.click(function(){var oActionForm=$(this).parents("form")[0];$(oActionForm).validate().resetForm();$(oActionForm)[0].reset();$(".error",$(oActionForm)).html("");$(".error",$(oActionForm)).hide();$(oActionForm).dialog('close');});}
$("button",oActionForm).button();}}}});};})(jQuery);