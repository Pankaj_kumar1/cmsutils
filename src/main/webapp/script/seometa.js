$(document).ready(function() {
	/* var bindContentEditableDivEvent = function() {
		$("div[contenteditable='true'].editableHyperlink").on("contextmenu", function(e) {
			replaceSelectedText(e);
		}).on("paste", function(e) {
			e.preventDefault();
			var plainText = e.originalEvent.clipboardData.getData("text/plain");
			document.execCommand("insertHTML", false, plainText.trim());
		});
	};
	bindContentEditableDivEvent();
	var replaceSelectedText = function(e) {
		e.preventDefault();
	    var sel, range, selText, anchor;
	    if (window.getSelection) {
	        sel = window.getSelection();
	        if (sel.rangeCount) {
	            range = sel.getRangeAt(0);
	            selText = range.toString();//.trim();
	            if(selText.trim()) {
				    var href = prompt("Please enter href");
				    if (href != null) {
				    	if(range.endContainer.parentElement.localName == "a") {
				    		range.endContainer.parentElement.href = href;
				    	} else {
				            anchor = document.createElement("a");
				            anchor.href = href;
				            anchor.text = selText;
				            range.deleteContents();
				            range.insertNode(anchor);
				    	}
				    }
	            }
	        }
	    }
	}; */
		$("#submit").click(function() {

				var titles = [];
				$("input[id='titleElement']").each(function() {
					titles.push($(this).val());
				});
				
				
				var values = [];
				$("input[id='urlElement']").each(function() {
				    values.push($(this).val());
				});
			
				var postdata = {};
				
				/* var elem = document.createElement('textarea');
	            elem.innerHTML = $("#atf-content").html();
	            var decodedAtf = elem.value; */
	            
	            /* var elem1 = document.createElement('textarea');
	            elem1.innerHTML = $("#btf-content").html();
	            var decodedBtf = elem1.value; */
				
	            var atf = CKEDITOR.instances.atf.getData();
	            var btf = CKEDITOR.instances.btf.getData();
	            
	            if(($("#channel").val()) == "Select Channel"){
	            	alert("Please select channel");
	            }
	            
//				postdata["username"] = $("#username").val();
				postdata["uri"] = $("#uri").val();
				postdata["channel"] = $("#channel").val();
				postdata["title"] = $("#title").val();
				postdata["longdesc"] = $("#longdesc").val();
				postdata["shortdesc"] = $("#shortdesc").val();
				postdata["atf-content"] = atf;
				postdata["btf-content"] = btf;
				postdata["canonicalUrl"] = $("#canonicalUrl").val();
				postdata["mUrl"] = $("#mUrl").val();
				postdata["ampUrl"] = $("#ampUrl").val();
				postdata["metakeywords"] = $("#metakeywords").val();
				postdata["titleElement"] = String(titles);
				postdata["urlElement"] = String(values);
//				postdata["mode"] = $("#mode").val();
				postdata["action"]="create";
				

				$.ajax({
						url : "/cmsutils/seo-meta/insert",
						data : postdata,
						type : "POST",
						dataType : "json",
						}).done(function(json) {
							var message = json.msg;
							if(message[0].indexOf("ERROR:")>=0){
								alert(json.msg);
								window.location.reload();
							}else{
							alert(json.msg);
							$("#insform")[0].reset();
							CKEDITOR.instances.atf.setData('');
							CKEDITOR.instances.btf.setData('');
							
							}
						});
			          });

				$("#sear").click(function() {

				var postdata = {};
				postdata["shorturl"] = $("#Shorturl").val();
				postdata["channel"] = $("#Schannel").val();

				$.ajax({
					url : "/cmsutils/seo-meta/geturilist",
					data : postdata,
					type : "GET",
					dataType : "json",
                }).done(function(json) {
                	 if(json.msg != null){
                		alert(json.msg);
                		window.location.reload();
                	}else{ 
					var data = json[0];
					var template = $('#template').html();
					Mustache.parse(template); // optional, speeds up future uses
					var rendered = Mustache.render(template,data);
					$('#target').html(rendered);
					bindCkeditor();
					<!--bindContentEditableDivEvent();-->
					$('#channell option[value='+ data["Channel"]+ ']').attr('selected','selected');
					
				
					$("#upt").click(function() {
							var titles = [];
							$("input[id='upTitleElement']").each(function() {
								titles.push($(this).val());
							});
						
							var values = [];
							$("input[id='upUrlElement']").each(function() {
							    values.push($(this).val());
							});

							var postdata = {};
							
							/* var elem = document.createElement('textarea');
				            elem.innerHTML = $("#atf-contentt").html();
				            var decodedAtf = elem.value;
				            
				            var elem1 = document.createElement('textarea');
				            elem1.innerHTML = $("#btf-contentt").html();
				            var decodedBtf = elem1.value; */
							
				            var atff = CKEDITOR.instances.atfupdate.getData();
				            var btff = CKEDITOR.instances.btfupdate.getData();

							postdata["uri"] = $("#urii").val();
							postdata["channel"] = $("#channell").val();
							postdata["title"] = $("#titlee").val();
							postdata["shortdesc"] = $("#shortdescc").val();
							postdata["longdesc"] = $("#longdescc").val();
//							postdata["username"] = $("#usernamee").val();
							postdata["atf-content"] = atff;
							postdata["btf-content"] = btff;
							postdata["canonicalUrl"] = $("#canonicalUrll").val();
							postdata["mUrl"] = $("#mUrl_up").val();
							postdata["ampUrl"] = $("#ampUrl_up").val();
							postdata["metakeywords"] = $("#metakeywords_up").val();
							postdata["titleElement"] = String(titles);
							postdata["urlElement"] = String(values);
//							postdata["mode"] = $("#modee").val();
							postdata["action"]="update";

					$.ajax({
							url : "/cmsutils/seo-meta/insert",
							data : postdata,
							type : "POST",
							dataType : "json",
						}).done(function(json) {
							var message = json.msg;
							if(message[0].indexOf("ERROR:")>=0){
								alert(json.msg);
								window.location.reload();
							}else{
							alert(json.msg);
							$("#shorturl").val("");
							}
					});

               });
					$("#uptTitle").click(function() {
						
						uptd_fields();
					});
                	}

			});

		});
				
				$("#upload").click(function() {
					
					 var formData = new FormData();
				        $.each(files, function(key, value){
				            formData.append(key, value);
				        });
				   
				        alert(formData);
					$.ajax({
							url : "/cmsutils/upload/uploadfile",
							 type: 'GET',
					            data: formData,
					            success:function(data){
					                                    $('#result').html(data);
					                                }, 
					            cache: false,
					            contentType: false,
					            processData: false
							
							});
				          });

				

	});